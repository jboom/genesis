using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public class Action {
        public enum ActionType {
            Set,
            Add
        }

        public String key = "";
        public String value = "";
        public ActionList parent = null;
        public ActionType type = ActionType.Set;

        public override string ToString() {
            switch (type) {
                default:
                case ActionType.Set:
                    return "Set " + key + " to '" + value + "'";
                case ActionType.Add:
                    return "Add to " + key + ": '" + value + "'";
            }
        }

        public TreeNode ToTreeNode() {
            TreeNode node = new TreeNode();

            node.Text = ToString();
            node.Tag = this;

            return node;
        }
    }
}
