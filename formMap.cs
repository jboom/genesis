using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Genesis
{
	/// <summary>
	/// Summary description for formMap.
	/// </summary>
	public class formMap : System.Windows.Forms.Form
	{
		private Form parent;

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboScale;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textFile;
		private System.Windows.Forms.Button buttonFile;
		private System.Windows.Forms.SaveFileDialog fileSave;
		private System.Windows.Forms.CheckBox checkDrawExits;
		private System.Windows.Forms.CheckBox checkDrawExitSquares;
		private System.Windows.Forms.CheckBox checkDrawGrid;
		private System.Windows.Forms.CheckBox checkDrawIDs;
		private System.Windows.Forms.CheckBox checkDrawInvisExits;
		private System.Windows.Forms.CheckBox checkDrawRooms;
		private System.Windows.Forms.CheckBox checkDrawShadows;
		private System.Windows.Forms.CheckBox checkShowEmpty;
		private System.Windows.Forms.CheckBox checkShowSelection;
		private System.Windows.Forms.CheckBox checkDrawZones;
		private System.Windows.Forms.CheckBox checkDrawExcluded;
		private System.Windows.Forms.CheckBox checkFlatten;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formMap(Form p)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkFlatten = new System.Windows.Forms.CheckBox();
			this.checkDrawExcluded = new System.Windows.Forms.CheckBox();
			this.checkDrawZones = new System.Windows.Forms.CheckBox();
			this.checkShowSelection = new System.Windows.Forms.CheckBox();
			this.checkShowEmpty = new System.Windows.Forms.CheckBox();
			this.checkDrawShadows = new System.Windows.Forms.CheckBox();
			this.checkDrawRooms = new System.Windows.Forms.CheckBox();
			this.checkDrawInvisExits = new System.Windows.Forms.CheckBox();
			this.checkDrawIDs = new System.Windows.Forms.CheckBox();
			this.checkDrawGrid = new System.Windows.Forms.CheckBox();
			this.checkDrawExitSquares = new System.Windows.Forms.CheckBox();
			this.checkDrawExits = new System.Windows.Forms.CheckBox();
			this.buttonFile = new System.Windows.Forms.Button();
			this.textFile = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboScale = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonCreate = new System.Windows.Forms.Button();
			this.fileSave = new System.Windows.Forms.SaveFileDialog();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkFlatten);
			this.groupBox1.Controls.Add(this.checkDrawExcluded);
			this.groupBox1.Controls.Add(this.checkDrawZones);
			this.groupBox1.Controls.Add(this.checkShowSelection);
			this.groupBox1.Controls.Add(this.checkShowEmpty);
			this.groupBox1.Controls.Add(this.checkDrawShadows);
			this.groupBox1.Controls.Add(this.checkDrawRooms);
			this.groupBox1.Controls.Add(this.checkDrawInvisExits);
			this.groupBox1.Controls.Add(this.checkDrawIDs);
			this.groupBox1.Controls.Add(this.checkDrawGrid);
			this.groupBox1.Controls.Add(this.checkDrawExitSquares);
			this.groupBox1.Controls.Add(this.checkDrawExits);
			this.groupBox1.Controls.Add(this.buttonFile);
			this.groupBox1.Controls.Add(this.textFile);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.comboScale);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(400, 288);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Options";
			// 
			// checkFlatten
			// 
			this.checkFlatten.Checked = true;
			this.checkFlatten.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkFlatten.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkFlatten.Location = new System.Drawing.Point(216, 256);
			this.checkFlatten.Name = "checkFlatten";
			this.checkFlatten.Size = new System.Drawing.Size(96, 16);
			this.checkFlatten.TabIndex = 16;
			this.checkFlatten.Text = "Flatten Zones";
			// 
			// checkDrawExcluded
			// 
			this.checkDrawExcluded.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawExcluded.Location = new System.Drawing.Point(16, 256);
			this.checkDrawExcluded.Name = "checkDrawExcluded";
			this.checkDrawExcluded.Size = new System.Drawing.Size(176, 24);
			this.checkDrawExcluded.TabIndex = 15;
			this.checkDrawExcluded.Text = "Draw excluded rooms grayed";
			// 
			// checkDrawZones
			// 
			this.checkDrawZones.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawZones.Location = new System.Drawing.Point(216, 224);
			this.checkDrawZones.Name = "checkDrawZones";
			this.checkDrawZones.TabIndex = 14;
			this.checkDrawZones.Text = "Draw Zones";
			// 
			// checkShowSelection
			// 
			this.checkShowSelection.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkShowSelection.Location = new System.Drawing.Point(216, 192);
			this.checkShowSelection.Name = "checkShowSelection";
			this.checkShowSelection.Size = new System.Drawing.Size(160, 24);
			this.checkShowSelection.TabIndex = 13;
			this.checkShowSelection.Text = "Draw selection and current";
			// 
			// checkShowEmpty
			// 
			this.checkShowEmpty.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkShowEmpty.Location = new System.Drawing.Point(216, 160);
			this.checkShowEmpty.Name = "checkShowEmpty";
			this.checkShowEmpty.Size = new System.Drawing.Size(128, 24);
			this.checkShowEmpty.TabIndex = 12;
			this.checkShowEmpty.Text = "Draw empty marker";
			// 
			// checkDrawShadows
			// 
			this.checkDrawShadows.Checked = true;
			this.checkDrawShadows.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkDrawShadows.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawShadows.Location = new System.Drawing.Point(216, 128);
			this.checkDrawShadows.Name = "checkDrawShadows";
			this.checkDrawShadows.Size = new System.Drawing.Size(120, 24);
			this.checkDrawShadows.TabIndex = 11;
			this.checkDrawShadows.Text = "Draw shadows";
			// 
			// checkDrawRooms
			// 
			this.checkDrawRooms.Checked = true;
			this.checkDrawRooms.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkDrawRooms.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawRooms.Location = new System.Drawing.Point(216, 96);
			this.checkDrawRooms.Name = "checkDrawRooms";
			this.checkDrawRooms.TabIndex = 10;
			this.checkDrawRooms.Text = "Draw rooms";
			// 
			// checkDrawInvisExits
			// 
			this.checkDrawInvisExits.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawInvisExits.Location = new System.Drawing.Point(16, 224);
			this.checkDrawInvisExits.Name = "checkDrawInvisExits";
			this.checkDrawInvisExits.Size = new System.Drawing.Size(128, 24);
			this.checkDrawInvisExits.TabIndex = 9;
			this.checkDrawInvisExits.Text = "Draw invisible exits";
			// 
			// checkDrawIDs
			// 
			this.checkDrawIDs.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawIDs.Location = new System.Drawing.Point(16, 192);
			this.checkDrawIDs.Name = "checkDrawIDs";
			this.checkDrawIDs.TabIndex = 8;
			this.checkDrawIDs.Text = "Draw IDs";
			// 
			// checkDrawGrid
			// 
			this.checkDrawGrid.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawGrid.Location = new System.Drawing.Point(16, 160);
			this.checkDrawGrid.Name = "checkDrawGrid";
			this.checkDrawGrid.TabIndex = 7;
			this.checkDrawGrid.Text = "Draw grid";
			// 
			// checkDrawExitSquares
			// 
			this.checkDrawExitSquares.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawExitSquares.Location = new System.Drawing.Point(16, 128);
			this.checkDrawExitSquares.Name = "checkDrawExitSquares";
			this.checkDrawExitSquares.Size = new System.Drawing.Size(136, 24);
			this.checkDrawExitSquares.TabIndex = 6;
			this.checkDrawExitSquares.Text = "Draw exit information";
			// 
			// checkDrawExits
			// 
			this.checkDrawExits.Checked = true;
			this.checkDrawExits.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkDrawExits.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkDrawExits.Location = new System.Drawing.Point(16, 96);
			this.checkDrawExits.Name = "checkDrawExits";
			this.checkDrawExits.TabIndex = 5;
			this.checkDrawExits.Text = "Draw exits";
			// 
			// buttonFile
			// 
			this.buttonFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonFile.Location = new System.Drawing.Point(368, 56);
			this.buttonFile.Name = "buttonFile";
			this.buttonFile.Size = new System.Drawing.Size(24, 20);
			this.buttonFile.TabIndex = 4;
			this.buttonFile.Text = "...";
			this.buttonFile.Click += new System.EventHandler(this.buttonFile_Click);
			// 
			// textFile
			// 
			this.textFile.Location = new System.Drawing.Point(80, 56);
			this.textFile.Name = "textFile";
			this.textFile.Size = new System.Drawing.Size(288, 20);
			this.textFile.TabIndex = 3;
			this.textFile.Text = "";
			this.textFile.TextChanged += new System.EventHandler(this.textFile_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(23, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "File";
			// 
			// comboScale
			// 
			this.comboScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboScale.Items.AddRange(new object[] {
															"10%",
															"25%",
															"50%",
															"75%",
															"100%",
															"150%",
															"200%"});
			this.comboScale.Location = new System.Drawing.Point(80, 24);
			this.comboScale.Name = "comboScale";
			this.comboScale.Size = new System.Drawing.Size(56, 21);
			this.comboScale.TabIndex = 1;
			this.comboScale.SelectedIndexChanged += new System.EventHandler(this.comboScale_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(33, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Scale";
			// 
			// buttonClose
			// 
			this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonClose.Location = new System.Drawing.Point(224, 304);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(88, 24);
			this.buttonClose.TabIndex = 1;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonCreate
			// 
			this.buttonCreate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonCreate.Location = new System.Drawing.Point(320, 304);
			this.buttonCreate.Name = "buttonCreate";
			this.buttonCreate.Size = new System.Drawing.Size(88, 24);
			this.buttonCreate.TabIndex = 2;
			this.buttonCreate.Text = "Create";
			this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
			// 
			// formMap
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(418, 336);
			this.Controls.Add(this.buttonCreate);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "formMap";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Area Map";
			this.Load += new System.EventHandler(this.formMap_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void formMap_Load(object sender, System.EventArgs e) {
			comboScale.SelectedIndex = 4;
			CheckReady();
		}

		private void buttonFile_Click(object sender, System.EventArgs e) {
			fileSave.AddExtension = true;
			fileSave.DefaultExt = ".bmp";
			fileSave.Filter = "Image files (*.bmp, *.gif;, *.jpg, *.wmf)|*.bmp;*.gif;*.jpg;*.wmf";
			fileSave.OverwritePrompt = true;
			fileSave.ValidateNames = true;

			if (fileSave.ShowDialog() == DialogResult.Cancel) {
				return;
			}

			textFile.Text = fileSave.FileName;
		}

		private void CheckReady() {
			if (comboScale.Text.Length != 0 && textFile.Text.Length != 0) {
				buttonCreate.Enabled = true;
			} else {
				buttonCreate.Enabled = false;
			}
		}

		private void textFile_TextChanged(object sender, System.EventArgs e) {
			CheckReady();
		}

		private void comboScale_SelectedIndexChanged(object sender, System.EventArgs e) {
			CheckReady();
		}

		private void buttonCreate_Click(object sender, System.EventArgs e) {
			string scale;
			System.Drawing.Imaging.ImageFormat format;
			Common.RenderOptions opt = new Common.RenderOptions();

			scale = comboScale.Text.Remove(comboScale.Text.Length - 1, 1);

			switch (textFile.Text.Substring(textFile.Text.Length - 4, 4)) {
				case ".bmp":
					format = System.Drawing.Imaging.ImageFormat.Bmp;
					break;
				case ".gif":
					format = System.Drawing.Imaging.ImageFormat.Gif;
					break;
				case ".jpg":
					format = System.Drawing.Imaging.ImageFormat.Jpeg;
					break;
				case ".wmf":
					format = System.Drawing.Imaging.ImageFormat.Wmf;
					break;
				default:
					MessageBox.Show("This image format is not supported", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
			}

			opt.DrawExits = checkDrawExits.Checked;
			opt.DrawExitSquares = checkDrawExitSquares.Checked;
			opt.DrawGrid = checkDrawGrid.Checked;
			opt.DrawIDs = checkDrawIDs.Checked;
			opt.DrawInvExits = checkDrawInvisExits.Checked;
			opt.DrawRooms = checkDrawRooms.Checked;
			opt.DrawShadows = checkDrawShadows.Checked;
			opt.ShowEmpty = checkShowEmpty.Checked;
			opt.ShowSelection = checkShowSelection.Checked;
			opt.DrawZones = checkDrawZones.Checked;
			opt.DrawExcluded = checkDrawExcluded.Checked;
			opt.FlattenZones = checkFlatten.Checked;
			opt.IgnoreScreenSize = true;

			if (((formMain)parent).SaveAreaBitmap(textFile.Text, Convert.ToInt32(scale), format, opt)) {
				MessageBox.Show("Image created succesfully.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void buttonClose_Click(object sender, System.EventArgs e) {
			this.Close();
		}
	}
}
