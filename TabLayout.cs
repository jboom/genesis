using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Genesis {
    [System.Xml.Serialization.XmlRoot(ElementName = "DetailLayout", IsNullable = false)]
    [System.Xml.Serialization.XmlInclude(typeof(DetailField))]
    [System.Xml.Serialization.XmlInclude(typeof(FieldText))]
    [System.Xml.Serialization.XmlInclude(typeof(FieldTextArea))]
    [System.Xml.Serialization.XmlInclude(typeof(FieldNumber))]
    [System.Xml.Serialization.XmlInclude(typeof(FieldCheckbox))]
    [System.Xml.Serialization.XmlInclude(typeof(FieldDropdown))]
    public class DetailLayout : List<DetailField> {
        public static DetailLayout Load(String filename) {
            DetailLayout layout;
            try {
                XmlSerializer xs = new XmlSerializer(typeof(DetailLayout));
                FileStream fs = new FileStream(filename, FileMode.Open);
                layout = (DetailLayout)xs.Deserialize(fs);
                fs.Close();
            } catch (Exception e) {
                Log.error(e);
                return null;
            }

            return layout;
        }

        public static void Save(String filename, DetailLayout layout) {
            try {
                XmlSerializer xs = new XmlSerializer(typeof(DetailLayout));
                TextWriter tw = new StreamWriter(filename);
                xs.Serialize(tw, layout);
                tw.Close();
            } catch (Exception e) {
                Log.error(e);
            }
        }
    }
}
