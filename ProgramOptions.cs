using System;
using System.Collections.Generic;
using System.Drawing;

namespace Genesis
{
	/// <summary>
	/// This class contains all options that need to be saved to a file.
	/// </summary>
	[System.Xml.Serialization.XmlRoot(ElementName="Settings", IsNullable=false)]
	public class ProgramOptions {
		private List<String> m_EmptyProperties;
        private List<String> m_CommonProperties;
        private List<String> m_PropertyTypeListRoom;
        private List<String> m_PropertyTypeListObject;
        private List<String> m_CodePropertyList;
		private string m_FileType;
        private string m_DefaultRoomLayoutFile;

        public string DefaultRoomLayoutFile {
            get { return m_DefaultRoomLayoutFile; }
            set { m_DefaultRoomLayoutFile = value; }
        }
        private string m_DefaultObjectLayoutFile;

        public string DefaultObjectLayoutFile {
            get { return m_DefaultObjectLayoutFile; }
            set { m_DefaultObjectLayoutFile = value; }
        }
        private bool m_AddInvisible;
		private bool m_DockEditor;
		private bool m_AutoSave;
		private bool m_DrawGrid;
		private bool m_DrawZones;
		private bool m_DrawShadows;
		private bool m_ExcludeOld;
		private bool m_NotesAsLabel;
		private int m_DrawQuality;
		private int m_BackgroundColour;
        private int m_GridColour;
		private int m_EditorOpacity;
        private int m_EditorLeft;
        private int m_EditorTop;
        private int m_EditorWidth;
        private int m_EditorHeight;
        private int m_ObjectsLeft;
        private int m_ObjectsTop;

		public ProgramOptions() {
			// default constructor
        }

        public void SetDefaults() {
            this.AddInvisible = true;
            this.AutoSave = false;
            this.BackgroundColour = Color.White.ToArgb();
            this.GridColour = Color.FromArgb(204, 221, 255).ToArgb();
            this.CommonProperties = new List<String>();
            this.CommonProperties.Add("long_night:");
            this.CommonProperties.Add("long:");
            this.CommonProperties.Add("short:");
            this.CommonProperties.Add("template:");
            this.CommonProperties.Add("colour:-1");
            this.CommonProperties.Add("properties:light^2|indoors^0|night_light^1");
            this.DockEditor = false;
            this.DrawGrid = false;
            this.DrawQuality = 3;
            this.DrawShadows = true;
            this.DrawZones = false;
            this.EditorOpacity = 100;
            this.EmptyProperties = new List<String>();
            this.EmptyProperties.Add("short");
            this.EmptyProperties.Add("long");
            this.EmptyProperties.Add("items");
            this.ExcludeOld = false;
            this.NotesAsLabel = false;
            this.ObjectPropertyTypeList = new List<String>();
            this.ObjectPropertyTypeList.Add("Properties");
            this.ObjectPropertyTypeList.Add("Items");
            this.ObjectPropertyTypeList.Add("Ids");
            this.RoomPropertyTypeList = new List<String>();
            this.RoomPropertyTypeList.Add("Properties");
            this.RoomPropertyTypeList.Add("Items");
            this.RoomPropertyTypeList.Add("Smells");
            this.RoomPropertyTypeList.Add("Listens");
            this.RoomPropertyTypeList.Add("Custom");
            this.CodePropertyList = new List<String>();
            this.CodePropertyList.Add("create");
            this.CodePropertyList.Add("reset");
            this.FileType = "Unix";
            this.DefaultRoomLayoutFile = "";
            this.DefaultObjectLayoutFile = "";
        }

		public int EditorOpacity {
			get {
				return m_EditorOpacity;
			}
			set {
				m_EditorOpacity = value;
			}
		}

        public int EditorLeft {
            get {
                return m_EditorLeft;
            }
            set {
                m_EditorLeft = value;
            }
        }

        public int EditorTop {
            get {
                return m_EditorTop;
            }
            set {
                m_EditorTop = value;
            }
        }

        public int EditorHeight {
            get {
                return m_EditorHeight;
            }
            set {
                m_EditorHeight = value;
            }
        }

        public int EditorWidth {
            get {
                return m_EditorWidth;
            }
            set {
                m_EditorWidth = value;
            }
        }

        public int ObjectsFormLeft {
            get {
                return m_ObjectsLeft;
            }
            set {
                m_ObjectsLeft = value;
            }
        }

        public int ObjectsFormTop {
            get {
                return m_ObjectsTop;
            }
            set {
                m_ObjectsTop = value;
            }
        }

        public int GridColour {
            get {
                return m_GridColour;
            }
            set {
                m_GridColour = value;
            }
        }

		public int BackgroundColour {
			get {
				return m_BackgroundColour;
			} 
			set {
				m_BackgroundColour = value;
			}
		}

        public List<String> RoomPropertyTypeList {
			get {
				return m_PropertyTypeListRoom;
			}
			set {
				m_PropertyTypeListRoom = value;
			}
		}

        public List<String> ObjectPropertyTypeList {
            get {
                return m_PropertyTypeListObject;
            }
            set {
                m_PropertyTypeListObject = value;
            }
        }

        public List<String> CommonProperties {
			get {
				return m_CommonProperties;
			}
			set {
				m_CommonProperties = value;
			}
		}

        public List<String> CodePropertyList {
            get {
                return m_CodePropertyList;
            }
            set {
                m_CodePropertyList = value;
            }
        }

        public List<String> EmptyProperties {
			get {
				return m_EmptyProperties;
			}
			set {
				m_EmptyProperties = value;
			}
		}

		public bool AddInvisible {
			get {
				return m_AddInvisible;
			}
			set {
				m_AddInvisible = value;
			}
		}

		public bool NotesAsLabel {
			get {
				return m_NotesAsLabel;
			}
			set {
				m_NotesAsLabel = value;
			}
		}

		public bool DrawShadows {
			get {
				return m_DrawShadows;
			}
			set {
				m_DrawShadows = value;
			}
		}

		public bool DrawZones {
			get {
				return m_DrawZones;
			}
			set {
				m_DrawZones = value;
			}
		}

		public bool DrawGrid {
			get {
				return m_DrawGrid;
			}
			set {
				m_DrawGrid = value;
			}
		}

		public bool AutoSave {
			get {
				return m_AutoSave;
			}
			set {
				m_AutoSave = value;
			}
		}

		public bool DockEditor {
			get {
				return m_DockEditor;
			}
			set {
				m_DockEditor = value;
			}
		}

		public bool ExcludeOld {
			get {
				return m_ExcludeOld;
			}
			set {
				m_ExcludeOld = value;
			}
		}

		public int DrawQuality {
			get {
				return m_DrawQuality;
			}
			set {
				m_DrawQuality = value;
			}
		}

		public string FileType {
			get {
				return m_FileType;
			}
			set {
				m_FileType = value;
			}
		}
	}
}
