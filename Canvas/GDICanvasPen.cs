using System;
using System.Collections.Generic;
using System.Text;

namespace Genesis.Canvas {
	public enum GDIPenStyle {
		Solid,
		Dotted
	}

    class GDICanvasPen : CanvasPen {
        private IntPtr _pen;
		private GDIPenStyle _style = GDIPenStyle.Solid;

        public IntPtr GDIPen {
            get { return _pen; }
            set { _pen = value; }
        }

		public GDIPenStyle Style {
			get { return _style; }
			set { _style = value; }
		}
    }
}
