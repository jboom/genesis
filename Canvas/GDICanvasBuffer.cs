using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Genesis.Canvas {
    public delegate void SizeChangedEventHandler(object sender, EventArgs e);

    class GDICanvasBuffer : CanvasBuffer {
        private IntPtr _bitmap;
        private IntPtr _context;
        private Size _size;

        public event SizeChangedEventHandler SizeChanged;

        public GDICanvasBuffer() {
        }

        public IntPtr Context {
            get { return _context; }
            set { _context = value; }
        }

        public IntPtr Bitmap {
            get { return _bitmap; }
            set { _bitmap = value; }
        }

        public Size Size {
            get { return _size; }
            set { _size = value; if (SizeChanged != null) SizeChanged(this, new EventArgs()); }
        }
    }
}
