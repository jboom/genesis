﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Genesis.Canvas {
	public class CanvasFont {
		private String _fontFamily;
		private int _size;

		public CanvasFont(String fontFamily, int size) {
			_fontFamily = fontFamily;
			_size = size;
		}
	}
}
