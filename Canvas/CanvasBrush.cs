using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Genesis.Canvas {
    public class CanvasBrush {
        private Color _color;

        public Color Color {
            get { return _color; }
            set { _color = value; }
        }
    }
}
