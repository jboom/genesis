﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Genesis.Canvas {
	public class GDICanvasFont : CanvasFont {
		private IntPtr _gdiFont;

		public GDICanvasFont(string fontFamily, int size) : base(fontFamily, size) {
		}

		public IntPtr GDIFont {
			get { return _gdiFont; }
			set { _gdiFont = value; }
		}
	}
}
