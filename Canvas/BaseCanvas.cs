using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using Genesis.Canvas;

namespace Genesis {
    public abstract class PaintCanvas : System.Windows.Forms.Panel {
        protected bool Locked = false;

        public abstract int BufferCount {
            get;
            set;
        }
        public abstract int CurrentBuffer {
            get;
            set;
        }

        public abstract void Rectangle(CanvasBrush fill, CanvasPen border, int x1, int y1, int x2, int y2);
        public abstract void Line(CanvasPen border, int x1, int y1, int x2, int y2);
        public abstract void Clear(Color color);
        public abstract void Clear();
        public abstract void Copy(int source, int dest);
        public abstract void Copy(int source, Rectangle sourceRect, int dest, int x, int y);
        public abstract void Present();
        public abstract void DrawString(string text, int x, int y, CanvasFont font);
        public abstract bool Lock();
        public abstract bool Unlock();
        public abstract void SetBufferSize(int index, int width, int height);
        public abstract void UpdateBuffers();

        public abstract CanvasPen CreatePen(Color color, int Width);
        public abstract CanvasBrush CreateBrush(Color color);
		public abstract CanvasFont CreateFont(String family, int size);
}
}
