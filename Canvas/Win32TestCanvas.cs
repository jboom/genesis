using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Genesis.Canvas;

namespace Genesis {
    public partial class Win32TestCanvas : PaintCanvas {
        #region GDI Interop Definitions
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateSolidBrush(uint crColor);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteObject(IntPtr hObject);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteDC(IntPtr hdc);
        [DllImport("gdi32.dll")]
        static extern bool Rectangle(IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);
        [DllImport("gdi32.dll")]
        static extern bool MoveToEx(IntPtr hdc, int X, int Y, IntPtr lpPoint);
        [DllImport("gdi32.dll")]
        static extern bool LineTo(IntPtr hdc, int nXEnd, int nYEnd);
        [DllImport("gdi32.dll")]
        static extern bool TextOut(IntPtr hdc, int nXStart, int nYStart, string lpString, int cbString);
        [DllImport("gdi32.dll")]
        static extern IntPtr GetStockObject(StockObjects fnObject);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreatePen(PenStyle fnPenStyle, int nWidth, uint crColor);
        [DllImport("user32.dll")]
        static extern int FillRect(IntPtr hDC, [In] ref RECT lprc, IntPtr hbr);
		[DllImport("gdi32.dll")]
		static extern IntPtr CreateFont(int nHeight, int nWidth, int nEscapement,
		   int nOrientation, int fnWeight, uint fdwItalic, uint fdwUnderline, uint
		   fdwStrikeOut, GdiCharsets fdwCharSet, uint fdwOutputPrecision, uint
		   fdwClipPrecision, uint fdwQuality, uint fdwPitchAndFamily, string lpszFace);
		/// <summary>
        ///    Performs a bit-block transfer of the color data corresponding to a
        ///    rectangle of pixels from the specified source device context into
        ///    a destination device context.
        /// </summary>
        /// <param name="hdc">Handle to the destination device context.</param>
        /// <param name="nXDest">The leftmost x-coordinate of the destination rectangle (in pixels).</param>
        /// <param name="nYDest">The topmost y-coordinate of the destination rectangle (in pixels).</param>
        /// <param name="nWidth">The width of the source and destination rectangles (in pixels).</param>
        /// <param name="nHeight">The height of the source and the destination rectangles (in pixels).</param>
        /// <param name="hdcSrc">Handle to the source device context.</param>
        /// <param name="nXSrc">The leftmost x-coordinate of the source rectangle (in pixels).</param>
        /// <param name="nYSrc">The topmost y-coordinate of the source rectangle (in pixels).</param>
        /// <param name="dwRop">A raster-operation code.</param>
        /// <returns>
        ///    <c>true</c> if the operation succeeded, <c>false</c> otherwise.
        /// </returns>
        [DllImport("gdi32.dll")]
        static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);
        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct RECT {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public RECT(int left_, int top_, int right_, int bottom_) {
                Left = left_;
                Top = top_;
                Right = right_;
                Bottom = bottom_;
            }

            public int Height { get { return Bottom - Top; } }
            public int Width { get { return Right - Left; } }
            public Size Size { get { return new Size(Width, Height); } }

            public Point Location { get { return new Point(Left, Top); } }

            // Handy method for converting to a System.Drawing.Rectangle
            public Rectangle ToRectangle() { return System.Drawing.Rectangle.FromLTRB(Left, Top, Right, Bottom); }

            public static RECT FromRectangle(Rectangle rectangle) {
                return new RECT(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
            }

            public override int GetHashCode() {
                return Left ^ ((Top << 13) | (Top >> 0x13))
                  ^ ((Width << 0x1a) | (Width >> 6))
                  ^ ((Height << 7) | (Height >> 0x19));
            }

            #region Operator overloads

            public static implicit operator Rectangle(RECT rect) {
                return rect.ToRectangle();
            }

            public static implicit operator RECT(Rectangle rect) {
                return FromRectangle(rect);
            }

            #endregion
        }
        /// <summary>
        ///     Specifies a raster-operation code. These codes define how the color data for the
        ///     source rectangle is to be combined with the color data for the destination
        ///     rectangle to achieve the final color.
        /// </summary>
        public enum TernaryRasterOperations : uint {
            /// <summary>dest = source</summary>
            SRCCOPY = 0x00CC0020,
            /// <summary>dest = source OR dest</summary>
            SRCPAINT = 0x00EE0086,
            /// <summary>dest = source AND dest</summary>
            SRCAND = 0x008800C6,
            /// <summary>dest = source XOR dest</summary>
            SRCINVERT = 0x00660046,
            /// <summary>dest = source AND (NOT dest)</summary>
            SRCERASE = 0x00440328,
            /// <summary>dest = (NOT source)</summary>
            NOTSRCCOPY = 0x00330008,
            /// <summary>dest = (NOT src) AND (NOT dest)</summary>
            NOTSRCERASE = 0x001100A6,
            /// <summary>dest = (source AND pattern)</summary>
            MERGECOPY = 0x00C000CA,
            /// <summary>dest = (NOT source) OR dest</summary>
            MERGEPAINT = 0x00BB0226,
            /// <summary>dest = pattern</summary>
            PATCOPY = 0x00F00021,
            /// <summary>dest = DPSnoo</summary>
            PATPAINT = 0x00FB0A09,
            /// <summary>dest = pattern XOR dest</summary>
            PATINVERT = 0x005A0049,
            /// <summary>dest = (NOT dest)</summary>
            DSTINVERT = 0x00550009,
            /// <summary>dest = BLACK</summary>
            BLACKNESS = 0x00000042,
            /// <summary>dest = WHITE</summary>
            WHITENESS = 0x00FF0062
        }
        public enum PenStyle : int {
            PS_SOLID = 0, //The pen is solid. 
            PS_DASH = 1, //The pen is dashed. 
            PS_DOT = 2, //The pen is dotted. 
            PS_DASHDOT = 3, //The pen has alternating dashes and dots. 
            PS_DASHDOTDOT = 4, //The pen has alternating dashes and double dots. 
            PS_NULL = 5, //The pen is invisible. 
            PS_INSIDEFRAME = 6
        };
        public enum GdiCharsets : uint {
            ANSI_CHARSET = 0,
            DEFAULT_CHARSET = 1,
            SYMBOL_CHARSET = 2,
            SHIFTJIS_CHARSET = 128,
            HANGEUL_CHARSET = 129,
            HANGUL_CHARSET = 129,
            GB2312_CHARSET = 134,
            CHINESEBIG5_CHARSET = 136,
            OEM_CHARSET = 255
        }
        public enum StockObjects {
            WHITE_BRUSH = 0,
            LTGRAY_BRUSH = 1,
            GRAY_BRUSH = 2,
            DKGRAY_BRUSH = 3,
            BLACK_BRUSH = 4,
            NULL_BRUSH = 5,
            HOLLOW_BRUSH = NULL_BRUSH,
            WHITE_PEN = 6,
            BLACK_PEN = 7,
            NULL_PEN = 8,
            OEM_FIXED_FONT = 10,
            ANSI_FIXED_FONT = 11,
            ANSI_VAR_FONT = 12,
            SYSTEM_FONT = 13,
            DEVICE_DEFAULT_FONT = 14,
            DEFAULT_PALETTE = 15,
            SYSTEM_FIXED_FONT = 16,
            DEFAULT_GUI_FONT = 17,
            DC_BRUSH = 18,
            DC_PEN = 19,
        }
        #endregion

        private IntPtr OldBackBufferBitmap = IntPtr.Zero;
        private IntPtr screenContext = IntPtr.Zero;
        private int bufferCount = 1;
        private int currentBuffer = 0;
        private List<GDICanvasBuffer> buffers;
        private Graphics screen;
        private bool locked = false;
        private List<GDICanvasBrush> brushes;
        private List<GDICanvasPen> pens;
		private List<GDICanvasFont> fonts;

        public Win32TestCanvas() {
            InitializeComponent();
            CreateBuffers();
            brushes = new List<GDICanvasBrush>();
            pens = new List<GDICanvasPen>();
			fonts = new List<GDICanvasFont>();
            this.Resize += new EventHandler(CanvasResize);
            this.Disposed += new EventHandler(CanvasDisposed);
        }

        private void CreateBuffers() {
            GDICanvasBuffer buffer;

            buffers = new List<GDICanvasBuffer>();
            for (int i = 0; i < bufferCount; i++) {
                buffer = new GDICanvasBuffer();
                buffer.Size = new Size(Width, Height);
                buffer.SizeChanged += new SizeChangedEventHandler(buffer_SizeChanged);
                buffers.Add(buffer);
            }
        }

        private void buffer_SizeChanged(object sender, EventArgs e) {
        }

        /**
         * Setting this value will undo any changes made to existing buffers (such as size)
         */
        public override int BufferCount {
            get { return bufferCount; }
            set {
                if (locked) {
                    throw new Exception("Cannot modify BufferCount while canvas is locked.");
                }
                bufferCount = value;
                DisposeBuffers();
                CreateBuffers();
                SetupBuffers();
            }
        }

        public override int CurrentBuffer {
            get { return currentBuffer + 1; }
            set {
                if (locked) {
                    throw new Exception("Cannot change the current buffer while canvas is locked.");
                }
                if (value > bufferCount) {
                    throw new Exception("The specified current buffer is not valid. Increase the amount of buffers.");
                }
                currentBuffer = value - 1;
            }
        }

        private void CanvasResize(object sender, EventArgs e) {
            DisposeBuffers();
            CreateBuffers();
            SetupBuffers();
        }

        private void CanvasDisposed(object sender, EventArgs e) {
            DisposeBuffers();
            DisposeObjects();
        }

        private GDICanvasBuffer GetCurrentBuffer() {
            return buffers[currentBuffer];
        }

        private IntPtr GetCurrentContext() {
            return buffers[currentBuffer].Context;
        }

        public override void SetBufferSize(int index, int width, int height) {
            buffers[index - 1].Size = new Size(width, height);
        }

        public override void Rectangle(CanvasBrush fill, CanvasPen border, int x1, int y1, int x2, int y2) {
            IntPtr context = GetCurrentContext();

            IntPtr brush = ((GDICanvasBrush)fill).GDIBrush;
            IntPtr pen = ((GDICanvasPen)border).GDIPen;
            IntPtr oldBrush = SelectObject(context, brush);
            IntPtr oldPen = SelectObject(context, pen);
            Rectangle(context, x1, y1, x2, y2);
            SelectObject(context, oldBrush);
            SelectObject(context, oldPen);
        }

        public override void Line(CanvasPen border, int x1, int y1, int x2, int y2) {
            IntPtr context = GetCurrentContext();

            IntPtr pen = ((GDICanvasPen)border).GDIPen;
            IntPtr oldPen = SelectObject(context, pen);
            MoveToEx(context, x1, y1, IntPtr.Zero);
            LineTo(context, x2, y2);
            SelectObject(context, oldPen);
        }

        private uint ConvertColorToUint(Color c) {
            return (uint)(c.R | c.G << 8 | c.B << 16);
        }

        public override void Clear() {
            Clear(BackColor);
        }

        public override void Clear(Color color) {
            GDICanvasBuffer buffer = GetCurrentBuffer();

            RECT rect = new RECT(0, 0, buffer.Size.Width, buffer.Size.Height);
            FillRect(buffer.Context, ref rect, CreateSolidBrush(ConvertColorToUint(color)));
        }

        public override void Copy(int source, int dest) {
            GDICanvasBuffer sourceBuffer = GetBuffer(source);
            GDICanvasBuffer destBuffer = GetBuffer(dest);

            BitBlt(destBuffer.Context, 0, 0, sourceBuffer.Size.Width, sourceBuffer.Size.Height, sourceBuffer.Context, 0, 0, TernaryRasterOperations.SRCCOPY);
        }

        public override void Copy(int source, Rectangle sourceRect, int dest, int x, int y) {
            IntPtr sourceContext = GetContext(source);
            IntPtr destContext = GetContext(dest);

            BitBlt(destContext, x, y, sourceRect.Width, sourceRect.Height, sourceContext, sourceRect.X, sourceRect.Y, TernaryRasterOperations.SRCCOPY);
        }

        private IntPtr GetContext(int i) {
            return buffers[i - 1].Context;
        }

        private GDICanvasBuffer GetBuffer(int i) {
            return buffers[i - 1];
        }

        public override void Present() {
            IntPtr context = GetCurrentContext();

            BitBlt(screenContext, 0, 0, Width, Height, context, 0, 0, TernaryRasterOperations.SRCCOPY);
        }

		public override void DrawString(string text, int x, int y, CanvasFont font) {
            IntPtr context = GetCurrentContext();

			IntPtr oldFont = SelectObject(context, ((GDICanvasFont)font).GDIFont);
            TextOut(context, x, y, text, text.Length);
			SelectObject(context, oldFont);
        }

        public override bool Lock() {
            locked = true;
            screenContext = screen.GetHdc();

            return true;
        }

        public override bool Unlock() {
            screen.ReleaseHdc(screenContext);
            locked = false;

            return true;
        }

        private void SetupBuffers() {
            screen = CreateGraphics();
            screenContext = screen.GetHdc();
            foreach (GDICanvasBuffer buffer in buffers) {
                SetupBuffer(buffer);
            }
            screen.ReleaseHdc(screenContext);
        }

        private void SetupBuffer(GDICanvasBuffer buffer) {
            DisposeBuffer(buffer);
            buffer.Bitmap = CreateCompatibleBitmap(screenContext, buffer.Size.Width, buffer.Size.Height);
            buffer.Context = CreateCompatibleDC(screenContext);
            SelectObject(buffer.Context, buffer.Bitmap);
            currentBuffer = buffers.IndexOf(buffer);
            Clear();
        }

        private void DisposeBuffer(GDICanvasBuffer buffer) {
            if (buffer.Bitmap != IntPtr.Zero) {
                DeleteObject(buffer.Bitmap);
                buffer.Bitmap = IntPtr.Zero;
            }
            if (buffer.Context != IntPtr.Zero) {
                DeleteDC(buffer.Context);
                buffer.Context = IntPtr.Zero;
            }
        }

        private void DisposeBuffers() {
            if (screen != null) screen.Dispose();
            foreach (GDICanvasBuffer buffer in buffers) {
                DisposeBuffer(buffer);
            }
        }

        public override void UpdateBuffers() {
			if (locked) {
				throw new Exception("Cannot update buffers while locked.");
			}
            SetupBuffers();
        }

        public override CanvasPen CreatePen(Color color, int width) {
            GDICanvasPen pen = new GDICanvasPen();
            pen.Color = color;
            pen.Width = width;
            pen.GDIPen = CreatePen(PenStyle.PS_SOLID, width, ConvertColorToUint(color));

            pens.Add(pen);

            return pen;
        }

        public override CanvasBrush CreateBrush(Color color) {
            GDICanvasBrush brush = new GDICanvasBrush();
            brush.Color = color;
            if (color.A == 0) {
				brush.GDIBrush = GetStockObject(StockObjects.HOLLOW_BRUSH);
            } else {
                brush.GDIBrush = CreateSolidBrush(ConvertColorToUint(color));
            }

            brushes.Add(brush);

            return brush;
        }

        private void DisposeObjects() {
            foreach (GDICanvasPen pen in pens) {
                DeleteObject(pen.GDIPen);
                pen.GDIPen = IntPtr.Zero;
            }

            foreach (GDICanvasBrush brush in brushes) {
                DeleteObject(brush.GDIBrush);
                brush.GDIBrush = IntPtr.Zero;
            }

			foreach (GDICanvasFont font in fonts) {
				DeleteObject(font.GDIFont);
				font.GDIFont = IntPtr.Zero;
			}
        }

		public override CanvasFont CreateFont(string family, int size) {
			GDICanvasFont font = new GDICanvasFont(family, size);

			font.GDIFont = CreateFont(size, size / 2, 0, 0, 400, 0, 0, 0, GdiCharsets.DEFAULT_CHARSET, 0, 0, 0, 0, family);

			fonts.Add(font);

			return font;
		}
	}
}