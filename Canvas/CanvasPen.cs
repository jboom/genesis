using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Genesis.Canvas {
    public class CanvasPen {
        private Color _color;
        private int _width;

        public Color Color {
            get { return _color; }
            set { _color = value; }
        }

        public int Width {
            get { return _width; }
            set { _width = value; }
        }
    }
}
