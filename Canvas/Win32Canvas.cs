using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using Genesis.Canvas;

namespace Genesis {
    class Win32Canvas : PaintCanvas {
        private System.Drawing.Graphics gScreen;
        private IntPtr FrontBufferBitmap;
        private IntPtr BackBufferBitmap;
        private IntPtr FrontBufferContext;
        private IntPtr BackBufferContext;
        private IntPtr TargetContext = IntPtr.Zero;
        private IntPtr ScreenContext;
        private IntPtr OldBufferBitmap;
        private int bufferWidth;

        public int BufferWidth {
            get { return bufferWidth; }
            set { bufferWidth = value; }
        }
        private int bufferHeight;

        public int BufferHeight {
            get { return bufferHeight; }
            set { bufferHeight = value; }
        }

        private int bufferFactor = 3;
        public int BufferFactor {
            get { return bufferFactor; }
            set { bufferFactor = value; }
        }

        public enum PenStyle : int {
            PS_SOLID = 0, //The pen is solid. 
            PS_DASH = 1, //The pen is dashed. 
            PS_DOT = 2, //The pen is dotted. 
            PS_DASHDOT = 3, //The pen has alternating dashes and dots. 
            PS_DASHDOTDOT = 4, //The pen has alternating dashes and double dots. 
            PS_NULL = 5, //The pen is invisible. 
            PS_INSIDEFRAME = 6
        };

        public enum GdiColors {
            //0x00bbggrr
            Black = 0x00000000,
            White = 0x00ffffff,
            Red = 0x000000ff,
            Blue = 0x00ff0000,
            Green = 0x0000ff00
        }

        public enum GdiCharsets : int {
            ANSI_CHARSET = 0,
            DEFAULT_CHARSET = 1,
            SYMBOL_CHARSET = 2,
            SHIFTJIS_CHARSET = 128,
            HANGEUL_CHARSET = 129,
            HANGUL_CHARSET = 129,
            GB2312_CHARSET = 134,
            CHINESEBIG5_CHARSET = 136,
            OEM_CHARSET = 255
        }

        public enum StockObjects {
            WHITE_BRUSH = 0,
            LTGRAY_BRUSH = 1,
            GRAY_BRUSH = 2,
            DKGRAY_BRUSH = 3,
            BLACK_BRUSH = 4,
            NULL_BRUSH = 5,
            HOLLOW_BRUSH = NULL_BRUSH,
            WHITE_PEN = 6,
            BLACK_PEN = 7,
            NULL_PEN = 8,
            OEM_FIXED_FONT = 10,
            ANSI_FIXED_FONT = 11,
            ANSI_VAR_FONT = 12,
            SYSTEM_FONT = 13,
            DEVICE_DEFAULT_FONT = 14,
            DEFAULT_PALETTE = 15,
            SYSTEM_FIXED_FONT = 16,
            DEFAULT_GUI_FONT = 17,
            DC_BRUSH = 18,
            DC_PEN = 19,
        }

        [DllImport("gdi32.dll")]
        static extern int SetBkColor(IntPtr hdc, int crColor);

        [DllImport("gdi32.dll")]
        static extern bool FloodFill(IntPtr hdc, int nXStart, int nYStart, uint crFill);

        [DllImport("gdi32.dll")]
        static extern IntPtr GetStockObject(StockObjects fnObject);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateHatchBrush(int fnStyle, uint clrref);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateFont(int nHeight, int nWidth, int nEscapement,
           int nOrientation, int fnWeight, uint fdwItalic, uint fdwUnderline, uint
           fdwStrikeOut, uint fdwCharSet, uint fdwOutputPrecision, uint
           fdwClipPrecision, uint fdwQuality, uint fdwPitchAndFamily, string lpszFace);

        [DllImport("gdi32.dll")]
        static extern bool TextOut(IntPtr hdc, int nXStart, int nYStart, string lpString, int cbString);

        [DllImport("gdi32.dll")]
        static extern bool Rectangle(IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

        [DllImport("gdi32.dll")]
        static extern bool MoveToEx(IntPtr hdc, int X, int Y, IntPtr lpPoint);

        [DllImport("gdi32.dll")]
        static extern bool LineTo(IntPtr hdc, int nXEnd, int nYEnd);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateSolidBrush(uint crColor);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreatePen(PenStyle fnPenStyle, int nWidth, uint crColor);

        /// <summary>
        ///     Specifies a raster-operation code. These codes define how the color data for the
        ///     source rectangle is to be combined with the color data for the destination
        ///     rectangle to achieve the final color.
        /// </summary>
        public enum TernaryRasterOperations : uint {
            /// <summary>dest = source</summary>
            SRCCOPY = 0x00CC0020,
            /// <summary>dest = source OR dest</summary>
            SRCPAINT = 0x00EE0086,
            /// <summary>dest = source AND dest</summary>
            SRCAND = 0x008800C6,
            /// <summary>dest = source XOR dest</summary>
            SRCINVERT = 0x00660046,
            /// <summary>dest = source AND (NOT dest)</summary>
            SRCERASE = 0x00440328,
            /// <summary>dest = (NOT source)</summary>
            NOTSRCCOPY = 0x00330008,
            /// <summary>dest = (NOT src) AND (NOT dest)</summary>
            NOTSRCERASE = 0x001100A6,
            /// <summary>dest = (source AND pattern)</summary>
            MERGECOPY = 0x00C000CA,
            /// <summary>dest = (NOT source) OR dest</summary>
            MERGEPAINT = 0x00BB0226,
            /// <summary>dest = pattern</summary>
            PATCOPY = 0x00F00021,
            /// <summary>dest = DPSnoo</summary>
            PATPAINT = 0x00FB0A09,
            /// <summary>dest = pattern XOR dest</summary>
            PATINVERT = 0x005A0049,
            /// <summary>dest = (NOT dest)</summary>
            DSTINVERT = 0x00550009,
            /// <summary>dest = BLACK</summary>
            BLACKNESS = 0x00000042,
            /// <summary>dest = WHITE</summary>
            WHITENESS = 0x00FF0062
        }

        /// <summary>
        ///    Performs a bit-block transfer of the color data corresponding to a
        ///    rectangle of pixels from the specified source device context into
        ///    a destination device context.
        /// </summary>
        /// <param name="hdc">Handle to the destination device context.</param>
        /// <param name="nXDest">The leftmost x-coordinate of the destination rectangle (in pixels).</param>
        /// <param name="nYDest">The topmost y-coordinate of the destination rectangle (in pixels).</param>
        /// <param name="nWidth">The width of the source and destination rectangles (in pixels).</param>
        /// <param name="nHeight">The height of the source and the destination rectangles (in pixels).</param>
        /// <param name="hdcSrc">Handle to the source device context.</param>
        /// <param name="nXSrc">The leftmost x-coordinate of the source rectangle (in pixels).</param>
        /// <param name="nYSrc">The topmost y-coordinate of the source rectangle (in pixels).</param>
        /// <param name="dwRop">A raster-operation code.</param>
        /// <returns>
        ///    <c>true</c> if the operation succeeded, <c>false</c> otherwise.
        /// </returns>
        [DllImport("gdi32.dll")]
        static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);
        
        /*
          HDC hdcDest,        // handle to destination DC
          int nXOriginDest,   // x-coord of destination upper-left corner
          int nYOriginDest,   // y-coord of destination upper-left corner
          int nWidthDest,     // width of destination rectangle
          int hHeightDest,    // height of destination rectangle
          HDC hdcSrc,         // handle to source DC
          int nXOriginSrc,    // x-coord of source upper-left corner
          int nYOriginSrc,    // y-coord of source upper-left corner
          int nWidthSrc,      // width of source rectangle
          int nHeightSrc,     // height of source rectangle
          UINT crTransparent  // color to make transparent
        */
        [DllImport("msimg32.dll")]
        static extern bool TransparentBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int hHeightDest, IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, uint crTransparent);

        public Win32Canvas() {
            this.Resize += new EventHandler(Win32Canvas_Resize);
            this.Disposed += new EventHandler(Win32Canvas_Disposed);
        }

        void Win32Canvas_Disposed(object sender, EventArgs e) {
            DisposeBuffers();
        }

        void Win32Canvas_Resize(object sender, EventArgs e) {
            if (Width == 0 || Height == 0) return;
            SetupBuffers();
        }

        private void SetupBuffers() {
            DisposeBuffers();
            gScreen = CreateGraphics();
            bufferWidth = Screen.PrimaryScreen.WorkingArea.Width;
            bufferHeight = Screen.PrimaryScreen.WorkingArea.Height;
            IntPtr pTarget = gScreen.GetHdc();
            BackBufferBitmap = CreateCompatibleBitmap(pTarget, bufferWidth * BufferFactor, bufferHeight * BufferFactor);
            gScreen.ReleaseHdc(pTarget);
            TargetContext = BackBufferContext;
        }

        private void DisposeBuffers() {
            if (gScreen != null) gScreen.Dispose();
            if (BackBufferBitmap != IntPtr.Zero) { DeleteObject(BackBufferBitmap); BackBufferBitmap = IntPtr.Zero; }
            TargetContext = IntPtr.Zero;
        }

        public override void Rectangle(CanvasBrush fill, CanvasPen border, int x1, int y1, int x2, int y2) {
            IntPtr brush = CreateSolidBrush(0x00f5ebc8);
            IntPtr oldBrush = SelectObject(TargetContext, brush);
            Rectangle(TargetContext, x1, y1, x2, y2);
            SelectObject(TargetContext, oldBrush);
            DeleteObject(brush);
        }

        public override void Line(CanvasPen border, int x1, int y1, int x2, int y2) {
            MoveToEx(TargetContext, x1, y1, IntPtr.Zero);
            LineTo(TargetContext, x2, y2);
        }

        public override void Clear(System.Drawing.Color color) {
            //Rectangle(new SolidBrush(Color.White), new Pen(Color.White), 0, 0, BufferWidth, BufferHeight);
        }

        public override void DrawString(string text, int x, int y, CanvasFont font) {
            TextOut(TargetContext, x, y, text, text.Length);
        }

        public override bool Lock() {
            Locked = true;
            ScreenContext = gScreen.GetHdc();
            BackBufferContext = CreateCompatibleDC(ScreenContext);
            OldBufferBitmap = SelectObject(BackBufferContext, BackBufferBitmap);

            return true;
        }

        public override bool Unlock() {
            SelectObject(BackBufferContext, OldBufferBitmap);
            DeleteDC(BackBufferContext);
            gScreen.ReleaseHdc(ScreenContext);
            Locked = false;

            return true;
        }

        public override void Copy(int xOffset, int yOffset) {
            if (!Locked) throw new Exception("Cannot Flip while canvas is not locked.");

            FrontBufferContext = CreateCompatibleDC(ScreenContext);
            FrontBufferBitmap = CreateCompatibleBitmap(ScreenContext, Width, Height);
            BitBlt(FrontBufferContext, 0, 0, Width, Height, BackBufferContext, bufferWidth - xOffset, bufferHeight - yOffset, TernaryRasterOperations.SRCCOPY);
            TargetContext = FrontBufferContext;
        }

        public override void Present() {
            if (!Locked) throw new Exception("Cannot Present while canvas is not locked.");
            BitBlt(ScreenContext, 0, 0, Width, Height, FrontBufferContext, 0, 0, TernaryRasterOperations.SRCCOPY);
            TargetContext = BackBufferContext;
        }

        public override void Clear() {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int BufferCount {
            get {
                throw new Exception("The method or operation is not implemented.");
            }
            set {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override int CurrentBuffer {
            get {
                throw new Exception("The method or operation is not implemented.");
            }
            set {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override void Copy(int source, Rectangle sourceRect, int dest, int x, int y) {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void SetBufferSize(int index, int width, int height) {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void UpdateBuffers() {
            throw new Exception("The method or operation is not implemented.");
        }

        public override CanvasPen CreatePen(Color color, int Width) {
            throw new Exception("The method or operation is not implemented.");
        }

        public override CanvasBrush CreateBrush(Color color) {
            throw new Exception("The method or operation is not implemented.");
        }

		public override CanvasFont CreateFont(string family, int size) {
			throw new NotImplementedException();
		}
	}
}
