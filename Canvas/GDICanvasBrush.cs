using System;
using System.Collections.Generic;
using System.Text;

namespace Genesis.Canvas {
    class GDICanvasBrush : CanvasBrush {
        private IntPtr _brush;

        public IntPtr GDIBrush {
            get { return _brush; }
            set { _brush = value; }
        }
    }
}
