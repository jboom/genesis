namespace Genesis {
    partial class formControl {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formControl));
            this.buttonNW = new System.Windows.Forms.Button();
            this.buttonN = new System.Windows.Forms.Button();
            this.buttonNE = new System.Windows.Forms.Button();
            this.buttonW = new System.Windows.Forms.Button();
            this.buttonCenter = new System.Windows.Forms.Button();
            this.buttonE = new System.Windows.Forms.Button();
            this.buttonSW = new System.Windows.Forms.Button();
            this.buttonS = new System.Windows.Forms.Button();
            this.buttonSE = new System.Windows.Forms.Button();
            this.buttonU = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonMode = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // buttonNW
            // 
            this.buttonNW.Location = new System.Drawing.Point(12, 12);
            this.buttonNW.Name = "buttonNW";
            this.buttonNW.Size = new System.Drawing.Size(36, 36);
            this.buttonNW.TabIndex = 0;
            this.buttonNW.UseVisualStyleBackColor = true;
            this.buttonNW.Click += new System.EventHandler(this.buttonNW_Click);
            // 
            // buttonN
            // 
            this.buttonN.Image = ((System.Drawing.Image)(resources.GetObject("buttonN.Image")));
            this.buttonN.Location = new System.Drawing.Point(54, 12);
            this.buttonN.Name = "buttonN";
            this.buttonN.Size = new System.Drawing.Size(36, 36);
            this.buttonN.TabIndex = 1;
            this.buttonN.UseVisualStyleBackColor = true;
            this.buttonN.Click += new System.EventHandler(this.buttonN_Click);
            // 
            // buttonNE
            // 
            this.buttonNE.Location = new System.Drawing.Point(96, 12);
            this.buttonNE.Name = "buttonNE";
            this.buttonNE.Size = new System.Drawing.Size(36, 36);
            this.buttonNE.TabIndex = 2;
            this.buttonNE.UseVisualStyleBackColor = true;
            this.buttonNE.Click += new System.EventHandler(this.buttonNE_Click);
            // 
            // buttonW
            // 
            this.buttonW.Image = ((System.Drawing.Image)(resources.GetObject("buttonW.Image")));
            this.buttonW.Location = new System.Drawing.Point(12, 54);
            this.buttonW.Name = "buttonW";
            this.buttonW.Size = new System.Drawing.Size(36, 36);
            this.buttonW.TabIndex = 3;
            this.buttonW.UseVisualStyleBackColor = true;
            this.buttonW.Click += new System.EventHandler(this.buttonW_Click);
            // 
            // buttonCenter
            // 
            this.buttonCenter.Image = ((System.Drawing.Image)(resources.GetObject("buttonCenter.Image")));
            this.buttonCenter.Location = new System.Drawing.Point(54, 54);
            this.buttonCenter.Name = "buttonCenter";
            this.buttonCenter.Size = new System.Drawing.Size(36, 36);
            this.buttonCenter.TabIndex = 4;
            this.buttonCenter.UseVisualStyleBackColor = true;
            this.buttonCenter.Click += new System.EventHandler(this.buttonCenter_Click);
            // 
            // buttonE
            // 
            this.buttonE.Image = ((System.Drawing.Image)(resources.GetObject("buttonE.Image")));
            this.buttonE.Location = new System.Drawing.Point(96, 54);
            this.buttonE.Name = "buttonE";
            this.buttonE.Size = new System.Drawing.Size(36, 36);
            this.buttonE.TabIndex = 5;
            this.buttonE.UseVisualStyleBackColor = true;
            this.buttonE.Click += new System.EventHandler(this.buttonE_Click);
            // 
            // buttonSW
            // 
            this.buttonSW.Location = new System.Drawing.Point(12, 96);
            this.buttonSW.Name = "buttonSW";
            this.buttonSW.Size = new System.Drawing.Size(36, 36);
            this.buttonSW.TabIndex = 6;
            this.buttonSW.UseVisualStyleBackColor = true;
            this.buttonSW.Click += new System.EventHandler(this.buttonSW_Click);
            // 
            // buttonS
            // 
            this.buttonS.Image = ((System.Drawing.Image)(resources.GetObject("buttonS.Image")));
            this.buttonS.Location = new System.Drawing.Point(54, 96);
            this.buttonS.Name = "buttonS";
            this.buttonS.Size = new System.Drawing.Size(36, 36);
            this.buttonS.TabIndex = 7;
            this.buttonS.UseVisualStyleBackColor = true;
            this.buttonS.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonSE
            // 
            this.buttonSE.Location = new System.Drawing.Point(96, 96);
            this.buttonSE.Name = "buttonSE";
            this.buttonSE.Size = new System.Drawing.Size(36, 36);
            this.buttonSE.TabIndex = 8;
            this.buttonSE.UseVisualStyleBackColor = true;
            this.buttonSE.Click += new System.EventHandler(this.buttonSE_Click);
            // 
            // buttonU
            // 
            this.buttonU.Image = ((System.Drawing.Image)(resources.GetObject("buttonU.Image")));
            this.buttonU.Location = new System.Drawing.Point(138, 12);
            this.buttonU.Name = "buttonU";
            this.buttonU.Size = new System.Drawing.Size(36, 36);
            this.buttonU.TabIndex = 9;
            this.buttonU.UseVisualStyleBackColor = true;
            this.buttonU.Click += new System.EventHandler(this.buttonU_Click);
            // 
            // buttonD
            // 
            this.buttonD.Image = ((System.Drawing.Image)(resources.GetObject("buttonD.Image")));
            this.buttonD.Location = new System.Drawing.Point(180, 12);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(36, 36);
            this.buttonD.TabIndex = 10;
            this.buttonD.UseVisualStyleBackColor = true;
            this.buttonD.Click += new System.EventHandler(this.buttonD_Click);
            // 
            // buttonMode
            // 
            this.buttonMode.Location = new System.Drawing.Point(138, 54);
            this.buttonMode.Name = "buttonMode";
            this.buttonMode.Size = new System.Drawing.Size(78, 36);
            this.buttonMode.TabIndex = 11;
            this.buttonMode.UseVisualStyleBackColor = true;
            this.buttonMode.Click += new System.EventHandler(this.buttonMode_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // formControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 144);
            this.Controls.Add(this.buttonMode);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonU);
            this.Controls.Add(this.buttonSE);
            this.Controls.Add(this.buttonS);
            this.Controls.Add(this.buttonSW);
            this.Controls.Add(this.buttonE);
            this.Controls.Add(this.buttonCenter);
            this.Controls.Add(this.buttonW);
            this.Controls.Add(this.buttonNE);
            this.Controls.Add(this.buttonN);
            this.Controls.Add(this.buttonNW);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "formControl";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Control";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formControl_FormClosing);
            this.Load += new System.EventHandler(this.formControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonNW;
        private System.Windows.Forms.Button buttonN;
        private System.Windows.Forms.Button buttonNE;
        private System.Windows.Forms.Button buttonW;
        private System.Windows.Forms.Button buttonCenter;
        private System.Windows.Forms.Button buttonE;
        private System.Windows.Forms.Button buttonSW;
        private System.Windows.Forms.Button buttonS;
        private System.Windows.Forms.Button buttonSE;
        private System.Windows.Forms.Button buttonU;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonMode;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}