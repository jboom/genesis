using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public class ActionList {
        public ArrayList actions = new ArrayList();

        public override string ToString() {
            String str = "";

            if (actions.Count == 0) {
                return "Empty";
            }

            foreach (Action a in actions) {
                if (str.Length > 0) str += ". ";
                str += a.ToString();
            }
            return str;
        }

        public TreeNode ToTreeNode() {
            TreeNode node = new TreeNode();

            node.Text = ToString();
            node.Tag = this;

            foreach (Action a in actions) {
                node.Nodes.Add(a.ToTreeNode());
            }

            return node;
        }
        
    }
}
