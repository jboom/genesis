using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Genesis.Canvas;

namespace Genesis {
    public partial class formTestCanvas : Form {
        private CanvasPen pen;

        public formTestCanvas() {
            InitializeComponent();
            win32TestCanvas1.BufferCount = 2;
            //win32TestCanvas1.SetBufferSize(2, Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
            pen = win32TestCanvas1.CreatePen(Color.White, 2);
        }

        private void win32Canvas1_Paint(object sender, PaintEventArgs e) {
        }

        private void win32Canvas1_MouseUp(object sender, MouseEventArgs e) {
        }

        private void win32TestCanvas1_MouseDown(object sender, MouseEventArgs e) {
            win32TestCanvas1.CurrentBuffer = 2;

            win32TestCanvas1.Lock();
            win32TestCanvas1.Clear();
            //win32TestCanvas1.Rectangle(new SolidBrush(Color.White), new Pen(Color.LightBlue), 500, 500, 505, 505);
            win32TestCanvas1.Line(pen, e.X, e.Y, e.X + 100, e.Y + 100);
            win32TestCanvas1.Copy(2, 1);
            win32TestCanvas1.Unlock();

            win32TestCanvas1.CurrentBuffer = 1;

            win32TestCanvas1.Lock();
            win32TestCanvas1.DrawString("sdf", e.X, e.Y, null);
            win32TestCanvas1.Present();
            win32TestCanvas1.Unlock();
        }
    }
}