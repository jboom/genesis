using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;

namespace Genesis
{
	/// <summary>
	/// Object that represents a room
	/// </summary>
	public class Room : BaseObject
	{
		public int id, x, y, z, rx, ry;
		public bool empty, include, updated, visible;

        public Room() {
			id = -1;
			x = 0;
			y = 0;
			z = 0;
			empty = true;
			include = true;
			updated = false;
            visible = false;
            properties = new Dictionary<String,String>();
        }

        public bool IsIndoors {
            get {
                Hashtable h;
                h = Common.GetMultiHash(GetProperty("properties"));
                if ((string)h["indoors"] == "1") return true; else return false;
            }
        }

		public void Validate(List<String> l) {
			empty = false;
			foreach (string s in l) {
				if (GetProperty(s) == "") {
					empty = true;
				}
			}
		}

        public void UpdateCoordinates(int spacing) {
            rx = x * spacing;
            ry = y * spacing;
        }

        public bool isWithin(int x1, int y1, int x2, int y2, int w, int h) {
            bool b = false;

            if (x1 < x2) {
                if (rx > x1 && rx < x2) b = true; else b = false;
                if (rx + w > x1 && rx < x2) b = true; else b = false;
            } else if (x2 < x1) {
                if (rx > x2 && rx < x1) b = true; else b = false;
                if (rx + w > x2 && rx < x1) b = true; else b = false;
            }

            if (b) {
                if (y1 < y2) {
                    if (ry > y1 && ry < y2) b = true; else b = false;
                    if (ry + h > y1 && ry < y2) b = true; else b = false;
                } else if (y2 < y1) {
                    if (ry > y2 && ry < y1) b = true; else b = false;
                    if (ry + h > y2 && ry < y1) b = true; else b = false;
                }
            }

            return b;
        }
    }
}
