using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formListEdit.
	/// </summary>
	public class formListEdit : System.Windows.Forms.Form
	{
		public Dictionary<String,String> values;

		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Label labelName;
		private System.Windows.Forms.TextBox textName;
		private System.Windows.Forms.Label labelValue;
		private System.Windows.Forms.TextBox textValue;
		private System.Windows.Forms.ListBox listItems;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonDelete;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formListEdit(Dictionary<String,String> current)
		{
			InitializeComponent();

			labelValue.Visible = true;
			textValue.Visible = true;
			values = new Dictionary<String,String>();

			foreach (KeyValuePair<String,String> DE in current) {
				values.Add(DE.Key, DE.Value);
			}

			LoadList();
		}

		public formListEdit(List<String> current) {
			InitializeComponent();

			labelValue.Visible = false;
			textValue.Visible = false;
			values = new Dictionary<String,String>();

			foreach (string s in current) {
				values.Add(s, "");
			}

			LoadList();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.listItems = new System.Windows.Forms.ListBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelName = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.labelValue = new System.Windows.Forms.Label();
            this.textValue = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listItems
            // 
            this.listItems.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listItems.ItemHeight = 14;
            this.listItems.Location = new System.Drawing.Point(8, 8);
            this.listItems.Name = "listItems";
            this.listItems.ScrollAlwaysVisible = true;
            this.listItems.Size = new System.Drawing.Size(96, 214);
            this.listItems.TabIndex = 0;
            this.listItems.SelectedIndexChanged += new System.EventHandler(this.listItems_SelectedIndexChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonOk.Location = new System.Drawing.Point(200, 232);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(80, 24);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCancel.Location = new System.Drawing.Point(112, 232);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 24);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(112, 8);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Name";
            // 
            // textName
            // 
            this.textName.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textName.Location = new System.Drawing.Point(112, 24);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(168, 20);
            this.textName.TabIndex = 4;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Location = new System.Drawing.Point(112, 56);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(34, 13);
            this.labelValue.TabIndex = 5;
            this.labelValue.Text = "Value";
            // 
            // textValue
            // 
            this.textValue.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValue.Location = new System.Drawing.Point(112, 72);
            this.textValue.Multiline = true;
            this.textValue.Name = "textValue";
            this.textValue.Size = new System.Drawing.Size(168, 144);
            this.textValue.TabIndex = 6;
            // 
            // buttonSave
            // 
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSave.Location = new System.Drawing.Point(232, 48);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(48, 20);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "&Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClear.Location = new System.Drawing.Point(176, 48);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(48, 20);
            this.buttonClear.TabIndex = 8;
            this.buttonClear.Text = "&Clear";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDelete.Location = new System.Drawing.Point(8, 232);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(48, 20);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // formListEdit
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.textValue);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.listItems);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "formListEdit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void buttonCancel_Click(object sender, System.EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void buttonOk_Click(object sender, System.EventArgs e) {
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		public void LoadList() {
			listItems.BeginUpdate();
			listItems.Items.Clear();
			foreach (KeyValuePair<String,String> DE in values) {
				listItems.Items.Add((string)DE.Key);
			}
			listItems.EndUpdate();

			UpdateButtons();
		}

		private void UpdateButtons() {
			if (listItems.SelectedIndex != -1 || textName.Text.Length > 0) {
				buttonSave.Enabled = true;
				buttonClear.Enabled = true;
			} else {
				buttonSave.Enabled = false;
				buttonClear.Enabled = false;
			}
		}

		private void buttonSave_Click(object sender, System.EventArgs e) {
			if (values.ContainsKey(textName.Text)) {
				values.Remove(textName.Text);
			}

            values.Add(textName.Text, textValue.Text);

			ResetForm();
			LoadList();
			UpdateButtons();
		}

		private void ResetForm() {
			listItems.SelectedIndex = -1;
			textName.Text = "";
			textValue.Text = "";
			textName.Focus();
		}

		private void buttonClear_Click(object sender, System.EventArgs e) {
			ResetForm();
		}

		private void listItems_SelectedIndexChanged(object sender, System.EventArgs e) {
			if (listItems.SelectedIndex == -1) return;

			if (values.ContainsKey((string)listItems.SelectedItem)) {
				textName.Text = (string)listItems.SelectedItem;
				textValue.Text = (string)values[(string)listItems.SelectedItem];
			}

			UpdateButtons();
		}

		private void buttonDelete_Click(object sender, System.EventArgs e) {
			if (listItems.SelectedIndex == -1) return;

			if (values.ContainsKey((string)listItems.SelectedItem)) {
				values.Remove((string)listItems.SelectedItem);
			}

			LoadList();
		}

		private void textName_TextChanged(object sender, System.EventArgs e) {
			UpdateButtons();
		}
	}
}
