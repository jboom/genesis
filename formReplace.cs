using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formReplace.
	/// </summary>
	public class formReplace : System.Windows.Forms.Form
	{
		private readonly formMain parent;
	    private readonly Area _area;

		private System.Windows.Forms.Button buttonReplaceAll;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textFind;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textReplace;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkCase;
		private System.Windows.Forms.RadioButton radioAll;
		private System.Windows.Forms.RadioButton radioSelected;
		private System.Windows.Forms.Button buttonSelect;
		private System.Windows.Forms.CheckBox checkRE;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formReplace(formMain p, Area area)
		{
			InitializeComponent();

			parent = p;
		    _area = area;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonReplaceAll = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textFind = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textReplace = new System.Windows.Forms.TextBox();
			this.buttonClose = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkRE = new System.Windows.Forms.CheckBox();
			this.checkCase = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioSelected = new System.Windows.Forms.RadioButton();
			this.radioAll = new System.Windows.Forms.RadioButton();
			this.buttonSelect = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonReplaceAll
			// 
			this.buttonReplaceAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonReplaceAll.Location = new System.Drawing.Point(288, 8);
			this.buttonReplaceAll.Name = "buttonReplaceAll";
			this.buttonReplaceAll.Size = new System.Drawing.Size(80, 24);
			this.buttonReplaceAll.TabIndex = 0;
			this.buttonReplaceAll.Text = "Replace &All";
			this.buttonReplaceAll.Click += new System.EventHandler(this.buttonReplaceAll_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Find what:";
			// 
			// textFind
			// 
			this.textFind.Location = new System.Drawing.Point(80, 8);
			this.textFind.Name = "textFind";
			this.textFind.Size = new System.Drawing.Size(200, 20);
			this.textFind.TabIndex = 2;
			this.textFind.Text = "";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Replace with:";
			// 
			// textReplace
			// 
			this.textReplace.Location = new System.Drawing.Point(80, 32);
			this.textReplace.Name = "textReplace";
			this.textReplace.Size = new System.Drawing.Size(200, 20);
			this.textReplace.TabIndex = 4;
			this.textReplace.Text = "";
			// 
			// buttonClose
			// 
			this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonClose.Location = new System.Drawing.Point(288, 112);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(80, 24);
			this.buttonClose.TabIndex = 5;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkRE);
			this.groupBox1.Controls.Add(this.checkCase);
			this.groupBox1.Location = new System.Drawing.Point(8, 56);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(144, 80);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Options";
			// 
			// checkRE
			// 
			this.checkRE.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkRE.Location = new System.Drawing.Point(8, 48);
			this.checkRE.Name = "checkRE";
			this.checkRE.Size = new System.Drawing.Size(128, 16);
			this.checkRE.TabIndex = 1;
			this.checkRE.Text = "Regular expression";
			// 
			// checkCase
			// 
			this.checkCase.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkCase.Location = new System.Drawing.Point(8, 24);
			this.checkCase.Name = "checkCase";
			this.checkCase.Size = new System.Drawing.Size(104, 16);
			this.checkCase.TabIndex = 0;
			this.checkCase.Text = "Case sensitive";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioSelected);
			this.groupBox2.Controls.Add(this.radioAll);
			this.groupBox2.Location = new System.Drawing.Point(160, 56);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(120, 80);
			this.groupBox2.TabIndex = 7;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Search";
			// 
			// radioSelected
			// 
			this.radioSelected.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioSelected.Location = new System.Drawing.Point(8, 48);
			this.radioSelected.Name = "radioSelected";
			this.radioSelected.Size = new System.Drawing.Size(104, 16);
			this.radioSelected.TabIndex = 1;
			this.radioSelected.Text = "Selected rooms";
			// 
			// radioAll
			// 
			this.radioAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioAll.Location = new System.Drawing.Point(8, 24);
			this.radioAll.Name = "radioAll";
			this.radioAll.Size = new System.Drawing.Size(104, 16);
			this.radioAll.TabIndex = 0;
			this.radioAll.Text = "All rooms";
			// 
			// buttonSelect
			// 
			this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonSelect.Location = new System.Drawing.Point(288, 40);
			this.buttonSelect.Name = "buttonSelect";
			this.buttonSelect.Size = new System.Drawing.Size(80, 24);
			this.buttonSelect.TabIndex = 8;
			this.buttonSelect.Text = "Select All";
			this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
			// 
			// formReplace
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(378, 144);
			this.ControlBox = false;
			this.Controls.Add(this.buttonSelect);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.textReplace);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textFind);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonReplaceAll);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "formReplace";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Replace";
			this.Load += new System.EventHandler(this.formReplace_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonClose_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void formReplace_Load(object sender, System.EventArgs e) {
			if (parent.SelectedRooms.Count > 0) {
				radioSelected.Enabled = true;
				radioSelected.Checked = true;
			} else {
				radioSelected.Enabled = false;
				radioAll.Checked = true;
			}
		}

		private void buttonSelect_Click(object sender, System.EventArgs e) {
			System.Text.RegularExpressions.Regex rx;
			System.Text.RegularExpressions.Match ma;
			int c;
			bool updated;

			if (textFind.Text.Length == 0) return;

			parent.ClearSelected();

			rx = null;
			if (checkRE.Checked) {
				if (checkCase.Checked) {
					rx = new System.Text.RegularExpressions.Regex(textFind.Text, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				} else {
					rx = new System.Text.RegularExpressions.Regex(textFind.Text);
				}
			}

			c = 0;
			foreach (var r in _area.Rooms.Values) {
				updated = false;
                foreach (KeyValuePair<String,String> kv2 in r.properties) {
                    var s = kv2.Value;

                    if (checkRE.Checked) {
                        if (s.Length == 0) continue;
                        ma = rx.Match(s);

                        if (ma.Success) {
                            parent.ToggleSelection(r, false);
                            updated = true;
                        }
                    } else {
                        if (s.IndexOf(textFind.Text) > -1) {
                            parent.ToggleSelection(r, false);
                            updated = true;
                        }
                    }
                }

                foreach (KeyValuePair<String,String> DE2 in r.properties) {
                    var s = DE2.Value;

					if (checkRE.Checked) {
						if (s.Length == 0) continue;
						ma = rx.Match(s);

						if (ma.Success) {
							parent.ToggleSelection(r, false);
							updated = true;
						}
					} else {
						if (s.IndexOf(textFind.Text) > -1) {
							parent.ToggleSelection(r, false);
							updated = true;
						}
					}
				}
				if (updated) {
					c++;
				}
			}

			MessageBox.Show(c.ToString() + " rooms selected.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
			parent.Redraw(true);
		}

		private void buttonReplaceAll_Click(object sender, System.EventArgs e) {
			System.Text.RegularExpressions.Regex rx;
			System.Text.RegularExpressions.Match ma;
			int c;
			string s;
			bool changed;
			bool updated;
            Dictionary<String,String> newHT;

			if (textFind.Text.Length == 0) return;

			rx = null;
			if (checkRE.Checked) {
				if (checkCase.Checked) {
					rx = new System.Text.RegularExpressions.Regex(textFind.Text, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				} else {
					rx = new System.Text.RegularExpressions.Regex(textFind.Text);
				}
			}
            
			c = 0;
			foreach (var r in _area.Rooms.Values) {
                newHT = new Dictionary<String,String>();
                updated = false;
                foreach (KeyValuePair<String,String> DE2 in r.properties) {
                    changed = false;
					s = DE2.Value;

					if (checkRE.Checked) {
						if (s.Length == 0) continue;
						ma = rx.Match(s);
						if (ma.Success) {
							s = s.Replace(ma.Groups[0].Value, textReplace.Text);
							changed = true;
							updated = true;
						}
					} else {
						if (s.IndexOf(textFind.Text) > -1) {
							s = s.Replace(textFind.Text, textReplace.Text);
							changed = true;
							updated = true;
						}
					}
					if (changed) {
                        newHT.Add((string)DE2.Key, s);
					}
				}
				if (updated) {
                    foreach (KeyValuePair<String,String> DE2 in newHT) {
                        r.SetProperty((string)DE2.Key, (string)DE2.Value);
                    }
					c++;
					r.updated = true;
					parent.ValidateRoom(r.id);
				}
			}

			MessageBox.Show(c.ToString() + " rooms updated.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
			parent.Redraw(true);
		}
	}
}
