using System;
using System.Drawing;

namespace Genesis
{
	/// <summary>
	/// Summary description for classExit.
	/// </summary>
	public class Exit
	{
        enum Type {
            Common,
            Level,
            Custom
        }
		public int zTo, zFrom, idFrom, idTo;
		private string _dirFrom, _dirTo;
		public bool canFrom, canTo;
		public bool invFrom, invTo;
        private Type type;
        public bool visible;
		public System.Drawing.Point from, to, p1, p2, oP1, oP2;

        public string dirFrom {
            get { return _dirFrom; }
            set { _dirFrom = value; updateType(); }
        }

        public string dirTo {
            get { return _dirTo; }
            set { _dirTo = value; updateType(); }
        }

		public Exit() {
			zTo = 0;
			zFrom = 0;
			dirFrom = "";
			dirTo = "";
			idFrom = -1;
			idTo = -1;
			canFrom = true;
			canTo = true;
			invFrom = false;
			invTo = false;
            visible = false;
			from = new Point(0, 0);
			to = new Point(0, 0);
            p1 = new Point(0, 0);
			p2 = new Point(0, 0);
            oP1 = new Point(0, 0);
            oP2 = new Point(0, 0);
            type = Type.Custom;
		}

        private void updateType() {
            if (_dirFrom == "north" ||
                _dirFrom == "northeast" ||
                _dirFrom == "east" ||
                _dirFrom == "southeast" ||
                _dirFrom == "south" ||
                _dirFrom == "southwest" ||
                _dirFrom == "west" ||
                _dirFrom == "northwest") {
                type = Type.Common;
            } else if (_dirTo == "north" ||
                _dirTo == "northeast" ||
                _dirTo == "east" ||
                _dirTo == "southeast" ||
                _dirTo == "south" ||
                _dirTo == "southwest" ||
                _dirTo == "west" ||
                _dirTo == "northwest") {
                type = Type.Common;
            } else if (_dirFrom == "up" ||
                _dirFrom == "down") {
                type = Type.Level;
            } else if (_dirTo == "up" ||
                       _dirTo == "down") {
                type = Type.Level;
            } else {
                type = Type.Custom;
            }
        }

        public bool isCommon() {
            return (type == Type.Common);
        }

        public bool isLevel() {
            return (type == Type.Level);
        }

        public bool isCustom() {
            return (type == Type.Custom);
        }

        public bool isWithin(int x1, int y1, int x2, int y2) {
            if (
                p1.X > x1 && p1.X < x2 &&
                p1.Y > y1 && p1.Y < y2 ||
                p2.X > x1 && p2.X < x2 &&
                p2.Y > y1 && p2.Y < y2) {
                return true;
            } else return false;
        }
	}
}
