# README #

Genesis is an application that attempts to be a generic area building tool for text games. Its focus was to provide a graphical UI to lay out areas and to edit rooms individually using a room editor. Various tools are included to automate certain steps of the room detailing process (randomized room descriptions based on triggers/etc.)

Rooms could then be compiled into nearly any format needed, but was primarily designed to output LPC styled files (similar to C code) using templates.

# LICENSE #

At this point this is just shared for contribution purposes, and this project should not be distributed.