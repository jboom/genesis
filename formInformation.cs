using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formInformation.
	/// </summary>
	public class formInformation : System.Windows.Forms.Form
	{
		private readonly formMain parent;
	    private readonly Area _area;

		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelRoomsTotal;
		private System.Windows.Forms.Label labelRoomsCompleted;
		private System.Windows.Forms.Label labelRoomsLevels;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label labelExitsTotal;
		private System.Windows.Forms.Label labelExitsInvisible;
        private GroupBox groupBox3;
        private Label labelGroupsTotal;
        private Label label6;
        private GroupBox groupBox4;
        private Label labelObjectsTotal;
        private Label label7;
        private Label label8;
        private Label labelObjectsAssigned;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formInformation(formMain p, Area area)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;
		    _area = area;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelRoomsLevels = new System.Windows.Forms.Label();
            this.labelRoomsCompleted = new System.Windows.Forms.Label();
            this.labelRoomsTotal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelExitsInvisible = new System.Windows.Forms.Label();
            this.labelExitsTotal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelGroupsTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelObjectsTotal = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelObjectsAssigned = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClose.Location = new System.Drawing.Point(96, 243);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(80, 24);
            this.buttonClose.TabIndex = 0;
            this.buttonClose.Text = "Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelRoomsLevels);
            this.groupBox1.Controls.Add(this.labelRoomsCompleted);
            this.groupBox1.Controls.Add(this.labelRoomsTotal);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(168, 63);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rooms";
            // 
            // labelRoomsLevels
            // 
            this.labelRoomsLevels.AutoSize = true;
            this.labelRoomsLevels.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRoomsLevels.Location = new System.Drawing.Point(96, 42);
            this.labelRoomsLevels.Name = "labelRoomsLevels";
            this.labelRoomsLevels.Size = new System.Drawing.Size(14, 13);
            this.labelRoomsLevels.TabIndex = 5;
            this.labelRoomsLevels.Text = "0";
            // 
            // labelRoomsCompleted
            // 
            this.labelRoomsCompleted.AutoSize = true;
            this.labelRoomsCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRoomsCompleted.Location = new System.Drawing.Point(96, 29);
            this.labelRoomsCompleted.Name = "labelRoomsCompleted";
            this.labelRoomsCompleted.Size = new System.Drawing.Size(14, 13);
            this.labelRoomsCompleted.TabIndex = 4;
            this.labelRoomsCompleted.Text = "0";
            // 
            // labelRoomsTotal
            // 
            this.labelRoomsTotal.AutoSize = true;
            this.labelRoomsTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRoomsTotal.Location = new System.Drawing.Point(96, 16);
            this.labelRoomsTotal.Name = "labelRoomsTotal";
            this.labelRoomsTotal.Size = new System.Drawing.Size(14, 13);
            this.labelRoomsTotal.TabIndex = 3;
            this.labelRoomsTotal.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Levels";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Completed";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Total";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelExitsInvisible);
            this.groupBox2.Controls.Add(this.labelExitsTotal);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(8, 77);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(168, 51);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Exits";
            // 
            // labelExitsInvisible
            // 
            this.labelExitsInvisible.AutoSize = true;
            this.labelExitsInvisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExitsInvisible.Location = new System.Drawing.Point(96, 29);
            this.labelExitsInvisible.Name = "labelExitsInvisible";
            this.labelExitsInvisible.Size = new System.Drawing.Size(14, 13);
            this.labelExitsInvisible.TabIndex = 3;
            this.labelExitsInvisible.Text = "0";
            // 
            // labelExitsTotal
            // 
            this.labelExitsTotal.AutoSize = true;
            this.labelExitsTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExitsTotal.Location = new System.Drawing.Point(96, 16);
            this.labelExitsTotal.Name = "labelExitsTotal";
            this.labelExitsTotal.Size = new System.Drawing.Size(14, 13);
            this.labelExitsTotal.TabIndex = 2;
            this.labelExitsTotal.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Invisible";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Total";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelGroupsTotal);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(8, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(168, 36);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Groups";
            // 
            // labelGroupsTotal
            // 
            this.labelGroupsTotal.AutoSize = true;
            this.labelGroupsTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGroupsTotal.Location = new System.Drawing.Point(96, 16);
            this.labelGroupsTotal.Name = "labelGroupsTotal";
            this.labelGroupsTotal.Size = new System.Drawing.Size(14, 13);
            this.labelGroupsTotal.TabIndex = 1;
            this.labelGroupsTotal.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Total";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelObjectsAssigned);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.labelObjectsTotal);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(8, 176);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(168, 61);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Objects";
            // 
            // labelObjectsTotal
            // 
            this.labelObjectsTotal.AutoSize = true;
            this.labelObjectsTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObjectsTotal.Location = new System.Drawing.Point(96, 16);
            this.labelObjectsTotal.Name = "labelObjectsTotal";
            this.labelObjectsTotal.Size = new System.Drawing.Size(14, 13);
            this.labelObjectsTotal.TabIndex = 1;
            this.labelObjectsTotal.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Total";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Assigned";
            // 
            // labelObjectsAssigned
            // 
            this.labelObjectsAssigned.AutoSize = true;
            this.labelObjectsAssigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObjectsAssigned.Location = new System.Drawing.Point(96, 32);
            this.labelObjectsAssigned.Name = "labelObjectsAssigned";
            this.labelObjectsAssigned.Size = new System.Drawing.Size(14, 13);
            this.labelObjectsAssigned.TabIndex = 3;
            this.labelObjectsAssigned.Text = "0";
            // 
            // formInformation
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(186, 279);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formInformation";
            this.Text = "Area Statistics";
            this.Load += new System.EventHandler(this.formInformation_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void buttonClose_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void formInformation_Load(object sender, System.EventArgs e) {
			int c, c2, assignedObjectCount = 0;
			decimal percent;
			ArrayList levels;
			Room r;

			c = 0; // total
			c2 = 0; // invis
			foreach (Exit ex in _area.Exits) {
				if (ex.canFrom) c++;
				if (ex.canTo) c++;
				if (ex.invFrom) c2++;
				if (ex.invTo) c2++;
			}

			labelExitsTotal.Text = c.ToString();
			labelExitsInvisible.Text = c2.ToString();

			c = 0; // total
			c2 = 0; // completed
			levels = new ArrayList();
            foreach (KeyValuePair<int,Room> DE in _area.Rooms) {
				r = DE.Value;
				c++;
				if (!r.empty) c2++;
				if (!levels.Contains(r.z)) levels.Add(r.z);
                assignedObjectCount += r.objects.Count;
			}

			labelRoomsTotal.Text = c.ToString();
			percent = c2 * 100 / c;
			labelRoomsCompleted.Text = c2.ToString() + " (" + percent.ToString() + "%)";
			labelRoomsLevels.Text = levels.Count.ToString();
            labelGroupsTotal.Text = _area.Groups.Count.ToString();
            labelObjectsTotal.Text = _area.Objects.Count.ToString();
            labelObjectsAssigned.Text = assignedObjectCount.ToString();
		}
	}
}
