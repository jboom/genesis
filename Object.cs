using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Genesis {
    public class BaseObject {
        public Dictionary<String,String> properties;
        public List<Object> objects;

        public BaseObject() {
            properties = new Dictionary<String,String>();
            objects = new List<Object>();
        }

        private bool _SetProperty(string name, string val) {
            if (!properties.ContainsKey(name)) {
                properties.Add(name, val);
            } else {
                properties[name] = val;
            }
            return true;
        }

        public string GetProperty(string name) {
            int i;
            if ((i = name.IndexOf(">")) > -1 && name.Length > i + 1) {
                if (properties.ContainsKey(name.Substring(0, i))) {
                    Hashtable ht = Common.GetMultiHash(properties[name.Substring(0, i)]);
                    return (string)ht[name.Substring(i + 1)];
                }
            } else {
                if (properties.ContainsKey(name)) {
                    return properties[name];
                }
            }

            return "";
        }

        public bool SetProperty(string name, string value) {
            int i;
            if ((i = name.IndexOf(">")) > -1 && name.Length > i + 1) {
                AddItemToList(name.Substring(0, i), name.Substring(i + 1), value);
            } else {
                _SetProperty(name, value);
            }
            return true;
        }

        public void AddToProperty(string name, string value) {
            SetProperty(name, HasProperty(name) ? GetProperty(name) + " " + value : value);
        }

        public void RemoveProperty(string name) {
            if (properties.ContainsKey(name)) {
                properties.Remove(name);
            }
        }

        public bool HasProperty(string name) {
            int i;
            if ((i = name.IndexOf(">")) > -1 && name.Length > i + 1) {
                if (properties.ContainsKey(name.Substring(0, i))) {
                    Hashtable ht = Common.GetMultiHash(properties[name.Substring(0, i)]);
                    if (ht.ContainsKey(name.Substring(i + 1))) {
                        if (((string)ht[name.Substring(i + 1)]).Length > 0) return true; else return false;
                    } else return false;
                } else return false;
            } else {
                if (!properties.ContainsKey(name)) {
                    return false;
                }  else {
                    if (GetProperty(name).Length == 0) return false;
                }
            }
            return true;
        }

        public void AddItemToList(string list, string k, string v) {
            Hashtable ht;

            ht = Common.GetMultiHash(GetProperty(list));

			if (k == null) return;

            if (ht.ContainsKey(k)) {
                ht.Remove(k);
            }

            ht.Add(k, v);

            SetProperty(list, Common.GetMultiString(ht));
        }

        public void RemoveItemFromList(string list, string k) {
            Hashtable ht;

            ht = Common.GetMultiHash(GetProperty(list));

			if (k == null) return;

            if (ht.ContainsKey(k)) {
                ht.Remove(k);
            }

            SetProperty(list, Common.GetMultiString(ht));
        }

        public bool HasList(string name) {
            if (properties.ContainsKey(name)) return true; else return false;
        }

        public void ClearRoom() {
            //String oldProperties = properties["properties"];
            //String oldCustom = properties["custom"];
            //properties = new Dictionary<String, String>();
            //properties["custom"] = oldCustom;
            properties["items"] = "";
            properties["smells"] = "";
            properties["listens"] = "";
        }
    }

    public class GenesisObject : BaseObject {
        public string id;
        public Group Group;

        public GenesisObject() {
            id = "";
            Group = null;
        }
    }
}
