using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formOptions.
	/// </summary>
	public class formOptions : System.Windows.Forms.Form
	{
		private formMain parent;

		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.TabControl tabOptions;
		private System.Windows.Forms.TabPage tabGeneral;
		private System.Windows.Forms.TabPage tabCompile;
		private System.Windows.Forms.CheckBox checkDockEditor;
		private System.Windows.Forms.CheckBox checkAutosave;
		private System.Windows.Forms.CheckBox checkAddInvisible;
        private System.Windows.Forms.TabPage tabAdvanced;
		private System.Windows.Forms.RadioButton radioUnix;
		private System.Windows.Forms.RadioButton radioPC;
		private System.Windows.Forms.RadioButton radioMac;
		private System.Windows.Forms.ListBox listType;
        private System.Windows.Forms.Button buttonEdit;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelBackgroundColour;
        private System.Windows.Forms.ColorDialog colorPick;
		private System.Windows.Forms.CheckBox checkNotes;
        private CheckBox checkExclude;
        private TabPage tabFiles;
        private GroupBox groupBox2;
        private NumericUpDown numericOpacity;
        private Label labelGridColour;
        private Label label3;
        private Label label2;
        private Label label5;
        private Label label4;
        private ComboBox comboObjectLayout;
        private ComboBox comboRoomLayout;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formOptions(formMain p)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabOptions = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.labelGridColour = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericOpacity = new System.Windows.Forms.NumericUpDown();
            this.checkNotes = new System.Windows.Forms.CheckBox();
            this.labelBackgroundColour = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkAutosave = new System.Windows.Forms.CheckBox();
            this.checkDockEditor = new System.Windows.Forms.CheckBox();
            this.tabCompile = new System.Windows.Forms.TabPage();
            this.checkExclude = new System.Windows.Forms.CheckBox();
            this.checkAddInvisible = new System.Windows.Forms.CheckBox();
            this.tabFiles = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioUnix = new System.Windows.Forms.RadioButton();
            this.radioMac = new System.Windows.Forms.RadioButton();
            this.radioPC = new System.Windows.Forms.RadioButton();
            this.tabAdvanced = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.listType = new System.Windows.Forms.ListBox();
            this.colorPick = new System.Windows.Forms.ColorDialog();
            this.comboRoomLayout = new System.Windows.Forms.ComboBox();
            this.comboObjectLayout = new System.Windows.Forms.ComboBox();
            this.tabOptions.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericOpacity)).BeginInit();
            this.tabCompile.SuspendLayout();
            this.tabFiles.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabAdvanced.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSave.Location = new System.Drawing.Point(232, 270);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(80, 24);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCancel.Location = new System.Drawing.Point(12, 270);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 24);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabOptions
            // 
            this.tabOptions.Controls.Add(this.tabGeneral);
            this.tabOptions.Controls.Add(this.tabCompile);
            this.tabOptions.Controls.Add(this.tabFiles);
            this.tabOptions.Controls.Add(this.tabAdvanced);
            this.tabOptions.HotTrack = true;
            this.tabOptions.Location = new System.Drawing.Point(12, 12);
            this.tabOptions.Name = "tabOptions";
            this.tabOptions.SelectedIndex = 0;
            this.tabOptions.ShowToolTips = true;
            this.tabOptions.Size = new System.Drawing.Size(300, 252);
            this.tabOptions.TabIndex = 2;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.labelGridColour);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.numericOpacity);
            this.tabGeneral.Controls.Add(this.checkNotes);
            this.tabGeneral.Controls.Add(this.labelBackgroundColour);
            this.tabGeneral.Controls.Add(this.label1);
            this.tabGeneral.Controls.Add(this.checkAutosave);
            this.tabGeneral.Controls.Add(this.checkDockEditor);
            this.tabGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Size = new System.Drawing.Size(292, 226);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "General";
            this.tabGeneral.ToolTipText = "Options regarding the program itself.";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // labelGridColour
            // 
            this.labelGridColour.BackColor = System.Drawing.Color.White;
            this.labelGridColour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelGridColour.Location = new System.Drawing.Point(123, 120);
            this.labelGridColour.Name = "labelGridColour";
            this.labelGridColour.Size = new System.Drawing.Size(40, 16);
            this.labelGridColour.TabIndex = 12;
            this.labelGridColour.Click += new System.EventHandler(this.labelGridColour_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Grid Colour";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Editor Opacity";
            // 
            // numericOpacity
            // 
            this.numericOpacity.Location = new System.Drawing.Point(123, 139);
            this.numericOpacity.Name = "numericOpacity";
            this.numericOpacity.Size = new System.Drawing.Size(40, 20);
            this.numericOpacity.TabIndex = 9;
            this.numericOpacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkNotes
            // 
            this.checkNotes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkNotes.Location = new System.Drawing.Point(16, 60);
            this.checkNotes.Name = "checkNotes";
            this.checkNotes.Size = new System.Drawing.Size(272, 16);
            this.checkNotes.TabIndex = 8;
            this.checkNotes.Text = "Use notes field as a label";
            // 
            // labelBackgroundColour
            // 
            this.labelBackgroundColour.BackColor = System.Drawing.Color.White;
            this.labelBackgroundColour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelBackgroundColour.Location = new System.Drawing.Point(123, 98);
            this.labelBackgroundColour.Name = "labelBackgroundColour";
            this.labelBackgroundColour.Size = new System.Drawing.Size(40, 16);
            this.labelBackgroundColour.TabIndex = 3;
            this.labelBackgroundColour.Click += new System.EventHandler(this.labelBackgroundColour_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Background Ccolour";
            // 
            // checkAutosave
            // 
            this.checkAutosave.Checked = true;
            this.checkAutosave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAutosave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkAutosave.Location = new System.Drawing.Point(16, 38);
            this.checkAutosave.Name = "checkAutosave";
            this.checkAutosave.Size = new System.Drawing.Size(272, 16);
            this.checkAutosave.TabIndex = 1;
            this.checkAutosave.Text = "Autosave every 5 minutes";
            // 
            // checkDockEditor
            // 
            this.checkDockEditor.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkDockEditor.Location = new System.Drawing.Point(16, 16);
            this.checkDockEditor.Name = "checkDockEditor";
            this.checkDockEditor.Size = new System.Drawing.Size(272, 16);
            this.checkDockEditor.TabIndex = 0;
            this.checkDockEditor.Text = "Dock the room editor";
            // 
            // tabCompile
            // 
            this.tabCompile.Controls.Add(this.checkExclude);
            this.tabCompile.Controls.Add(this.checkAddInvisible);
            this.tabCompile.Location = new System.Drawing.Point(4, 22);
            this.tabCompile.Name = "tabCompile";
            this.tabCompile.Size = new System.Drawing.Size(292, 226);
            this.tabCompile.TabIndex = 1;
            this.tabCompile.Text = "Compile";
            this.tabCompile.ToolTipText = "Options regarding the compiling stage.";
            this.tabCompile.UseVisualStyleBackColor = true;
            // 
            // checkExclude
            // 
            this.checkExclude.AutoSize = true;
            this.checkExclude.Checked = true;
            this.checkExclude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkExclude.Location = new System.Drawing.Point(16, 38);
            this.checkExclude.Name = "checkExclude";
            this.checkExclude.Size = new System.Drawing.Size(176, 17);
            this.checkExclude.TabIndex = 2;
            this.checkExclude.Text = "Exclude rooms without changes";
            this.checkExclude.UseVisualStyleBackColor = true;
            // 
            // checkAddInvisible
            // 
            this.checkAddInvisible.Checked = true;
            this.checkAddInvisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAddInvisible.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkAddInvisible.Location = new System.Drawing.Point(16, 16);
            this.checkAddInvisible.Name = "checkAddInvisible";
            this.checkAddInvisible.Size = new System.Drawing.Size(272, 16);
            this.checkAddInvisible.TabIndex = 0;
            this.checkAddInvisible.Text = "Add invisible exits to exit list";
            // 
            // tabFiles
            // 
            this.tabFiles.Controls.Add(this.groupBox2);
            this.tabFiles.Location = new System.Drawing.Point(4, 22);
            this.tabFiles.Name = "tabFiles";
            this.tabFiles.Size = new System.Drawing.Size(292, 226);
            this.tabFiles.TabIndex = 3;
            this.tabFiles.Text = "Files";
            this.tabFiles.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioUnix);
            this.groupBox2.Controls.Add(this.radioMac);
            this.groupBox2.Controls.Add(this.radioPC);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(286, 94);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "File Type";
            // 
            // radioUnix
            // 
            this.radioUnix.AutoSize = true;
            this.radioUnix.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioUnix.Location = new System.Drawing.Point(19, 19);
            this.radioUnix.Name = "radioUnix";
            this.radioUnix.Size = new System.Drawing.Size(52, 18);
            this.radioUnix.TabIndex = 0;
            this.radioUnix.TabStop = true;
            this.radioUnix.Text = "Unix";
            // 
            // radioMac
            // 
            this.radioMac.AutoSize = true;
            this.radioMac.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioMac.Location = new System.Drawing.Point(19, 67);
            this.radioMac.Name = "radioMac";
            this.radioMac.Size = new System.Drawing.Size(52, 18);
            this.radioMac.TabIndex = 2;
            this.radioMac.Text = "Mac";
            // 
            // radioPC
            // 
            this.radioPC.AutoSize = true;
            this.radioPC.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioPC.Location = new System.Drawing.Point(19, 43);
            this.radioPC.Name = "radioPC";
            this.radioPC.Size = new System.Drawing.Size(75, 18);
            this.radioPC.TabIndex = 1;
            this.radioPC.Text = "Windows";
            // 
            // tabAdvanced
            // 
            this.tabAdvanced.Controls.Add(this.comboObjectLayout);
            this.tabAdvanced.Controls.Add(this.comboRoomLayout);
            this.tabAdvanced.Controls.Add(this.label5);
            this.tabAdvanced.Controls.Add(this.label4);
            this.tabAdvanced.Controls.Add(this.buttonEdit);
            this.tabAdvanced.Controls.Add(this.listType);
            this.tabAdvanced.Location = new System.Drawing.Point(4, 22);
            this.tabAdvanced.Name = "tabAdvanced";
            this.tabAdvanced.Size = new System.Drawing.Size(292, 226);
            this.tabAdvanced.TabIndex = 2;
            this.tabAdvanced.Text = "Advanced";
            this.tabAdvanced.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Default Object Editor Layout";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Default Room Editor Layout";
            // 
            // buttonEdit
            // 
            this.buttonEdit.AutoSize = true;
            this.buttonEdit.Enabled = false;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonEdit.Location = new System.Drawing.Point(157, 3);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(60, 25);
            this.buttonEdit.TabIndex = 2;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // listType
            // 
            this.listType.Items.AddRange(new object[] {
            "Common Properties",
            "Empty Properties",
            "Room Property Type List",
            "Object Property Type List",
            "Code Property List"});
            this.listType.Location = new System.Drawing.Point(3, 3);
            this.listType.Name = "listType";
            this.listType.Size = new System.Drawing.Size(148, 69);
            this.listType.TabIndex = 1;
            this.listType.SelectedIndexChanged += new System.EventHandler(this.listType_SelectedIndexChanged);
            // 
            // comboRoomLayout
            // 
            this.comboRoomLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboRoomLayout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRoomLayout.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRoomLayout.FormattingEnabled = true;
            this.comboRoomLayout.Location = new System.Drawing.Point(6, 91);
            this.comboRoomLayout.Name = "comboRoomLayout";
            this.comboRoomLayout.Size = new System.Drawing.Size(283, 23);
            this.comboRoomLayout.TabIndex = 8;
            // 
            // comboObjectLayout
            // 
            this.comboObjectLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboObjectLayout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboObjectLayout.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboObjectLayout.FormattingEnabled = true;
            this.comboObjectLayout.Location = new System.Drawing.Point(6, 130);
            this.comboObjectLayout.Name = "comboObjectLayout";
            this.comboObjectLayout.Size = new System.Drawing.Size(283, 23);
            this.comboObjectLayout.TabIndex = 9;
            // 
            // formOptions
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(324, 304);
            this.ControlBox = false;
            this.Controls.Add(this.tabOptions);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formOptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.formOptions_Load);
            this.tabOptions.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericOpacity)).EndInit();
            this.tabCompile.ResumeLayout(false);
            this.tabCompile.PerformLayout();
            this.tabFiles.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabAdvanced.ResumeLayout(false);
            this.tabAdvanced.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void buttonCancel_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void buttonSave_Click(object sender, System.EventArgs e) {
            parent.AddInvisible = checkAddInvisible.Checked;
            parent.AutoSave = checkAutosave.Checked;
            parent.DockEditor = checkDockEditor.Checked;
            parent.ExcludeOld = checkExclude.Checked;
            parent.NotesAsLabel = checkNotes.Checked;
            parent.EditorOpacity = (int)numericOpacity.Value;
            parent.BackgroundColour = labelBackgroundColour.BackColor;
            parent.GridColour = labelGridColour.BackColor;
            parent.options.DefaultRoomLayoutFile = comboRoomLayout.Text;
            parent.options.DefaultObjectLayoutFile = comboObjectLayout.Text;
            if (radioPC.Checked) {
                parent.FileType = "\r\n";
            } else if (radioMac.Checked) {
                parent.FileType = "\r";
            } else {
                parent.FileType = "\n";
            }

            parent.UpdateForm();
            
			this.Close();
		}

		private void buttonEdit_Click(object sender, System.EventArgs e) {
			formListEdit f;
			Dictionary<String,String> h;
			List<String> a, a2;
			Array l;

			if (listType.SelectedIndex == -1) return;

			switch (listType.SelectedIndex) {
				case 0:
					h = new Dictionary<String,String>();
					foreach (string s in parent.CommonProperties) {
						l = s.Split(':');
						h.Add((String)l.GetValue(0), (String)l.GetValue(1));
					}
					f = new formListEdit(h);
					f.Text = "Edit Common Properties";

					if (f.ShowDialog() == DialogResult.Cancel) return;

					a = new List<String>();
					foreach (KeyValuePair<String,String> DE in f.values) {
						a.Add((string)DE.Key + ":" + (string)DE.Value);

					}
					parent.CommonProperties = a;
					break;
				default:
					a = new List<String>();

					a2 = new List<String>();
					switch (listType.SelectedIndex) {
						case 1:
							a2 = parent.EmptyProperties;
							break;
						case 2:
							a2 = parent.RoomPropertyTypeList;
							break;
                        case 3:
                            a2 = parent.ObjectPropertyTypeList;
                            break;
                        case 4:
                            a2 = parent.CodePropertyList;
                            break;
                    }

					f = new formListEdit(a2);

					switch (listType.SelectedIndex) {
						case 1:
							f.Text = "Edit Empty Properties";
							break;
						case 2:
							f.Text = "Edit Room Property Type List";
							break;
                        case 3:
                            f.Text = "Edit Object Property Type List";
                            break;
                        case 4:
                            f.Text = "Edit Code Property List";
                            break;
                    }

					if (f.ShowDialog() == DialogResult.Cancel) return;

					a = new List<String>();
					foreach (KeyValuePair<String,String> DE in f.values) {
						a.Add((string)DE.Key);
					}

					switch (listType.SelectedIndex) {
                        case 0:
                            parent.CommonProperties = a;
                            break;
                        case 1:
							parent.EmptyProperties = a;
							break;
						case 2:
							parent.RoomPropertyTypeList = a;
							break;
                        case 3:
                            parent.ObjectPropertyTypeList = a;
                            break;
                        case 4:
                            parent.CodePropertyList = a;
                            break;
                    }
                    Properties.Settings.Default.Save();

					break;
			}
		}

		private void listType_SelectedIndexChanged(object sender, System.EventArgs e) {
			if (listType.SelectedIndex == -1) return;

			buttonEdit.Enabled = true;
		}

		private void labelBackgroundColour_Click(object sender, System.EventArgs e) {
			colorPick.Color = labelBackgroundColour.BackColor;

			if (colorPick.ShowDialog() == DialogResult.Cancel) {
				return;
			}

			labelBackgroundColour.BackColor = colorPick.Color;
		}

        private void formOptions_Load(object sender, EventArgs e) {
            //LoadLayouts();
            Common.LoadLayouts(comboRoomLayout, parent);
            Common.LoadLayouts(comboObjectLayout, parent);
            Common.SetComboSelected(comboObjectLayout, parent.options.DefaultObjectLayoutFile);
            Common.SetComboSelected(comboRoomLayout, parent.options.DefaultRoomLayoutFile);
            checkAddInvisible.Checked = parent.AddInvisible;
            checkAutosave.Checked = parent.AutoSave;
            checkDockEditor.Checked = parent.DockEditor;
            checkExclude.Checked = parent.ExcludeOld;
            checkNotes.Checked = parent.NotesAsLabel;
            numericOpacity.Value = parent.EditorOpacity;
            labelBackgroundColour.BackColor = parent.BackgroundColour;
            labelGridColour.BackColor = parent.GridColour;
            //comboRoomLayout.Text = parent.options.DefaultRoomLayoutFile;
            Common.SetComboSelected(comboRoomLayout, parent.options.DefaultRoomLayoutFile);
            //comboObjectLayout.Text = parent.options.DefaultObjectLayoutFile;
            switch (parent.FileType) {
                case "\\r\\n":
                    radioPC.Checked = true;
                    break;
                case "\\r":
                    radioMac.Checked = true;
                    break;
                default:
                case "\\n":
                    radioUnix.Checked = true;
                    break;
            }
        }

        private void LoadLayouts() {
            comboRoomLayout.SuspendLayout();
            comboRoomLayout.Items.Clear();
            foreach (string template in Layouts.Names) {
                comboRoomLayout.Items.Add(template);
            }
            comboRoomLayout.ResumeLayout();

            comboObjectLayout.SuspendLayout();
            comboObjectLayout.Items.Clear();
            foreach (string template in Layouts.Names) {
                comboObjectLayout.Items.Add(template);
            }
            comboObjectLayout.ResumeLayout();
        }

        private void labelGridColour_Click(object sender, EventArgs e) {
            colorPick.Color = labelGridColour.BackColor;

            if (colorPick.ShowDialog() == DialogResult.Cancel) {
                return;
            }

            labelGridColour.BackColor = colorPick.Color;
        }

        private void button1_Click(object sender, EventArgs e) {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Title = "Select a Layout File";
            dialog.Filter = "Genesis Layout Files (*.glf)|*.glf";
            dialog.DefaultExt = "glf";
            dialog.CheckFileExists = true;

            if (dialog.ShowDialog() != DialogResult.Cancel) {
                //textDefaultRoomLayoutFile.Text = dialog.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Title = "Select a Layout File";
            dialog.Filter = "Genesis Layout Files (*.glf)|*.glf";
            dialog.DefaultExt = "glf";
            dialog.CheckFileExists = true;

            if (dialog.ShowDialog() != DialogResult.Cancel) {
                //textDefaultObjectLayoutFile.Text = dialog.FileName;
            }
        }
	}
}
