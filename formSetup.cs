using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formSetup.
	/// </summary>
	public class formSetup : System.Windows.Forms.Form
	{
		private readonly formMain parent;
	    private readonly Area _area;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textFolder;
        private System.Windows.Forms.TextBox textFileName;
		private System.Windows.Forms.OpenFileDialog fileOpen;
		private System.Windows.Forms.FolderBrowserDialog folderOpen;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonCancel;
        private Label label4;
        private TextBox textIDAdd;
        private Label label5;
        private DataGridView gridConstants;
        private Label label6;
        private DataGridViewTextBoxColumn columnName;
        private DataGridViewTextBoxColumn columnValue;
        private ComboBox comboTemplate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formSetup(formMain p, Area area) {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;
		    _area = area;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonSave = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textFolder = new System.Windows.Forms.TextBox();
			this.textFileName = new System.Windows.Forms.TextBox();
			this.fileOpen = new System.Windows.Forms.OpenFileDialog();
			this.folderOpen = new System.Windows.Forms.FolderBrowserDialog();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.textIDAdd = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.gridConstants = new System.Windows.Forms.DataGridView();
			this.columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.columnValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label6 = new System.Windows.Forms.Label();
			this.comboTemplate = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.gridConstants)).BeginInit();
			this.SuspendLayout();
			// 
			// buttonSave
			// 
			this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonSave.Location = new System.Drawing.Point(272, 271);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(80, 24);
			this.buttonSave.TabIndex = 6;
			this.buttonSave.Text = "Save";
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Location = new System.Drawing.Point(8, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(102, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Room Output Folder";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label2.Location = new System.Drawing.Point(8, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Template";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label3.Location = new System.Drawing.Point(8, 67);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(49, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Filename";
			// 
			// textFolder
			// 
			this.textFolder.Location = new System.Drawing.Point(116, 9);
			this.textFolder.Name = "textFolder";
			this.textFolder.Size = new System.Drawing.Size(236, 20);
			this.textFolder.TabIndex = 0;
			// 
			// textFileName
			// 
			this.textFileName.Location = new System.Drawing.Point(116, 64);
			this.textFileName.Name = "textFileName";
			this.textFileName.Size = new System.Drawing.Size(236, 20);
			this.textFileName.TabIndex = 2;
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonCancel.Location = new System.Drawing.Point(184, 271);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(80, 24);
			this.buttonCancel.TabIndex = 5;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(8, 93);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(56, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Add To ID";
			// 
			// textIDAdd
			// 
			this.textIDAdd.Location = new System.Drawing.Point(116, 90);
			this.textIDAdd.Name = "textIDAdd";
			this.textIDAdd.Size = new System.Drawing.Size(63, 20);
			this.textIDAdd.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(185, 93);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(124, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "(Use with VNUM ranges)";
			// 
			// gridConstants
			// 
			this.gridConstants.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.gridConstants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridConstants.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnName,
            this.columnValue});
			this.gridConstants.Location = new System.Drawing.Point(12, 133);
			this.gridConstants.Name = "gridConstants";
			this.gridConstants.Size = new System.Drawing.Size(340, 132);
			this.gridConstants.TabIndex = 4;
			// 
			// columnName
			// 
			this.columnName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.columnName.HeaderText = "Name";
			this.columnName.Name = "columnName";
			this.columnName.Width = 60;
			// 
			// columnValue
			// 
			this.columnValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.columnValue.HeaderText = "Value";
			this.columnValue.Name = "columnValue";
			this.columnValue.Width = 59;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(8, 114);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(54, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "Constants";
			// 
			// comboTemplate
			// 
			this.comboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboTemplate.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboTemplate.FormattingEnabled = true;
			this.comboTemplate.Location = new System.Drawing.Point(116, 35);
			this.comboTemplate.Name = "comboTemplate";
			this.comboTemplate.Size = new System.Drawing.Size(236, 23);
			this.comboTemplate.TabIndex = 1;
			// 
			// formSetup
			// 
			this.AcceptButton = this.buttonSave;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(364, 307);
			this.Controls.Add(this.comboTemplate);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.gridConstants);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textIDAdd);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.textFileName);
			this.Controls.Add(this.textFolder);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonSave);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "formSetup";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Area Setup";
			this.Load += new System.EventHandler(this.formSetup_Load);
			((System.ComponentModel.ISupportInitialize)(this.gridConstants)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private void buttonBrowseTemplate_Click(object sender, System.EventArgs e) {
			fileOpen.AddExtension = true;
			fileOpen.CheckFileExists = true;
			fileOpen.DefaultExt = Common.EXT_TEMPLATE;
			fileOpen.Filter = "Template files (*" + Common.EXT_TEMPLATE + ")|*" + Common.EXT_TEMPLATE;
			fileOpen.Multiselect = false;
			fileOpen.ShowReadOnly = false;
			fileOpen.ValidateNames = true;

			if (fileOpen.ShowDialog() == DialogResult.Cancel) {
				return;
			}

//			textTemplate.Text = fileOpen.FileName;
		}

		private void buttonBrowseFolder_Click(object sender, System.EventArgs e) {
			folderOpen.Description = "Select the folder where the room files will go.";
            folderOpen.SelectedPath = textFolder.Text;
			folderOpen.ShowNewFolderButton = true;

			if (folderOpen.ShowDialog() == DialogResult.Cancel) {
				return;
			}

			textFolder.Text = folderOpen.SelectedPath;
		}

		private void buttonCancel_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void buttonSave_Click(object sender, System.EventArgs e) {
			_area.RoomFolderName = textFolder.Text;
			_area.DefaultRoomFileNamePrefix = textFileName.Text;
            _area.DefaultRoomTemplate = comboTemplate.Text;
            _area.RoomIdOffset = Convert.ToInt32(textIDAdd.Text);
			parent.Saved = false;
            _area.Constants.Clear();
            foreach (DataGridViewRow row in gridConstants.Rows) {
                if (row.Cells[0].Value == null) continue;
                _area.Constants.Add((string)row.Cells[0].Value, (string)row.Cells[1].Value);
            }

			Close();
		}

		private void formSetup_Load(object sender, System.EventArgs e) {
            Common.LoadTemplates(comboTemplate, parent, null);
			textFolder.Text = _area.RoomFolderName;
            Common.SetComboSelected(comboTemplate, _area.DefaultRoomTemplate);
			textFileName.Text = _area.DefaultRoomFileNamePrefix;
            textIDAdd.Text = _area.RoomIdOffset.ToString();
            gridConstants.Rows.Clear();
            foreach (KeyValuePair<string,string> KV in _area.Constants) {
                var row = new DataGridViewRow();
                row.CreateCells(gridConstants);
                row.Cells[0].Value = KV.Key;
                row.Cells[1].Value = KV.Value;
                gridConstants.Rows.Add(row);
            }
		}
	}
}
