using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace Genesis {
    public partial class formLayouts : Form {
        private DetailLayout currentLayout = null;
        private bool saved = true;
        private string currentFile = "";

        public formLayouts() {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e) {
            if (!saved) {
                if (MessageBox.Show("The layout has not been saved yet, are you sure you want to close this window?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    return;
                }
            }
            Layouts.Reload();
            Close();
        }

        private void buttonLoad_Click(object sender, EventArgs e) {
            ToolStripMenuItem item;

            menuLoadLayout.Items.Clear();
            DirectoryInfo dirList = new DirectoryInfo(Common.GetLayoutsPath());
            foreach (FileInfo file in dirList.GetFiles()) {
                if (file.Extension == Common.EXT_LAYOUT) {
                    item = new ToolStripMenuItem();
                    item.Text = file.Name.Substring(0, file.Name.Length - Common.EXT_LAYOUT.Length);
                    item.Click += new EventHandler(item_Click);
                    menuLoadLayout.Items.Add(item);
                }
            }
            menuLoadLayout.Show(buttonLoad, new Point(0, 0), ToolStripDropDownDirection.AboveRight);
        }

        void item_Click(object sender, EventArgs e) {
            string name = ((ToolStripMenuItem)sender).Text;
            currentFile = name;
            name = Common.GetLayoutsPath() + name + Common.EXT_LAYOUT;
            LoadLayout(name);
            saved = true;
        }

        private void LoadLayout(String filename) {
            /*
            try {
                XmlSerializer xs = new XmlSerializer(typeof(DetailLayout));
                FileStream fs = new FileStream(filename, FileMode.Open);
                currentLayout = (DetailLayout)xs.Deserialize(fs);
                fs.Close();
            } catch (Exception e) {
                Console.WriteLine("Error loading layout file: " + filename);
                Console.WriteLine(e.Message);
                currentLayout = null;
            }
            */
            currentLayout = DetailLayout.Load(filename);
            UpdateList();
        }

        private void UpdateList() {
            DataGridViewRow row;

            listFields.Rows.Clear();
            if (currentLayout == null) return;
            foreach (DetailField field in currentLayout) {
                row = new DataGridViewRow();
                row.CreateCells(listFields);
                row.Cells[0].Value = field.Name;
                row.Cells[1].Value = field.getTypeName();
                row.Tag = field;
                listFields.Rows.Add(row);
            }
            if (currentLayout.Count > 0) listFields.Rows[0].Selected = false;
            UpdateButtons();
        }

        private void UpdateButtons() {
            buttonNew.Enabled = true;
            if (listFields.SelectedRows.Count == 0) {
                buttonDelete.Enabled = false;
                buttonMoveUp.Enabled = false;
                buttonMoveDown.Enabled = false;
            } else {
                if (listFields.Rows.Count > 0) {
                    if (listFields.SelectedRows[0].Index == 0) {
                        buttonMoveUp.Enabled = false;
                    } else {
                        buttonMoveUp.Enabled = true;
                    }
                    if (listFields.SelectedRows[0].Index == listFields.Rows.Count - 1) {
                        buttonMoveDown.Enabled = false;
                    } else {
                        buttonMoveDown.Enabled = true;
                    }
                }
                buttonDelete.Enabled = true;
            }
            if (textLabel.Text.Length > 0 && textSource.Text.Length > 0 && comboType.SelectedIndex > -1) {
                buttonSaveField.Enabled = true;
            } else {
                buttonSaveField.Enabled = false;
            }
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateButtons();
            numericHeight.Visible = false;
            labelHeight.Visible = false;
            labelMinimum.Visible = false;
            labelMaximum.Visible = false;
            numericMinimum.Visible = false;
            numericMaximum.Visible = false;
            labelValues.Visible = false;
            gridValues.Visible = false;
            switch (comboType.Text) {
                case "Text":
                    break;
                case "Text Area":
                    numericHeight.Visible = true;
                    labelHeight.Visible = true;
                    break;
                case "Number":
                    labelMinimum.Visible = true;
                    labelMaximum.Visible = true;
                    numericMinimum.Visible = true;
                    numericMaximum.Visible = true;
                    break;
                case "Dropdown":
                    labelValues.Visible = true;
                    gridValues.Visible = true;
                    break;
            }
        }

        private void textLabel_TextChanged(object sender, EventArgs e) {
            UpdateButtons();
        }

        private void textSource_TextChanged(object sender, EventArgs e) {
            UpdateButtons();
        }

        private void formLayouts_Load(object sender, EventArgs e) {
            currentLayout = new DetailLayout();
            UpdateButtons();
        }

        private void ClearFields() {
            comboType.SelectedIndex = -1;
            textLabel.Text = "";
            textSource.Text = "";
            listFields.CurrentCell = null;
        }

        private void listFields_SelectionChanged(object sender, EventArgs e) {
            if (listFields.SelectedRows.Count == 0) {
                ClearFields();
                return;
            }

            DetailField field = (DetailField)listFields.SelectedRows[0].Tag;

            textLabel.Text = field.Name;
            textSource.Text = field.Source;
            comboType.SelectedIndex = -1;
            switch (field.getTypeName()) {
                case "Text":
                    comboType.SelectedIndex = 0;
                    break;
                case "Text Area":
                    comboType.SelectedIndex = 1;
                    numericHeight.Value = ((FieldTextArea)field).Height;
                    break;
                case "Number":
                    comboType.SelectedIndex = 2;
                    numericMinimum.Value = ((FieldNumber)field).Min;
                    numericMaximum.Value = ((FieldNumber)field).Max;
                    break;
                case "Checkbox":
                    comboType.SelectedIndex = 3;
                    break;
                case "Dropdown":
                    comboType.SelectedIndex = 4;
                    FieldDropdown tempField = (FieldDropdown)field;
                    gridValues.Rows.Clear();
                    DataGridViewRow row;
                    foreach (string str in tempField.Values) {
                        row = new DataGridViewRow();
                        row.CreateCells(gridValues);
                        row.Cells[0].Value = str;
                        gridValues.Rows.Add(row);
                    }
                    break;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            if (listFields.SelectedRows.Count == 0) return;

            currentLayout.Remove((DetailField)listFields.SelectedRows[0].Tag);

            UpdateList();
        }

        private void buttonSaveField_Click(object sender, EventArgs e) {
            DetailField field = null;

            if (listFields.SelectedRows.Count > 0) {
                field = (DetailField)listFields.SelectedRows[0].Tag;
            } else {
                switch (comboType.SelectedIndex) {
                    case 0:
                        field = new FieldText();
                        break;
                    case 1:
                        field = new FieldTextArea();
                        break;
                    case 2:
                        field = new FieldNumber();
                        break;
                    case 3:
                        field = new FieldCheckbox();
                        break;
                    case 4:
                        field = new FieldDropdown();
                        break;
                    default:
                        return;
                }
            }


            switch (comboType.SelectedIndex) {
                case 0:
                    break;
                case 1:
                    ((FieldTextArea)field).Height = (int)numericHeight.Value;
                    break;
                case 2:
                    ((FieldNumber)field).Min = (int)numericMinimum.Value;
                    ((FieldNumber)field).Max = (int)numericMaximum.Value;
                    break;
                case 3:
                    break;
                case 4:
                    FieldDropdown tempField = (FieldDropdown)field;
                    tempField.Values = new List<string>();
                    tempField.Values.Clear();
                    foreach (DataGridViewRow row in gridValues.Rows) {
                        if (((string)row.Cells[0].Value).Length == 0) continue;
                        tempField.Values.Add((string)row.Cells[0].Value);
                    }
                    break;
                default:
                    return;
            }

            field.Name = textLabel.Text;
            field.Source = textSource.Text;

            if (listFields.SelectedRows.Count == 0) {
                currentLayout.Add(field);
            }

            ClearFields();
            UpdateList();
            saved = false;
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            ClearFields();
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            string filename;
            formInput input = new formInput("Please enter a name for the layout", currentFile);

            if (input.ShowDialog() == DialogResult.Cancel) {
                return;
            }

            filename = Common.GetLayoutsPath() + input.Value + Common.EXT_LAYOUT;

            if (File.Exists(filename)) {
                if (MessageBox.Show("A layout with this name already exists, are you sure you want to overwrite it?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    return;
                }
            }

            SaveLayout(filename, currentLayout);
            currentFile = input.Value;
            saved = true;
        }

        private void SaveLayout(String filename, DetailLayout layout) {
            /*
            try {
                XmlSerializer xs = new XmlSerializer(typeof(DetailLayout));
                TextWriter tw = new StreamWriter(filename);
                xs.Serialize(tw, layout);
                tw.Close();
            } catch (Exception e) {
                Log.error(e);
            }
             */
            DetailLayout.Save(filename, layout);
        }

        private void buttonMoveUp_Click(object sender, EventArgs e) {
            int i;
            DetailField field1, field2;

            if (listFields.SelectedRows.Count == 0) return;
            i = listFields.SelectedRows[0].Index;
            if (i == 0) return;
            field1 = currentLayout[i];
            field2 = currentLayout[i - 1];
            currentLayout.RemoveAt(i);
            currentLayout.RemoveAt(i - 1);
            currentLayout.Insert(i - 1, field1);
            currentLayout.Insert(i, field2);

            UpdateList();
            listFields.Rows[i - 1].Selected = true;
            saved = false;
        }

        private void buttonMoveDown_Click(object sender, EventArgs e) {
            int i;
            DetailField field1, field2;

            if (listFields.SelectedRows.Count == 0) return;
            i = listFields.SelectedRows[0].Index;
            if (i == listFields.Rows.Count - 1) return;
            field1 = currentLayout[i];
            field2 = currentLayout[i + 1];
            currentLayout.RemoveAt(i);
            currentLayout.RemoveAt(i);
            currentLayout.Insert(i, field1);
            currentLayout.Insert(i, field2);

            UpdateList();
            listFields.Rows[i + 1].Selected = true;
            saved = false;
        }
    }
}