namespace Genesis {
    partial class formLayouts {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formLayouts));
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonLoad = new System.Windows.Forms.Button();
			this.comboType = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonNew = new System.Windows.Forms.Button();
			this.buttonSaveField = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.textLabel = new System.Windows.Forms.TextBox();
			this.textSource = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.listFields = new System.Windows.Forms.DataGridView();
			this.columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.columnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.buttonDelete = new System.Windows.Forms.Button();
			this.tableExtras = new System.Windows.Forms.TableLayoutPanel();
			this.gridValues = new System.Windows.Forms.DataGridView();
			this.columnValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.labelHeight = new System.Windows.Forms.Label();
			this.labelMinimum = new System.Windows.Forms.Label();
			this.labelMaximum = new System.Windows.Forms.Label();
			this.numericMinimum = new System.Windows.Forms.NumericUpDown();
			this.numericMaximum = new System.Windows.Forms.NumericUpDown();
			this.numericHeight = new System.Windows.Forms.NumericUpDown();
			this.labelValues = new System.Windows.Forms.Label();
			this.menuLoadLayout = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.buttonMoveUp = new System.Windows.Forms.Button();
			this.buttonMoveDown = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.listFields)).BeginInit();
			this.tableExtras.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridValues)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericMinimum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericMaximum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericHeight)).BeginInit();
			this.SuspendLayout();
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonClose.Location = new System.Drawing.Point(435, 375);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(75, 23);
			this.buttonClose.TabIndex = 6;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = true;
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSave.Location = new System.Drawing.Point(354, 375);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(75, 23);
			this.buttonSave.TabIndex = 5;
			this.buttonSave.Text = "Save";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonLoad
			// 
			this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonLoad.Location = new System.Drawing.Point(273, 375);
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.Size = new System.Drawing.Size(75, 23);
			this.buttonLoad.TabIndex = 4;
			this.buttonLoad.Text = "Load";
			this.buttonLoad.UseVisualStyleBackColor = true;
			this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
			// 
			// comboType
			// 
			this.comboType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.comboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboType.FormattingEnabled = true;
			this.comboType.Items.AddRange(new object[] {
            "Text",
            "Text Area",
            "Number",
            "Checkbox",
            "Dropdown"});
			this.comboType.Location = new System.Drawing.Point(78, 3);
			this.comboType.Name = "comboType";
			this.comboType.Size = new System.Drawing.Size(203, 21);
			this.comboType.TabIndex = 1;
			this.comboType.SelectedIndexChanged += new System.EventHandler(this.comboType_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(31, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Type";
			// 
			// buttonNew
			// 
			this.buttonNew.Enabled = false;
			this.buttonNew.Location = new System.Drawing.Point(226, 12);
			this.buttonNew.Name = "buttonNew";
			this.buttonNew.Size = new System.Drawing.Size(75, 23);
			this.buttonNew.TabIndex = 1;
			this.buttonNew.Text = "New";
			this.buttonNew.UseVisualStyleBackColor = true;
			this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
			// 
			// buttonSaveField
			// 
			this.buttonSaveField.Enabled = false;
			this.buttonSaveField.Location = new System.Drawing.Point(388, 12);
			this.buttonSaveField.Name = "buttonSaveField";
			this.buttonSaveField.Size = new System.Drawing.Size(75, 23);
			this.buttonSaveField.TabIndex = 3;
			this.buttonSaveField.Text = "Save";
			this.buttonSaveField.UseVisualStyleBackColor = true;
			this.buttonSaveField.Click += new System.EventHandler(this.buttonSaveField_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 27);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(33, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Label";
			// 
			// textLabel
			// 
			this.textLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.textLabel.Location = new System.Drawing.Point(78, 30);
			this.textLabel.Name = "textLabel";
			this.textLabel.Size = new System.Drawing.Size(203, 20);
			this.textLabel.TabIndex = 3;
			this.textLabel.TextChanged += new System.EventHandler(this.textLabel_TextChanged);
			// 
			// textSource
			// 
			this.textSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.textSource.Location = new System.Drawing.Point(78, 56);
			this.textSource.Name = "textSource";
			this.textSource.Size = new System.Drawing.Size(203, 20);
			this.textSource.TabIndex = 5;
			this.textSource.TextChanged += new System.EventHandler(this.textSource_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 53);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Source";
			// 
			// listFields
			// 
			this.listFields.AllowUserToAddRows = false;
			this.listFields.AllowUserToDeleteRows = false;
			this.listFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.listFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.listFields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnName,
            this.columnType});
			this.listFields.Location = new System.Drawing.Point(12, 12);
			this.listFields.Name = "listFields";
			this.listFields.RowHeadersVisible = false;
			this.listFields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.listFields.Size = new System.Drawing.Size(208, 357);
			this.listFields.TabIndex = 0;
			this.listFields.SelectionChanged += new System.EventHandler(this.listFields_SelectionChanged);
			// 
			// columnName
			// 
			this.columnName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.columnName.HeaderText = "Name";
			this.columnName.Name = "columnName";
			this.columnName.Width = 60;
			// 
			// columnType
			// 
			this.columnType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.columnType.HeaderText = "Type";
			this.columnType.Name = "columnType";
			this.columnType.Width = 56;
			// 
			// buttonDelete
			// 
			this.buttonDelete.Enabled = false;
			this.buttonDelete.Location = new System.Drawing.Point(307, 12);
			this.buttonDelete.Name = "buttonDelete";
			this.buttonDelete.Size = new System.Drawing.Size(75, 23);
			this.buttonDelete.TabIndex = 2;
			this.buttonDelete.Text = "Delete";
			this.buttonDelete.UseVisualStyleBackColor = true;
			this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
			// 
			// tableExtras
			// 
			this.tableExtras.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tableExtras.AutoSize = true;
			this.tableExtras.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableExtras.ColumnCount = 2;
			this.tableExtras.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
			this.tableExtras.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableExtras.Controls.Add(this.gridValues, 1, 6);
			this.tableExtras.Controls.Add(this.labelHeight, 0, 3);
			this.tableExtras.Controls.Add(this.label1, 0, 0);
			this.tableExtras.Controls.Add(this.comboType, 1, 0);
			this.tableExtras.Controls.Add(this.label2, 0, 1);
			this.tableExtras.Controls.Add(this.textSource, 1, 2);
			this.tableExtras.Controls.Add(this.label3, 0, 2);
			this.tableExtras.Controls.Add(this.textLabel, 1, 1);
			this.tableExtras.Controls.Add(this.labelMinimum, 0, 4);
			this.tableExtras.Controls.Add(this.labelMaximum, 0, 5);
			this.tableExtras.Controls.Add(this.numericMinimum, 1, 4);
			this.tableExtras.Controls.Add(this.numericMaximum, 1, 5);
			this.tableExtras.Controls.Add(this.numericHeight, 1, 3);
			this.tableExtras.Controls.Add(this.labelValues, 0, 6);
			this.tableExtras.Location = new System.Drawing.Point(229, 41);
			this.tableExtras.Name = "tableExtras";
			this.tableExtras.RowCount = 7;
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableExtras.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableExtras.Size = new System.Drawing.Size(284, 257);
			this.tableExtras.TabIndex = 14;
			// 
			// gridValues
			// 
			this.gridValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridValues.ColumnHeadersVisible = false;
			this.gridValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnValue});
			this.gridValues.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridValues.Location = new System.Drawing.Point(78, 160);
			this.gridValues.Name = "gridValues";
			this.gridValues.RowHeadersVisible = false;
			this.gridValues.Size = new System.Drawing.Size(203, 94);
			this.gridValues.TabIndex = 15;
			// 
			// columnValue
			// 
			this.columnValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.columnValue.HeaderText = "Value";
			this.columnValue.Name = "columnValue";
			// 
			// labelHeight
			// 
			this.labelHeight.AutoSize = true;
			this.labelHeight.Location = new System.Drawing.Point(3, 79);
			this.labelHeight.Name = "labelHeight";
			this.labelHeight.Size = new System.Drawing.Size(38, 13);
			this.labelHeight.TabIndex = 6;
			this.labelHeight.Text = "Height";
			this.labelHeight.Visible = false;
			// 
			// labelMinimum
			// 
			this.labelMinimum.AutoSize = true;
			this.labelMinimum.Location = new System.Drawing.Point(3, 105);
			this.labelMinimum.Name = "labelMinimum";
			this.labelMinimum.Size = new System.Drawing.Size(48, 13);
			this.labelMinimum.TabIndex = 8;
			this.labelMinimum.Text = "Minimum";
			this.labelMinimum.Visible = false;
			// 
			// labelMaximum
			// 
			this.labelMaximum.AutoSize = true;
			this.labelMaximum.Location = new System.Drawing.Point(3, 131);
			this.labelMaximum.Name = "labelMaximum";
			this.labelMaximum.Size = new System.Drawing.Size(51, 13);
			this.labelMaximum.TabIndex = 10;
			this.labelMaximum.Text = "Maximum";
			this.labelMaximum.Visible = false;
			// 
			// numericMinimum
			// 
			this.numericMinimum.Location = new System.Drawing.Point(78, 108);
			this.numericMinimum.Minimum = new decimal(new int[] {
            1241513983,
            370409800,
            542101,
            -2147483648});
			this.numericMinimum.Name = "numericMinimum";
			this.numericMinimum.Size = new System.Drawing.Size(120, 20);
			this.numericMinimum.TabIndex = 9;
			this.numericMinimum.Visible = false;
			// 
			// numericMaximum
			// 
			this.numericMaximum.Location = new System.Drawing.Point(78, 134);
			this.numericMaximum.Maximum = new decimal(new int[] {
            1241513983,
            370409800,
            542101,
            0});
			this.numericMaximum.Name = "numericMaximum";
			this.numericMaximum.Size = new System.Drawing.Size(120, 20);
			this.numericMaximum.TabIndex = 11;
			this.numericMaximum.Visible = false;
			// 
			// numericHeight
			// 
			this.numericHeight.Location = new System.Drawing.Point(78, 82);
			this.numericHeight.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numericHeight.Name = "numericHeight";
			this.numericHeight.Size = new System.Drawing.Size(120, 20);
			this.numericHeight.TabIndex = 7;
			this.numericHeight.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numericHeight.Visible = false;
			// 
			// labelValues
			// 
			this.labelValues.AutoSize = true;
			this.labelValues.Location = new System.Drawing.Point(3, 157);
			this.labelValues.Name = "labelValues";
			this.labelValues.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
			this.labelValues.Size = new System.Drawing.Size(39, 18);
			this.labelValues.TabIndex = 16;
			this.labelValues.Text = "Values";
			// 
			// menuLoadLayout
			// 
			this.menuLoadLayout.Name = "menuLoadLayout";
			this.menuLoadLayout.Size = new System.Drawing.Size(61, 4);
			// 
			// buttonMoveUp
			// 
			this.buttonMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonMoveUp.Enabled = false;
			this.buttonMoveUp.Location = new System.Drawing.Point(12, 375);
			this.buttonMoveUp.Name = "buttonMoveUp";
			this.buttonMoveUp.Size = new System.Drawing.Size(75, 23);
			this.buttonMoveUp.TabIndex = 16;
			this.buttonMoveUp.Text = "Move Up";
			this.buttonMoveUp.UseVisualStyleBackColor = true;
			this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
			// 
			// buttonMoveDown
			// 
			this.buttonMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonMoveDown.Enabled = false;
			this.buttonMoveDown.Location = new System.Drawing.Point(145, 375);
			this.buttonMoveDown.Name = "buttonMoveDown";
			this.buttonMoveDown.Size = new System.Drawing.Size(75, 23);
			this.buttonMoveDown.TabIndex = 17;
			this.buttonMoveDown.Text = "Move Down";
			this.buttonMoveDown.UseVisualStyleBackColor = true;
			this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
			// 
			// formLayouts
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(514, 410);
			this.Controls.Add(this.buttonMoveDown);
			this.Controls.Add(this.tableExtras);
			this.Controls.Add(this.buttonDelete);
			this.Controls.Add(this.buttonMoveUp);
			this.Controls.Add(this.listFields);
			this.Controls.Add(this.buttonSaveField);
			this.Controls.Add(this.buttonNew);
			this.Controls.Add(this.buttonLoad);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.buttonClose);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(530, 2000);
			this.MinimumSize = new System.Drawing.Size(530, 36);
			this.Name = "formLayouts";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Layout Editor";
			this.Load += new System.EventHandler(this.formLayouts_Load);
			((System.ComponentModel.ISupportInitialize)(this.listFields)).EndInit();
			this.tableExtras.ResumeLayout(false);
			this.tableExtras.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridValues)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericMinimum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericMaximum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericHeight)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.ComboBox comboType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonSaveField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textLabel;
        private System.Windows.Forms.TextBox textSource;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView listFields;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnType;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.TableLayoutPanel tableExtras;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Label labelMinimum;
        private System.Windows.Forms.Label labelMaximum;
        private System.Windows.Forms.NumericUpDown numericMinimum;
        private System.Windows.Forms.NumericUpDown numericMaximum;
        private System.Windows.Forms.NumericUpDown numericHeight;
        private System.Windows.Forms.DataGridView gridValues;
        private System.Windows.Forms.Label labelValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnValue;
        private System.Windows.Forms.ContextMenuStrip menuLoadLayout;
        private System.Windows.Forms.Button buttonMoveUp;
        private System.Windows.Forms.Button buttonMoveDown;
    }
}