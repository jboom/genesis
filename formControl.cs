using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formControl : Form {
        private formMain parent;
        private int Mode;

        private const int MODE_MOVE = 0;
        private const int MODE_CREATE = 1;

        public formControl(formMain p) {
            InitializeComponent();
            parent = p;
        }

        private void formControl_FormClosing(object sender, FormClosingEventArgs e) {
            parent.Controller = null;
        }

        private void UpdateModeButton() {
            switch (Mode) {
                case MODE_MOVE:
                    buttonMode.Text = "Move";
                    toolTip1.SetToolTip(buttonMode, "Switch to Move Mode");
                    break;
                case MODE_CREATE:
                    buttonMode.Text = "Create";
                    toolTip1.SetToolTip(buttonMode, "Switch to Create Mode");
                    break;
            }
        }

        private void buttonMode_Click(object sender, EventArgs e) {
            switch (Mode) {
                case MODE_MOVE:
                    Mode = MODE_CREATE;
                    break;
                case MODE_CREATE:
                    Mode = MODE_MOVE;
                    break;
            }
            UpdateModeButton();
        }

        private void DoMove(Keys k) {
            if (Mode == MODE_CREATE) parent.ControlPressed = true; else parent.ControlPressed = false;
            parent.HandleCursorMove(k);
        }

        private void buttonNW_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad7);
        }

        private void buttonN_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad8);
        }

        private void buttonNE_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad9);
        }

        private void buttonW_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad4);
        }

        private void buttonCenter_Click(object sender, EventArgs e) {
            parent.CenterOnCurrent();
        }

        private void buttonE_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad6);
        }

        private void buttonSW_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad1);
        }

        private void buttonS_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad2);
        }

        private void buttonSE_Click(object sender, EventArgs e) {
            DoMove(Keys.NumPad3);
        }

        private void buttonU_Click(object sender, EventArgs e) {
            DoMove(Keys.Subtract);
        }

        private void buttonD_Click(object sender, EventArgs e) {
            DoMove(Keys.Add);
        }

        private void formControl_Load(object sender, EventArgs e) {
            UpdateModeButton();
        }
    }
}