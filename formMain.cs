using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.InteropServices;
using Genesis.Canvas;

namespace Genesis {
    public enum CustomActions {
        None = 0,
        CopyRoom = 1,
        CustomExit = 2,
        SetTemplate = 3
    }

    /// <summary>
    /// Summary description for formMain
    /// </summary>
    public class formMain : System.Windows.Forms.Form {
        // graphics variables
        private Common.RenderOptions optStandard;
        const int BACKBUFFER = 2;
        const int FRONTBUFFER = 1;

        // main data
        private Area _area;
        public DetailLayout defaultRoomLayout;
        public DetailLayout defaultObjectLayout;

        // misc. lists
        public List<Exit> SelectedExits = new List<Exit>();
        public Room Template;
        //public List<string> Templates = new List<string>();
        //public List<string> Layouts = new List<string>();

        private List<String> CommonExits;
        private List<String> LevelExits;

        // graphics elements
        private int bufferWidth, bufferHeight;
        private CanvasPen penGrid;
        private CanvasPen penExit;
		private CanvasPen penExitShadow;
        private CanvasPen penLevelExit;
        private CanvasPen penCurrentRoom;
		private CanvasPen penCustomExit;
        private CanvasPen penSelected;
        private CanvasPen penEmptyRoom;
        private CanvasPen penSelection;
        private CanvasPen penSelectedExit;
        private CanvasPen penRoom;
        private CanvasPen penIndoors;
        private CanvasPen penRoomShadow;
        private CanvasPen penZone;
        private CanvasPen penTemplate;
        private CanvasPen penObjects;
        private CanvasBrush brushRoom;
        private CanvasBrush brushRoomShadow;
        private CanvasBrush brushExitSquare;
        private CanvasBrush brushTransparent;
		private CanvasFont fontRoom;

        // boolean trackers
        private bool _ControlPressed;

        public bool ControlPressed {
            get { return _ControlPressed; }
            set {
                _ControlPressed = value;
                switch (value) {
                    case true: panelMode.Text = "Edit Mode"; break;
                    case false: panelMode.Text = ""; break;
                }
                UpdateStatus();
            }
        }
        private bool _ShiftPressed;

        public bool ShiftPressed {
            get { return _ShiftPressed; }
            set {
                _ShiftPressed = value;
                UpdateStatus();
            }
        }
        private bool AreaLoaded;
        private bool IsDragging;
        private bool IsSelecting;
        private bool IsLoading;

        // program settings
        private List<Room> m_Selected;
        public ProgramOptions options;
        private bool m_Saved;

        // edit mode variables
        private CustomActions editMode;
        private int editStep;
        private List<BaseObject> editObjects;

        // remember variables
        private string oldDir1;
        private string oldDir2;
        public List<String> oldCopy = new List<String>();

        // area properties
        public int AreaHeight;      // height measured from top to bottom
        public int AreaWidth;       // width measured from left to right
        public int AreaOffsetX;     // lowest x compared to 0
        public int AreaOffsetY;     // lowest y compared to 0

        // dragging variables
        bool HasMoved = false;
        private int oldX;
        private int oldY;
        private int newX;
        private int newY;
        private int xTempOffset;    // temp offset for dragging viewport
        private int yTempOffset;    // temp offset for dragging viewport

        // open forms
        public formEditor Editor;
        public formControl Controller;
        public formObjects ObjectsForm;
        public formAreaMap Overview;
        private Room currentRoom;

        private int RoomDistance = 50;
        private int RoomHeight = 20;
        private int RoomWidth = 20;
        private int ZoomLevel = 3;

        // constants
        private const int EXIT_DISTANCE = 8;
        private const int SHADOW_OFFSET = 3;
        private const int CLICK_DISTANCE = 10;
        private const int BORDER_OFFSET = 13;
        private const int FOCUS_SPACING = 100;
        private const int FOCUS_OFFSET = 400;
        private const int LEVELEXIT_OFFSET = 3;

        private StatusStrip statusMain;
        private ToolStripStatusLabel panelStatus;
        private ToolStrip toolbarMain;
        private ToolStripButton toolNew;
        private ToolStripButton toolSave;
        private ToolStripButton toolOpen;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton toolEdit;
        private ToolStripSplitButton toolZoom;
        private ToolStripMenuItem toolZoom25;
        private ToolStripMenuItem toolZoom50;
        private ToolStripMenuItem toolZoom100;
        private ToolStripMenuItem toolZoom200;
        private MenuStrip menuMain;
        private ToolStripMenuItem menuFile;
        private ToolStripMenuItem menuFileNew;
        private ToolStripSeparator toolStripMenuItem2;
        private ToolStripMenuItem menuFileOpen;
        private ToolStripMenuItem menuFileSave;
        private ToolStripMenuItem menuFileSaveAs;
        private ToolStripMenuItem menuFileClose;
        private ToolStripSeparator toolStripMenuItem3;
        private ToolStripMenuItem menuFileExit;
        private ToolStripMenuItem menuSelection;
        private ToolStripMenuItem menuSelectionInvert;
        private ToolStripMenuItem menuSelectionDelete;
        private ToolStripMenuItem menuSelectionClear;
        private ToolStripSeparator toolStripMenuItem7;
        private ToolStripMenuItem menuSelectionSelectAll;
        private ToolStripMenuItem menuTools;
        private ToolStripMenuItem menuToolsRoomEditor;
        private ToolStripMenuItem menuToolsShowExits;
        private ToolStripSeparator toolStripMenuItem4;
        private ToolStripMenuItem menuToolsRoomCopy;
        private ToolStripMenuItem menuToolsCustomExit;
        private ToolStripMenuItem menuToolsRoomSetter;
        private ToolStripMenuItem menuToolsWordReplace;
        private ToolStripMenuItem menuToolsRoomSelect;
        private ToolStripSeparator toolStripMenuItem5;
        private ToolStripMenuItem menuToolsSetTemplate;
        private ToolStripSeparator toolStripMenuItem6;
        private ToolStripMenuItem menuToolsOptions;
        private ToolStripMenuItem menuView;
        private ToolStripMenuItem menuViewGrid;
        private ToolStripMenuItem menuViewZones;
        private ToolStripMenuItem menuViewRoomsBelow;
        private ToolStripMenuItem menuHelp;
        private ToolStripMenuItem menuHelpAbout;
        private ToolStripSeparator toolStripMenuItem8;
        private ToolStripMenuItem menuToolsControlWindow;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripButton toolObjects;
        private ToolStripMenuItem menuArea;
        private ToolStripMenuItem menuAreaSetup;
        private ToolStripMenuItem menuAreaInformation;
        private ToolStripSeparator toolStripMenuItem1;
        private ToolStripMenuItem menuAreaCompileRooms;
        private ToolStripMenuItem menuAreaCompileAll;
        private ToolStripSeparator toolStripMenuItem9;
        private ToolStripMenuItem menuAreaObjects;
        private ToolStripButton toolGroups;
        private ToolStripMenuItem menuAreaGroups;
        private ToolStripButton toolCopyRoom;
        private ToolStripButton toolCompile;
        private ToolStripSeparator toolStripSeparator3;
        private ToolTip toolTip1;
        private ToolStripStatusLabel panelMode;
        private ToolStripSeparator toolStripMenuItem10;
        private ToolStripMenuItem menuToolsLayoutEditor;
        private ToolStripSeparator toolStripMenuItem11;
		private ToolStripMenuItem menuFileSaveAsTemplate;
		private Win32TestCanvas canvasMain;
		private ToolStripMenuItem menuViewRoomIDs;
        private ToolStripSeparator toolStripMenuItem12;
        private ToolStripMenuItem menuAreaMoveToUnfinishedRoom;
        private ToolStripMenuItem menuViewGoTo;

        private enum VirtualKeyStates : int {
            VK_CONTROL = 0x11,
            VK_SHIFT = 0x10
        }

        [DllImport("user32.dll")]
        static extern short GetKeyState(VirtualKeyStates nVirtKey);
        private System.Windows.Forms.SaveFileDialog fileSave;
        private System.Windows.Forms.OpenFileDialog fileOpen;
        private System.Windows.Forms.ContextMenu contextExits;
        private System.Windows.Forms.Timer timerAutoSave;
        private System.ComponentModel.IContainer components;

        public formMain() {
            InitializeComponent();

            bufferWidth = Screen.PrimaryScreen.WorkingArea.Width;
            bufferHeight = Screen.PrimaryScreen.WorkingArea.Height;
            ResetCanvasSettings();
            canvasMain.Paint += new PaintEventHandler(canvasMain_Paint);
            canvasMain.MouseDown += new MouseEventHandler(canvasMain_MouseDown);
            canvasMain.MouseUp += new MouseEventHandler(canvasMain_MouseUp);
            canvasMain.MouseMove += new MouseEventHandler(canvasMain_MouseMove);

            SetupPens();
            SetupBrushes();
            SetupFonts();
            SetupDefaultVariables();
            SetupExitLists();
            SetupDefaultRenderOptions();
            UpdateStatus();
        }

        void canvasMain_MouseMove(object sender, MouseEventArgs e) {
            HandleMouseMove(e.X, e.Y);
        }

        void ResetCanvasSettings() {
            canvasMain.SetBufferSize(2, bufferWidth * 3, bufferHeight * 3);
            canvasMain.UpdateBuffers();
        }

        void canvasMain_MouseUp(object sender, MouseEventArgs e) {
            HandleMouseUp(e.X, e.Y, e.Button);
        }

        void canvasMain_MouseDown(object sender, MouseEventArgs e) {
            HandleMouseDown(e.X, e.Y, e.Button);
        }

        void canvasMain_Paint(object sender, PaintEventArgs e) {
            Redraw(false);
        }

        private void SetupDefaultVariables() {
            options = new ProgramOptions();

            options.CommonProperties = new List<String>();
            options.EmptyProperties = new List<String>();
            options.RoomPropertyTypeList = new List<String>();
            Saved = true;
            options.FileType = "";
            options.DockEditor = true;
            options.AutoSave = false;
            options.DrawGrid = false;
            options.DrawShadows = true;

            _area = new Area();
            options.DrawQuality = 2;
            FileType = "Unix";
            _ShiftPressed = false;
            _ControlPressed = false;
            currentRoom = null;
            Editor = null;
            ObjectsForm = null;
            Template = null;
            AreaLoaded = false;
            Saved = true;
            IsDragging = false;
            IsSelecting = false;
            IsLoading = false;
            oldX = 0;
            oldY = 0;
            newX = 0;
            newY = 0;
            xTempOffset = 0;
            yTempOffset = 0;
            SelectedExits = new List<Exit>();
            editMode = CustomActions.None;
            editStep = 0;
            editObjects = new List<BaseObject>();
            m_Selected = new List<Room>();
        }

        private void SetupExitLists() {
            CommonExits = new List<String>();
            CommonExits.Add("north");
            CommonExits.Add("northeast");
            CommonExits.Add("east");
            CommonExits.Add("southeast");
            CommonExits.Add("south");
            CommonExits.Add("southwest");
            CommonExits.Add("west");
            CommonExits.Add("northwest");

            LevelExits = new List<String>();
            LevelExits.Add("up");
            LevelExits.Add("down");
        }

        private void SetupDefaultRenderOptions() {
            optStandard = new Common.RenderOptions();
            optStandard.DrawExits = true;
            optStandard.DrawExitSquares = true;
            optStandard.DrawGrid = true;
            optStandard.DrawIDs = true;
            optStandard.DrawInvExits = true;
            optStandard.DrawRooms = true;
            optStandard.DrawShadows = true;
            optStandard.DrawZones = false;
            optStandard.IgnoreScreenSize = false;
            optStandard.DrawExcluded = true;
            optStandard.ShowSelection = true;
            optStandard.ShowEmpty = true;
            optStandard.DrawAboveBelow = true;
            optStandard.FlattenZones = false;
            optStandard.scrollOffsetX = 0;
            optStandard.scrollOffsetY = 0;
        }

        private void SetupFonts() {
            //fontRoom = CreateFont(10, 4, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
            //fontRoomLarge = CreateFont(15, 6, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
			fontRoom = canvasMain.CreateFont("Arial", 10);
        }

        private void SetupBrushes() {
            brushRoom = canvasMain.CreateBrush(Color.White);
            brushRoomShadow = canvasMain.CreateBrush(Color.Gray);
            brushExitSquare = canvasMain.CreateBrush(Color.Black);
            brushTransparent = canvasMain.CreateBrush(Color.FromArgb(255));
        }

        private void SetupPens() {
            penExit = canvasMain.CreatePen(Color.DarkGray, 2);
			penExitShadow = canvasMain.CreatePen(Color.Gray, 1);
            penLevelExit = canvasMain.CreatePen(Color.BlueViolet, 2);
            penRoom = canvasMain.CreatePen(Color.Black, 1);
            penIndoors = canvasMain.CreatePen(Color.Maroon, 2);
            penRoomShadow = canvasMain.CreatePen(Color.Gray, 1);
            penEmptyRoom = canvasMain.CreatePen(Color.Red, 2);
            penGrid = canvasMain.CreatePen(Color.LightBlue, 1);
            penSelected = canvasMain.CreatePen(Color.FromArgb(0, 168, 255), 2);
			penSelectedExit = canvasMain.CreatePen(Color.FromArgb(0, 168, 255), 2);
            penCurrentRoom = canvasMain.CreatePen(Color.Blue, 2);
            penSelection = canvasMain.CreatePen(Color.FromArgb(0, 136, 255), 1);
            penZone = canvasMain.CreatePen(Color.White, 1);
            penTemplate = canvasMain.CreatePen(Color.FromArgb(0, 136, 68), 2);
			penCustomExit = canvasMain.CreatePen(Color.Red, 1);
            penObjects = canvasMain.CreatePen(Color.Green, 2);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.fileSave = new System.Windows.Forms.SaveFileDialog();
            this.fileOpen = new System.Windows.Forms.OpenFileDialog();
            this.contextExits = new System.Windows.Forms.ContextMenu();
            this.timerAutoSave = new System.Windows.Forms.Timer(this.components);
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.panelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolbarMain = new System.Windows.Forms.ToolStrip();
            this.toolNew = new System.Windows.Forms.ToolStripButton();
            this.toolSave = new System.Windows.Forms.ToolStripButton();
            this.toolOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolEdit = new System.Windows.Forms.ToolStripButton();
            this.toolZoom = new System.Windows.Forms.ToolStripSplitButton();
            this.toolZoom25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolZoom50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolZoom100 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolZoom200 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolCopyRoom = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolCompile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolObjects = new System.Windows.Forms.ToolStripButton();
            this.toolGroups = new System.Windows.Forms.ToolStripButton();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileSaveAsTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSelectionInvert = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSelectionDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSelectionClear = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSelectionSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuArea = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAreaSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAreaInformation = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAreaObjects = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAreaGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAreaCompileRooms = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAreaCompileAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTools = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsRoomEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsShowExits = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuToolsRoomCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsCustomExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsRoomSetter = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsWordReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsRoomSelect = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.menuToolsLayoutEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuToolsSetTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuToolsControlWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolsOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.menuView = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewZones = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewRoomIDs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewRoomsBelow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.menuViewGoTo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.canvasMain = new Genesis.Win32TestCanvas();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAreaMoveToUnfinishedRoom = new System.Windows.Forms.ToolStripMenuItem();
            this.statusMain.SuspendLayout();
            this.toolbarMain.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerAutoSave
            // 
            this.timerAutoSave.Interval = 300000;
            this.timerAutoSave.Tick += new System.EventHandler(this.timerAutoSave_Tick);
            // 
            // statusMain
            // 
            this.statusMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.panelStatus,
            this.panelMode});
            this.statusMain.Location = new System.Drawing.Point(0, 642);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(884, 22);
            this.statusMain.TabIndex = 2;
            this.statusMain.Text = "statusStrip1";
            // 
            // panelStatus
            // 
            this.panelStatus.AutoToolTip = true;
            this.panelStatus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(794, 17);
            this.panelStatus.Spring = true;
            this.panelStatus.Text = "Ready";
            this.panelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelMode
            // 
            this.panelMode.AutoSize = false;
            this.panelMode.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelMode.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.panelMode.Name = "panelMode";
            this.panelMode.Size = new System.Drawing.Size(75, 17);
            // 
            // toolbarMain
            // 
            this.toolbarMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolbarMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNew,
            this.toolSave,
            this.toolOpen,
            this.toolStripSeparator1,
            this.toolEdit,
            this.toolZoom,
            this.toolCopyRoom,
            this.toolStripSeparator2,
            this.toolCompile,
            this.toolStripSeparator3,
            this.toolObjects,
            this.toolGroups});
            this.toolbarMain.Location = new System.Drawing.Point(0, 24);
            this.toolbarMain.Name = "toolbarMain";
            this.toolbarMain.Size = new System.Drawing.Size(884, 25);
            this.toolbarMain.TabIndex = 6;
            this.toolbarMain.Text = "Standard Toolbar";
            // 
            // toolNew
            // 
            this.toolNew.Image = ((System.Drawing.Image)(resources.GetObject("toolNew.Image")));
            this.toolNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNew.Name = "toolNew";
            this.toolNew.Size = new System.Drawing.Size(51, 22);
            this.toolNew.Text = "New";
            this.toolNew.ToolTipText = "New Area";
            this.toolNew.Click += new System.EventHandler(this.toolNew_Click);
            // 
            // toolSave
            // 
            this.toolSave.Image = ((System.Drawing.Image)(resources.GetObject("toolSave.Image")));
            this.toolSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSave.Name = "toolSave";
            this.toolSave.Size = new System.Drawing.Size(51, 22);
            this.toolSave.Text = "Save";
            this.toolSave.Click += new System.EventHandler(this.toolSave_Click);
            // 
            // toolOpen
            // 
            this.toolOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolOpen.Image")));
            this.toolOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolOpen.Name = "toolOpen";
            this.toolOpen.Size = new System.Drawing.Size(56, 22);
            this.toolOpen.Text = "Open";
            this.toolOpen.ToolTipText = "Open Area";
            this.toolOpen.Click += new System.EventHandler(this.toolOpen_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolEdit
            // 
            this.toolEdit.Image = ((System.Drawing.Image)(resources.GetObject("toolEdit.Image")));
            this.toolEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolEdit.Name = "toolEdit";
            this.toolEdit.Size = new System.Drawing.Size(47, 22);
            this.toolEdit.Text = "Edit";
            this.toolEdit.ToolTipText = "Edit Room";
            this.toolEdit.Click += new System.EventHandler(this.toolEdit_Click);
            // 
            // toolZoom
            // 
            this.toolZoom.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolZoom25,
            this.toolZoom50,
            this.toolZoom100,
            this.toolZoom200});
            this.toolZoom.Image = ((System.Drawing.Image)(resources.GetObject("toolZoom.Image")));
            this.toolZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolZoom.Name = "toolZoom";
            this.toolZoom.Size = new System.Drawing.Size(71, 22);
            this.toolZoom.Text = "Zoom";
            this.toolZoom.ToolTipText = "Zoom In (Hold Shift to Zoom Out)";
            this.toolZoom.ButtonClick += new System.EventHandler(this.toolZoom_ButtonClick);
            this.toolZoom.DropDownOpening += new System.EventHandler(this.toolZoom_DropDownOpening);
            // 
            // toolZoom25
            // 
            this.toolZoom25.Name = "toolZoom25";
            this.toolZoom25.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D1)));
            this.toolZoom25.Size = new System.Drawing.Size(142, 22);
            this.toolZoom25.Text = "25%";
            this.toolZoom25.Click += new System.EventHandler(this.toolZoom25_Click);
            // 
            // toolZoom50
            // 
            this.toolZoom50.Name = "toolZoom50";
            this.toolZoom50.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D2)));
            this.toolZoom50.Size = new System.Drawing.Size(142, 22);
            this.toolZoom50.Text = "50%";
            this.toolZoom50.Click += new System.EventHandler(this.toolZoom50_Click);
            // 
            // toolZoom100
            // 
            this.toolZoom100.Name = "toolZoom100";
            this.toolZoom100.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D3)));
            this.toolZoom100.Size = new System.Drawing.Size(142, 22);
            this.toolZoom100.Text = "100%";
            this.toolZoom100.Click += new System.EventHandler(this.toolZoom100_Click);
            // 
            // toolZoom200
            // 
            this.toolZoom200.Name = "toolZoom200";
            this.toolZoom200.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D4)));
            this.toolZoom200.Size = new System.Drawing.Size(142, 22);
            this.toolZoom200.Text = "200%";
            this.toolZoom200.Click += new System.EventHandler(this.toolZoom200_Click);
            // 
            // toolCopyRoom
            // 
            this.toolCopyRoom.Image = ((System.Drawing.Image)(resources.GetObject("toolCopyRoom.Image")));
            this.toolCopyRoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolCopyRoom.Name = "toolCopyRoom";
            this.toolCopyRoom.Size = new System.Drawing.Size(55, 22);
            this.toolCopyRoom.Text = "Copy";
            this.toolCopyRoom.ToolTipText = "Copy Room";
            this.toolCopyRoom.Click += new System.EventHandler(this.toolCopyRoom_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolCompile
            // 
            this.toolCompile.Image = ((System.Drawing.Image)(resources.GetObject("toolCompile.Image")));
            this.toolCompile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolCompile.Name = "toolCompile";
            this.toolCompile.Size = new System.Drawing.Size(72, 22);
            this.toolCompile.Text = "Compile";
            this.toolCompile.ToolTipText = "Compile Rooms";
            this.toolCompile.Click += new System.EventHandler(this.toolCompile_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolObjects
            // 
            this.toolObjects.Image = ((System.Drawing.Image)(resources.GetObject("toolObjects.Image")));
            this.toolObjects.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolObjects.Name = "toolObjects";
            this.toolObjects.Size = new System.Drawing.Size(67, 22);
            this.toolObjects.Text = "Objects";
            this.toolObjects.ToolTipText = "Display Objects Window";
            this.toolObjects.Click += new System.EventHandler(this.toolObjects_Click);
            // 
            // toolGroups
            // 
            this.toolGroups.Image = ((System.Drawing.Image)(resources.GetObject("toolGroups.Image")));
            this.toolGroups.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolGroups.Name = "toolGroups";
            this.toolGroups.Size = new System.Drawing.Size(65, 22);
            this.toolGroups.Text = "Groups";
            this.toolGroups.ToolTipText = "Display Groups Window";
            this.toolGroups.Click += new System.EventHandler(this.toolGroups_Click);
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuSelection,
            this.menuArea,
            this.menuTools,
            this.menuView,
            this.menuHelp});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(884, 24);
            this.menuMain.TabIndex = 1;
            this.menuMain.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.toolStripMenuItem2,
            this.menuFileOpen,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.menuFileClose,
            this.toolStripMenuItem11,
            this.menuFileSaveAsTemplate,
            this.toolStripMenuItem3,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "&File";
            // 
            // menuFileNew
            // 
            this.menuFileNew.Image = ((System.Drawing.Image)(resources.GetObject("menuFileNew.Image")));
            this.menuFileNew.Name = "menuFileNew";
            this.menuFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuFileNew.Size = new System.Drawing.Size(195, 22);
            this.menuFileNew.Text = "&New...";
            this.menuFileNew.Click += new System.EventHandler(this.menuFileNew_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(192, 6);
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpen.Image")));
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(195, 22);
            this.menuFileOpen.Text = "&Open...";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuFileSave
            // 
            this.menuFileSave.Image = ((System.Drawing.Image)(resources.GetObject("menuFileSave.Image")));
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuFileSave.Size = new System.Drawing.Size(195, 22);
            this.menuFileSave.Text = "&Save...";
            this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
            // 
            // menuFileSaveAs
            // 
            this.menuFileSaveAs.Name = "menuFileSaveAs";
            this.menuFileSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.menuFileSaveAs.Size = new System.Drawing.Size(195, 22);
            this.menuFileSaveAs.Text = "Save &As...";
            this.menuFileSaveAs.Click += new System.EventHandler(this.menuFileSaveAs_Click);
            // 
            // menuFileClose
            // 
            this.menuFileClose.Name = "menuFileClose";
            this.menuFileClose.Size = new System.Drawing.Size(195, 22);
            this.menuFileClose.Text = "&Close";
            this.menuFileClose.Click += new System.EventHandler(this.menuFileClose_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(192, 6);
            // 
            // menuFileSaveAsTemplate
            // 
            this.menuFileSaveAsTemplate.Name = "menuFileSaveAsTemplate";
            this.menuFileSaveAsTemplate.Size = new System.Drawing.Size(195, 22);
            this.menuFileSaveAsTemplate.Text = "Save As &Template...";
            this.menuFileSaveAsTemplate.Click += new System.EventHandler(this.menuFileSaveAsTemplate_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(192, 6);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.Size = new System.Drawing.Size(195, 22);
            this.menuFileExit.Text = "E&xit";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // menuSelection
            // 
            this.menuSelection.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSelectionInvert,
            this.menuSelectionDelete,
            this.menuSelectionClear,
            this.toolStripMenuItem7,
            this.menuSelectionSelectAll});
            this.menuSelection.Name = "menuSelection";
            this.menuSelection.Size = new System.Drawing.Size(67, 20);
            this.menuSelection.Text = "&Selection";
            // 
            // menuSelectionInvert
            // 
            this.menuSelectionInvert.Name = "menuSelectionInvert";
            this.menuSelectionInvert.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.menuSelectionInvert.Size = new System.Drawing.Size(164, 22);
            this.menuSelectionInvert.Text = "&Invert";
            this.menuSelectionInvert.Click += new System.EventHandler(this.menuSelectionInvert_Click);
            // 
            // menuSelectionDelete
            // 
            this.menuSelectionDelete.Image = ((System.Drawing.Image)(resources.GetObject("menuSelectionDelete.Image")));
            this.menuSelectionDelete.Name = "menuSelectionDelete";
            this.menuSelectionDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.menuSelectionDelete.Size = new System.Drawing.Size(164, 22);
            this.menuSelectionDelete.Text = "&Delete";
            this.menuSelectionDelete.Click += new System.EventHandler(this.menuSelectionDelete_Click);
            // 
            // menuSelectionClear
            // 
            this.menuSelectionClear.Name = "menuSelectionClear";
            this.menuSelectionClear.Size = new System.Drawing.Size(164, 22);
            this.menuSelectionClear.Text = "&Clear";
            this.menuSelectionClear.Click += new System.EventHandler(this.menuSelectionClear_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(161, 6);
            // 
            // menuSelectionSelectAll
            // 
            this.menuSelectionSelectAll.Name = "menuSelectionSelectAll";
            this.menuSelectionSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.menuSelectionSelectAll.Size = new System.Drawing.Size(164, 22);
            this.menuSelectionSelectAll.Text = "&Select All";
            this.menuSelectionSelectAll.Click += new System.EventHandler(this.menuSelectionSelectAll_Click);
            // 
            // menuArea
            // 
            this.menuArea.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAreaSetup,
            this.menuAreaInformation,
            this.toolStripMenuItem9,
            this.menuAreaObjects,
            this.menuAreaGroups,
            this.toolStripMenuItem1,
            this.menuAreaCompileRooms,
            this.menuAreaCompileAll,
            this.toolStripMenuItem12,
            this.menuAreaMoveToUnfinishedRoom});
            this.menuArea.Name = "menuArea";
            this.menuArea.Size = new System.Drawing.Size(43, 20);
            this.menuArea.Text = "&Area";
            // 
            // menuAreaSetup
            // 
            this.menuAreaSetup.Name = "menuAreaSetup";
            this.menuAreaSetup.Size = new System.Drawing.Size(216, 22);
            this.menuAreaSetup.Text = "&Setup";
            this.menuAreaSetup.Click += new System.EventHandler(this.menuAreaSetup_Click);
            // 
            // menuAreaInformation
            // 
            this.menuAreaInformation.Name = "menuAreaInformation";
            this.menuAreaInformation.Size = new System.Drawing.Size(216, 22);
            this.menuAreaInformation.Text = "&Statistics";
            this.menuAreaInformation.Click += new System.EventHandler(this.menuAreaInformation_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(213, 6);
            // 
            // menuAreaObjects
            // 
            this.menuAreaObjects.Image = ((System.Drawing.Image)(resources.GetObject("menuAreaObjects.Image")));
            this.menuAreaObjects.Name = "menuAreaObjects";
            this.menuAreaObjects.Size = new System.Drawing.Size(216, 22);
            this.menuAreaObjects.Text = "Objects";
            this.menuAreaObjects.Click += new System.EventHandler(this.menuAreaObjects_Click);
            // 
            // menuAreaGroups
            // 
            this.menuAreaGroups.Image = ((System.Drawing.Image)(resources.GetObject("menuAreaGroups.Image")));
            this.menuAreaGroups.Name = "menuAreaGroups";
            this.menuAreaGroups.Size = new System.Drawing.Size(216, 22);
            this.menuAreaGroups.Text = "&Groups";
            this.menuAreaGroups.Click += new System.EventHandler(this.menuAreaGroups_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(213, 6);
            // 
            // menuAreaCompileRooms
            // 
            this.menuAreaCompileRooms.Image = ((System.Drawing.Image)(resources.GetObject("menuAreaCompileRooms.Image")));
            this.menuAreaCompileRooms.Name = "menuAreaCompileRooms";
            this.menuAreaCompileRooms.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.menuAreaCompileRooms.Size = new System.Drawing.Size(216, 22);
            this.menuAreaCompileRooms.Text = "&Compile Rooms";
            this.menuAreaCompileRooms.Click += new System.EventHandler(this.menuAreaCompileRooms_Click);
            // 
            // menuAreaCompileAll
            // 
            this.menuAreaCompileAll.Name = "menuAreaCompileAll";
            this.menuAreaCompileAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
            this.menuAreaCompileAll.Size = new System.Drawing.Size(216, 22);
            this.menuAreaCompileAll.Text = "Compile All";
            this.menuAreaCompileAll.Click += new System.EventHandler(this.menuAreaCompileAll_Click);
            // 
            // menuTools
            // 
            this.menuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolsRoomEditor,
            this.menuToolsShowExits,
            this.toolStripMenuItem4,
            this.menuToolsRoomCopy,
            this.menuToolsCustomExit,
            this.menuToolsRoomSetter,
            this.menuToolsWordReplace,
            this.menuToolsRoomSelect,
            this.toolStripMenuItem10,
            this.menuToolsLayoutEditor,
            this.toolStripMenuItem5,
            this.menuToolsSetTemplate,
            this.toolStripMenuItem6,
            this.menuToolsControlWindow,
            this.menuToolsOptions});
            this.menuTools.Name = "menuTools";
            this.menuTools.Size = new System.Drawing.Size(48, 20);
            this.menuTools.Text = "&Tools";
            // 
            // menuToolsRoomEditor
            // 
            this.menuToolsRoomEditor.Image = ((System.Drawing.Image)(resources.GetObject("menuToolsRoomEditor.Image")));
            this.menuToolsRoomEditor.Name = "menuToolsRoomEditor";
            this.menuToolsRoomEditor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.menuToolsRoomEditor.Size = new System.Drawing.Size(217, 22);
            this.menuToolsRoomEditor.Text = "Room &Editor";
            this.menuToolsRoomEditor.Click += new System.EventHandler(this.menuToolsRoomEditor_Click);
            // 
            // menuToolsShowExits
            // 
            this.menuToolsShowExits.Name = "menuToolsShowExits";
            this.menuToolsShowExits.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.menuToolsShowExits.Size = new System.Drawing.Size(217, 22);
            this.menuToolsShowExits.Text = "Show E&xits";
            this.menuToolsShowExits.Click += new System.EventHandler(this.menuToolsShowExits_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(214, 6);
            // 
            // menuToolsRoomCopy
            // 
            this.menuToolsRoomCopy.Name = "menuToolsRoomCopy";
            this.menuToolsRoomCopy.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.C)));
            this.menuToolsRoomCopy.Size = new System.Drawing.Size(217, 22);
            this.menuToolsRoomCopy.Text = "Room C&opy";
            this.menuToolsRoomCopy.Click += new System.EventHandler(this.menuToolsRoomCopy_Click);
            // 
            // menuToolsCustomExit
            // 
            this.menuToolsCustomExit.Name = "menuToolsCustomExit";
            this.menuToolsCustomExit.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.E)));
            this.menuToolsCustomExit.Size = new System.Drawing.Size(217, 22);
            this.menuToolsCustomExit.Text = "C&ustom Exit";
            this.menuToolsCustomExit.Click += new System.EventHandler(this.menuToolsCustomExit_Click);
            // 
            // menuToolsRoomSetter
            // 
            this.menuToolsRoomSetter.Name = "menuToolsRoomSetter";
            this.menuToolsRoomSetter.Size = new System.Drawing.Size(217, 22);
            this.menuToolsRoomSetter.Text = "Room Se&tter";
            this.menuToolsRoomSetter.Click += new System.EventHandler(this.menuToolsRoomSetter_Click);
            // 
            // menuToolsWordReplace
            // 
            this.menuToolsWordReplace.Name = "menuToolsWordReplace";
            this.menuToolsWordReplace.Size = new System.Drawing.Size(217, 22);
            this.menuToolsWordReplace.Text = "&Word Replace";
            this.menuToolsWordReplace.Click += new System.EventHandler(this.menuToolsWordReplace_Click);
            // 
            // menuToolsRoomSelect
            // 
            this.menuToolsRoomSelect.Name = "menuToolsRoomSelect";
            this.menuToolsRoomSelect.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.W)));
            this.menuToolsRoomSelect.Size = new System.Drawing.Size(217, 22);
            this.menuToolsRoomSelect.Text = "Room Se&lect";
            this.menuToolsRoomSelect.Click += new System.EventHandler(this.menuToolsRoomSelect_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(214, 6);
            // 
            // menuToolsLayoutEditor
            // 
            this.menuToolsLayoutEditor.Name = "menuToolsLayoutEditor";
            this.menuToolsLayoutEditor.Size = new System.Drawing.Size(217, 22);
            this.menuToolsLayoutEditor.Text = "&Layout Editor";
            this.menuToolsLayoutEditor.Click += new System.EventHandler(this.menuToolsLayoutEditor_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(214, 6);
            // 
            // menuToolsSetTemplate
            // 
            this.menuToolsSetTemplate.Name = "menuToolsSetTemplate";
            this.menuToolsSetTemplate.Size = new System.Drawing.Size(217, 22);
            this.menuToolsSetTemplate.Text = "Set Te&mplate";
            this.menuToolsSetTemplate.Click += new System.EventHandler(this.menuToolsSetTemplate_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(214, 6);
            // 
            // menuToolsControlWindow
            // 
            this.menuToolsControlWindow.Image = ((System.Drawing.Image)(resources.GetObject("menuToolsControlWindow.Image")));
            this.menuToolsControlWindow.Name = "menuToolsControlWindow";
            this.menuToolsControlWindow.Size = new System.Drawing.Size(217, 22);
            this.menuToolsControlWindow.Text = "Control Win&dow";
            this.menuToolsControlWindow.Click += new System.EventHandler(this.menuToolsControlWindow_Click);
            // 
            // menuToolsOptions
            // 
            this.menuToolsOptions.Name = "menuToolsOptions";
            this.menuToolsOptions.Size = new System.Drawing.Size(217, 22);
            this.menuToolsOptions.Text = "O&ptions";
            this.menuToolsOptions.Click += new System.EventHandler(this.menuToolsOptions_Click);
            // 
            // menuView
            // 
            this.menuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuViewZones,
            this.menuViewGrid,
            this.menuViewRoomIDs,
            this.menuViewRoomsBelow,
            this.toolStripMenuItem8,
            this.menuViewGoTo});
            this.menuView.Name = "menuView";
            this.menuView.Size = new System.Drawing.Size(44, 20);
            this.menuView.Text = "&View";
            this.menuView.DropDownOpening += new System.EventHandler(this.menuView_DropDownOpening);
            // 
            // menuViewZones
            // 
            this.menuViewZones.CheckOnClick = true;
            this.menuViewZones.Name = "menuViewZones";
            this.menuViewZones.Size = new System.Drawing.Size(157, 22);
            this.menuViewZones.Text = "&Zones";
            this.menuViewZones.Visible = false;
            this.menuViewZones.Click += new System.EventHandler(this.menuViewZones_Click);
            // 
            // menuViewGrid
            // 
            this.menuViewGrid.CheckOnClick = true;
            this.menuViewGrid.Image = ((System.Drawing.Image)(resources.GetObject("menuViewGrid.Image")));
            this.menuViewGrid.Name = "menuViewGrid";
            this.menuViewGrid.Size = new System.Drawing.Size(157, 22);
            this.menuViewGrid.Text = "&Grid";
            this.menuViewGrid.Click += new System.EventHandler(this.menuViewGrid_Click);
            // 
            // menuViewRoomIDs
            // 
            this.menuViewRoomIDs.CheckOnClick = true;
            this.menuViewRoomIDs.Name = "menuViewRoomIDs";
            this.menuViewRoomIDs.Size = new System.Drawing.Size(157, 22);
            this.menuViewRoomIDs.Text = "Room IDs";
            this.menuViewRoomIDs.Click += new System.EventHandler(this.menuViewRoomIDs_Click);
            // 
            // menuViewRoomsBelow
            // 
            this.menuViewRoomsBelow.CheckOnClick = true;
            this.menuViewRoomsBelow.Name = "menuViewRoomsBelow";
            this.menuViewRoomsBelow.Size = new System.Drawing.Size(157, 22);
            this.menuViewRoomsBelow.Text = "&Rooms Below";
            this.menuViewRoomsBelow.Click += new System.EventHandler(this.menuViewRoomsBelow_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(154, 6);
            // 
            // menuViewGoTo
            // 
            this.menuViewGoTo.Image = ((System.Drawing.Image)(resources.GetObject("menuViewGoTo.Image")));
            this.menuViewGoTo.Name = "menuViewGoTo";
            this.menuViewGoTo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.menuViewGoTo.Size = new System.Drawing.Size(157, 22);
            this.menuViewGoTo.Text = "&Go To...";
            this.menuViewGoTo.Click += new System.EventHandler(this.menuViewGoTo_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(44, 20);
            this.menuHelp.Text = "&Help";
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Image = ((System.Drawing.Image)(resources.GetObject("menuHelpAbout.Image")));
            this.menuHelpAbout.Name = "menuHelpAbout";
            this.menuHelpAbout.Size = new System.Drawing.Size(107, 22);
            this.menuHelpAbout.Text = "&About";
            this.menuHelpAbout.Click += new System.EventHandler(this.menuHelpAbout_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // canvasMain
            // 
            this.canvasMain.BufferCount = 2;
            this.canvasMain.CurrentBuffer = 2;
            this.canvasMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvasMain.Location = new System.Drawing.Point(0, 49);
            this.canvasMain.Name = "canvasMain";
            this.canvasMain.Size = new System.Drawing.Size(884, 593);
            this.canvasMain.TabIndex = 7;
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(213, 6);
            // 
            // menuAreaMoveToUnfinishedRoom
            // 
            this.menuAreaMoveToUnfinishedRoom.Name = "menuAreaMoveToUnfinishedRoom";
            this.menuAreaMoveToUnfinishedRoom.Size = new System.Drawing.Size(216, 22);
            this.menuAreaMoveToUnfinishedRoom.Text = "Move To Unfinished Room";
            this.menuAreaMoveToUnfinishedRoom.Click += new System.EventHandler(this.menuAreaMoveToUnfinishedRoom_Click);
            // 
            // formMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 664);
            this.Controls.Add(this.canvasMain);
            this.Controls.Add(this.toolbarMain);
            this.Controls.Add(this.statusMain);
            this.Controls.Add(this.menuMain);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuMain;
            this.MinimumSize = new System.Drawing.Size(100, 150);
            this.Name = "formMain";
            this.Text = "Genesis";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.formMain_Closing);
            this.Closed += new System.EventHandler(this.formMain_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formMain_FormClosing);
            this.Load += new System.EventHandler(this.formMain_Load);
            this.ResizeBegin += new System.EventHandler(this.formMain_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.formMain_ResizeEnd);
            this.LocationChanged += new System.EventHandler(this.formMain_LocationChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.formMain_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.formMain_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.formMain_KeyUp);
            this.statusMain.ResumeLayout(false);
            this.statusMain.PerformLayout();
            this.toolbarMain.ResumeLayout(false);
            this.toolbarMain.PerformLayout();
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        public int EditorOpacity {
            get {
                return options.EditorOpacity;
            }
            set {
                options.EditorOpacity = value;
            }
        }

        public int EditorTop {
            get {
                return options.EditorTop;
            }
            set {
                options.EditorTop = value;
            }
        }

        public int EditorLeft {
            get {
                return options.EditorLeft;
            }
            set {
                options.EditorLeft = value;
            }
        }

        public int EditorHeight {
            get {
                return options.EditorHeight;
            }
            set {
                options.EditorHeight = value;
            }
        }

        public int EditorWidth {
            get {
                return options.EditorWidth;
            }
            set {
                options.EditorWidth = value;
            }
        }

        public int ObjectsFormTop {
            get {
                return options.ObjectsFormTop;
            }
            set {
                options.ObjectsFormTop = value;
            }
        }

        public int ObjectsFormLeft {
            get {
                return options.ObjectsFormLeft;
            }
            set {
                options.ObjectsFormLeft = value;
            }
        }

        public Color BackgroundColour {
            get {
                return Color.FromArgb(options.BackgroundColour);
            }
            set {
                options.BackgroundColour = value.ToArgb();
            }
        }

        public Color GridColour {
            get {
                return Color.FromArgb(options.GridColour);
            }
            set {
                options.GridColour = value.ToArgb();
            }
        }

        public List<String> CodePropertyList {
            get {
                return options.CodePropertyList;
            }
            set {
                options.CodePropertyList = value;
            }
        }

        public Room CurrentRoom {
            get {
                return currentRoom;
            }
        }

        public bool NotesAsLabel {
            get {
                return options.NotesAsLabel;
            }
            set {
                options.NotesAsLabel = value;
                optStandard.DrawNotesAsLabel = value;
            }
        }

        public bool AddInvisible {
            get {
                return options.AddInvisible;
            }
            set {
                options.AddInvisible = value;
            }
        }

        public List<String> RoomPropertyTypeList {
            get {
                return options.RoomPropertyTypeList;
            }
            set {
                options.RoomPropertyTypeList = value;
            }
        }

        public List<String> ObjectPropertyTypeList {
            get {
                return options.ObjectPropertyTypeList;
            }
            set {
                options.ObjectPropertyTypeList = value;
            }
        }

        public List<Room> SelectedRooms {
            get {
                return m_Selected;
            }
            set {
                m_Selected = value;
                UpdateStatus();
            }
        }

        public List<String> EmptyProperties {
            get {
                return options.EmptyProperties;
            }
            set {
                options.EmptyProperties = value;
            }
        }

        public List<String> CommonProperties {
            get {
                return options.CommonProperties;
            }
            set {
                options.CommonProperties = value;
            }
        }

        public bool DrawGrid {
            get {
                return options.DrawGrid;
            }
            set {
                options.DrawGrid = value;
                optStandard.DrawGrid = value;
            }
        }

        public bool DrawZones {
            get {
                return options.DrawZones;
            }
            set {
                options.DrawZones = value;
                optStandard.DrawZones = value;
            }
        }

		public bool DrawRoomIDs {
			get {
				return optStandard.DrawIDs;
			}
			set {
				optStandard.DrawIDs = value;
			}
		}

        public bool DrawRoomsBelow {
            get {
                return optStandard.DrawAboveBelow;
            }
            set {
                optStandard.DrawAboveBelow = value;
            }
        }

        public string FileType {
            get {
                return options.FileType;
            }
            set {
                options.FileType = value;
                switch (value) {
                    case "PC":
                        _area.FileLineBreak = "\r\n";
                        break;
                    case "Unix":
                        _area.FileLineBreak = "\n";
                        break;
                    case "Macintosh":
                        _area.FileLineBreak = "\r";
                        break;
                    default:
                        _area.FileLineBreak = "\n";
                        break;
                }
            }
        }

        public bool ExcludeOld {
            get {
                return options.ExcludeOld;
            }
            set {
                options.ExcludeOld = value;
            }
        }

        public bool AutoSave {
            get {
                return options.AutoSave;
            }
            set {
                options.AutoSave = value;
                timerAutoSave.Enabled = value;
            }
        }

        public bool DockEditor {
            get {
                return options.DockEditor;
            }
            set {
                options.DockEditor = value;
            }
        }

        public bool Saved {
            get {
                return m_Saved;
            }
            set {
                m_Saved = value;
                UpdateTitle();
            }
        }

        public int ViewportWidth {
            get {
                return ClientRectangle.Width;
            }
        }

        public int ViewportHeight {
            get {
                return ClientRectangle.Height;
            }
        }

        public Room AddRoom(int x, int y, int z) {
            Room newRoom;
            int id;

            newRoom = new Room();

            id = GetNewId();

            newRoom.id = id;
            newRoom.x = x;
            newRoom.y = y;
            newRoom.z = z;
            newRoom.updated = true;

            if (Template != null) {
                // TODO:
                //newRoom.properties = Template.properties.Copy();
            } else {
                SetupProperties(newRoom);
            }
            newRoom.UpdateCoordinates(RoomDistance);

            _area.Rooms.Add(id, newRoom);
            ValidateRoom(newRoom.id);

            if (currentRoom == null) currentRoom = newRoom;

            Saved = false;

            return newRoom;
        }

        private void SetupProperties(Room r) {
            Array a;

            if (Template == null) {
                foreach (string s in CommonProperties) {
                    a = s.Split(':');
                    if (!r.HasProperty((string)a.GetValue(0))) {
                        r.SetProperty((string)a.GetValue(0), (string)a.GetValue(1));
                    }
                }
            } else {
                foreach (KeyValuePair<String,String> DE in Template.properties) {
                    r.SetProperty((string)DE.Key, (string)DE.Value);
                }
            }
        }

        public void SaveSettings() {
            try {
                var xs = new XmlSerializer(typeof(ProgramOptions));
                using (var tw = new StreamWriter(ConfigurationFile)) {
                    xs.Serialize(tw, options);
                }
            } catch (Exception e) {
                Log.error(e);
            }
        }

        private string ConfigurationFile {
            get { return Path.Combine(Application.ExecutablePath, "config.xml"); }
        }

        private bool LoadSettings() {
            if (!File.Exists(ConfigurationFile)) return false;

            try {
                var xs = new XmlSerializer(typeof(ProgramOptions));
                using (var fs = new FileStream(ConfigurationFile, FileMode.Open)) {
                    options = (ProgramOptions)xs.Deserialize(fs);                    
                }
            } catch (Exception e) {
                Log.error(e);
                options = new ProgramOptions();
                options.SetDefaults();
            }

            optStandard.DrawGrid = DrawGrid;
            optStandard.DrawZones = DrawZones;
            optStandard.DrawAboveBelow = DrawRoomsBelow;
            optStandard.DrawNotesAsLabel = NotesAsLabel;

            options.DefaultObjectLayoutFile = StripExtensionAndPath(options.DefaultObjectLayoutFile);
            options.DefaultRoomLayoutFile = StripExtensionAndPath(options.DefaultRoomLayoutFile);

            UpdateForm();

            return true;
        }

        public Room AddRoom(Room r) {
            _area.Rooms.Add(r.id, r);

            if (currentRoom == null) currentRoom = r;

            r.UpdateCoordinates(RoomDistance);
            SetupProperties(r);

            return r;
        }

        public Exit AddExit(string dirFrom, Room roomFrom, string dirTo, Room roomTo) {
            Exit newExit;

            newExit = new Exit();
            newExit.dirFrom = dirFrom;
            newExit.dirTo = dirTo;
            newExit.idFrom = roomFrom.id;
            newExit.idTo = roomTo.id;
            newExit.zFrom = roomFrom.z;
            newExit.zTo = roomTo.z;
            SetExitCoordinates(newExit);

            _area.Exits.Add(newExit);

            Saved = false;

            return newExit;
        }

        public void AddExit(Exit ex) {
            SetExitCoordinates(ex);
            _area.Exits.Add(ex);
        }

        public void UpdateScreen() {
			/* copy from source buffer */
			canvasMain.Copy(BACKBUFFER, new Rectangle(bufferWidth - xTempOffset, bufferHeight - yTempOffset, canvasMain.Width, canvasMain.Height), FRONTBUFFER, 0, 0);

			canvasMain.CurrentBuffer = FRONTBUFFER;
            canvasMain.Lock();


            /* draw selected rooms indicators */
			if (SelectedRooms.Count > 0) {
				foreach (Room r in SelectedRooms) {
                    if (r.z == currentRoom.z) {
                        canvasMain.Rectangle(brushTransparent, penSelected, r.rx + xTempOffset + optStandard.scrollOffsetX, r.ry + yTempOffset + optStandard.scrollOffsetY, r.rx + optStandard.scrollOffsetX + xTempOffset + RoomWidth, r.ry + RoomHeight + yTempOffset + optStandard.scrollOffsetY);
                    }
				}
			}

			if (SelectedExits.Count > 0) {
				foreach (Exit ex in SelectedExits) {
					canvasMain.Line(penSelectedExit, ex.p1.X + xTempOffset + optStandard.scrollOffsetX, ex.p1.Y + yTempOffset + optStandard.scrollOffsetY, ex.p2.X + xTempOffset + optStandard.scrollOffsetX, ex.p2.Y + yTempOffset + optStandard.scrollOffsetY);
				}
			}

			/* draw current room indicator */
			if (currentRoom != null) {
				canvasMain.Rectangle(brushTransparent, penCurrentRoom, currentRoom.rx + xTempOffset + optStandard.scrollOffsetX, currentRoom.ry + yTempOffset + optStandard.scrollOffsetY, currentRoom.rx + optStandard.scrollOffsetX + xTempOffset + RoomWidth, currentRoom.ry + RoomHeight + yTempOffset + optStandard.scrollOffsetY);
			}

			/* draw selection rectangle */
			if (IsSelecting) {
				int x, y, h, w;
				if (oldX != newX && oldY != newY && newX != 0 && newY != 0) {
					h = 0;
					w = 0;
					x = 0;
					y = 0;
					if (oldX < newX) {
						x = oldX;
						w = newX - oldX;
					} else if (oldX > newX) {
						x = newX;
						w = oldX - newX;
					}
					if (oldY < newY) {
						y = oldY;
						h = newY - oldY;
					} else if (oldY > newY) {
						y = newY;
						h = oldY - newY;
					}
					canvasMain.Rectangle(brushTransparent, penSelection, x, y, x + w, y + h);
				}
			}

            /* copy to screen */
            canvasMain.Present();

            /* clean up */
            canvasMain.Unlock();
        }

        public void UpdateBuffer() {
            Render(optStandard);
            // TODO: re-enable
            //UpdateOverview();
        }

        public void Render(Common.RenderOptions opt) {
            int xo, yo, x, y;

            if (IsLoading) {
                return;
            }

            if (!AreaLoaded) {
                canvasMain.CurrentBuffer = BACKBUFFER;
                canvasMain.Lock();
                canvasMain.Clear(BackgroundColour);
				canvasMain.Present();
                canvasMain.Unlock();

                return;
            }

			if (currentRoom == null) return;

			canvasMain.CurrentBuffer = BACKBUFFER;
            canvasMain.Lock();

			/*
            if (ZoomLevel == 4) {
                oldFont = SelectObject(hdcBackBuffer, fontRoomLarge);
            } else {
                oldFont = SelectObject(hdcBackBuffer, fontRoom);
            }
			*/

            canvasMain.Clear(BackgroundColour);

            /* layers:
             * 0 = zones
             * 1 = shadow exits
             * 2 = shadow rooms
             * 3 = grid
             * 4 = exits
             * 5 = rooms
             */

			xo = bufferWidth + opt.scrollOffsetX;
			yo = bufferHeight + opt.scrollOffsetY;

			/* calculate visibility */
            foreach (var r1 in _area.Rooms.Values) {
				r1.visible = false;
				if (r1.z < currentRoom.z - 1 || r1.z > currentRoom.z + 1) continue;

				if (r1.rx + xo > 0 - bufferWidth && r1.rx + xo < 0 + bufferWidth * 3) r1.visible = true;
				if (!r1.visible && r1.ry + yo > 0 - bufferHeight && r1.ry + yo < 0 + bufferHeight * 3) r1.visible = true;
			}

			/* draw zones */
			/*
			if (opt.DrawZones) {
			    string Colour;
			    Color c;
			    foreach (KeyValuePair<int,Room> DE in rooms) {
			        r1 = DE.Value;
			        if (!r1.visible) continue;
			        Colour = r1.GetProperty("colour");
			        if (Colour == "" || Colour == "-1") continue;
			        if (!opt.FlattenZones && r1.z != currentRoom.z) continue;
			        x = r1.rx + xo + (RoomWidth / 2) - (RoomDistance / 2);
			        y = r1.ry + yo + (RoomHeight / 2) - (RoomDistance / 2);
			        c = Color.FromArgb(Int32.Parse(Colour));
			        c = Color.FromArgb(0, c);
			        canvasMain.Rectangle(canvasMain.CreateBrush(c), penZone, x, y, x + RoomDistance + 1, y + RoomDistance + 1);
			    }
			}
			*/


			/* draw shadow exits */
			if (opt.DrawAboveBelow) {
				foreach (Exit e in _area.Exits) {
					if (e.zTo < currentRoom.z - 1) continue;
					if (e.zTo > currentRoom.z) continue;
					if (e.zFrom < currentRoom.z - 1) continue;
					if (e.zFrom > currentRoom.z) continue;
					if (e.zTo == currentRoom.z && e.zFrom == currentRoom.z) continue;

                    var r1 = _area.Rooms[e.idFrom];
                    var r2 = _area.Rooms[e.idTo];
					if (e.zTo < currentRoom.z) {
						DrawExit(e, opt, 1, xo, yo);
					} else {
						DrawExit(e, opt, -1, xo, yo);
					}
				}
			}

			/* draw shadows */
			if (AreaLoaded && (opt.DrawZones || opt.DrawAboveBelow)) {
                foreach (var r1 in _area.Rooms.Values) {
					if (opt.DrawAboveBelow) {
						if (!r1.visible) continue;
						if (r1.z == currentRoom.z - 1) {
							canvasMain.Rectangle(brushRoomShadow, penRoomShadow, r1.rx + SHADOW_OFFSET + xo, r1.ry + SHADOW_OFFSET + yo, r1.rx + SHADOW_OFFSET + xo + RoomWidth, r1.ry + SHADOW_OFFSET + yo + RoomHeight);
						/*
						} else if (r1.z == currentRoom.z + 1) {
							canvasMain.Rectangle(brushRoomShadow, penRoomShadow, r1.rx - SHADOW_OFFSET + xo, r1.ry - SHADOW_OFFSET + yo, r1.rx - SHADOW_OFFSET + xo + RoomWidth, r1.ry - SHADOW_OFFSET + yo + RoomHeight);
						*/
						}
					}
				}
			}

			/* draw grid */
			if (opt.DrawGrid && AreaLoaded) {
				// draw |
				x = (opt.scrollOffsetX % RoomDistance) - (RoomWidth / 2);
				while (x < bufferWidth * 3) {
					canvasMain.Line(penGrid, x, 0, x, bufferHeight * 3);
					x += RoomDistance;
				}
				// draw --
				//y = (opt.scrollOffsetY % RoomDistance) + (RoomHeight / 2);
				y = (opt.scrollOffsetY % RoomDistance) + RoomHeight + (RoomHeight / 2);
				while (y < bufferHeight * 3) {
					canvasMain.Line(penGrid, 0, y, bufferWidth * 3, y);
					y += RoomDistance;
				}
			}

			/* draw exits */
            if (_area.Exits.Count > 0 && opt.DrawExits) {
                foreach (var e in _area.Exits) {
					if (e.zTo != currentRoom.z && e.zFrom != currentRoom.z) continue;

                    var r1 = (Room)_area.Rooms[e.idFrom];
                    var r2 = (Room)_area.Rooms[e.idTo];
					if (r1 != null && r2 != null) {
						DrawExit(e, opt, 0, xo, yo);
					}
				}
			}

			/* draw rooms */
            if (_area.Rooms.Count > 0 && opt.DrawRooms) {
                foreach (var r1 in _area.Rooms.Values) {
					if (r1.z != currentRoom.z) continue;
					if (!r1.visible) continue;

					DrawRoom(r1, opt, false, xo, yo);
				}
			}

			/* draw labels */
			if (opt.DrawNotesAsLabel && ZoomLevel >= 3) {
                foreach (var r1 in _area.Rooms.Values) {
					if (r1.z != currentRoom.z) continue;
					if (r1.GetProperty("notes") != "") {
						string notes = r1.GetProperty("notes");
						// TODO: draw labels
						canvasMain.DrawString(notes, r1.rx + SHADOW_OFFSET + xo, r1.ry + RoomHeight + SHADOW_OFFSET + yo, fontRoom);

						//SizeF size = Buffer.MeasureString(notes, fontLabel);
						//Buffer.FillRectangle(brushLabel, r1.x  + ROOM_WIDTH + SHADOW_OFFSET, r1.y , size.Width + 5, size.Height + 4);
						//Buffer.DrawString(notes, fontLabel, brushFont, r1.x  + ROOM_WIDTH + 3, r1.y  + 2);
						//Buffer.DrawRectangle(penLabel, r1.x  + ROOM_WIDTH + SHADOW_OFFSET, r1.y , size.Width + 5, size.Height + 4);
					}
				}
			}

            canvasMain.Unlock();
        }

        private void DrawRoom(Room cRoom, Common.RenderOptions opt, bool shadow, int xo, int yo) {
            // draw with normal, excluded or selected background
            CanvasPen pen;

            if (!shadow) {
                if (Template == cRoom) {
                    pen = penTemplate;
                } else {
                    if (cRoom.IsIndoors) {
                        pen = penIndoors;
                    } else {
                        pen = penRoom;
                    }
                }
            } else {
                pen = penRoomShadow;
            }

            int x = cRoom.rx + xo;
            int y = cRoom.ry + yo;
            canvasMain.Rectangle(brushRoom, pen, x, y, x + RoomWidth, y + RoomWidth);

            // draw the ID
            if (opt.DrawIDs && !shadow && ZoomLevel >= 3) {
                canvasMain.DrawString(cRoom.id.ToString(), x + 2, y + 1, fontRoom);
            }

            // draw empty line if empty
            if (cRoom.empty && opt.ShowEmpty && !shadow) {
                canvasMain.Line(penEmptyRoom, x + 3, y + RoomHeight - 4, x + RoomWidth - 3, y + RoomHeight - 4);
            }

            if (cRoom.objects.Count > 0) {
                canvasMain.Line(penObjects, x + 3, y + RoomHeight - 6, x + RoomWidth - 3, y + RoomHeight - 6);
            }
        }

        private void SetExitCoordinates(Exit ex) {
            Room r1, r2;

            r1 = GetRoom(ex.idFrom);
            r2 = GetRoom(ex.idTo);
            if (ex.isCommon()) {
                if (r1 != null) {
                    ex.p1.X = GetExitX(ex.dirFrom, 0) + (r1.x * RoomDistance);
                    ex.p1.Y = GetExitY(ex.dirFrom, 0) + (r1.y * RoomDistance);
                }
                if (r2 != null) {
                    ex.p2.X = GetExitX(ex.dirTo, 0) + (r2.x * RoomDistance);
                    ex.p2.Y = GetExitY(ex.dirTo, 0) + (r2.y * RoomDistance);
                }
            } else if (ex.isLevel()) {
                if (r1 != null) {
                    ex.p1.X = (r1.x * RoomDistance) + RoomWidth + EXIT_DISTANCE;
                    ex.p1.Y = (r1.y * RoomDistance);
                    if (ex.dirFrom == "down") ex.p1.Y += RoomHeight;
                }
                if (r2 != null) {
                    ex.p2.X = (r2.x * RoomDistance) + RoomWidth + EXIT_DISTANCE;
                    ex.p2.Y = (r2.y * RoomDistance);
                    if (ex.dirTo == "down") ex.p2.Y += RoomHeight;
                }
            } else {
                if (r1 != null) {
					ex.p1.X = (r1.x * RoomDistance) + (RoomWidth / 2);
					ex.p1.Y = (r1.y * RoomDistance) + (RoomHeight / 2);
				}
                if (r2 != null) {
					ex.p2.X = (r2.x * RoomDistance) + (RoomWidth / 2);
					ex.p2.Y = (r2.y * RoomDistance) + (RoomHeight / 2);
				}
            }
        }

        private void UpdateMenus() {
            if (AreaLoaded) {
                menuToolsRoomEditor.Enabled = true;
                menuAreaSetup.Enabled = true;
                menuToolsCustomExit.Enabled = true;
                menuToolsShowExits.Enabled = true;
                menuAreaCompileRooms.Enabled = true;
                menuToolsRoomCopy.Enabled = true;
                menuToolsRoomSetter.Enabled = true;
                menuAreaInformation.Enabled = true;
                menuToolsSetTemplate.Enabled = true;
                menuToolsWordReplace.Enabled = true;
                menuToolsRoomSelect.Enabled = true;
                menuToolsControlWindow.Enabled = true;
                menuAreaCompileAll.Enabled = true;
                menuAreaGroups.Enabled = true;
                menuAreaObjects.Enabled = true;
                menuFileSave.Enabled = true;
                menuFileSaveAs.Enabled = true;
                menuFileClose.Enabled = true;
                menuFileSaveAsTemplate.Enabled = true;
                menuSelectionDelete.Enabled = true;
                menuSelectionInvert.Enabled = true;
                menuSelectionClear.Enabled = true;
                menuSelectionSelectAll.Enabled = true;
                menuViewGoTo.Enabled = true;
                toolSave.Enabled = true;
                toolEdit.Enabled = true;
                toolZoom.Enabled = true;
                toolObjects.Enabled = true;
                toolGroups.Enabled = true;
                toolCopyRoom.Enabled = true;
                toolCompile.Enabled = true;
            } else {
                menuToolsRoomEditor.Enabled = false;
                menuAreaSetup.Enabled = false;
                menuToolsCustomExit.Enabled = false;
                menuToolsShowExits.Enabled = false;
                menuAreaCompileRooms.Enabled = false;
                menuToolsRoomCopy.Enabled = false;
                menuToolsRoomSetter.Enabled = false;
                menuAreaInformation.Enabled = false;
                menuToolsSetTemplate.Enabled = false;
                menuToolsWordReplace.Enabled = false;
                menuToolsRoomSelect.Enabled = false;
                menuToolsControlWindow.Enabled = false;
                menuAreaCompileAll.Enabled = false;
                menuAreaGroups.Enabled = false;
                menuAreaObjects.Enabled = false;
                menuFileSave.Enabled = false;
                menuFileSaveAs.Enabled = false;
                menuFileClose.Enabled = false;
                menuFileSaveAsTemplate.Enabled = false;
                menuSelectionDelete.Enabled = false;
                menuSelectionInvert.Enabled = false;
                menuSelectionClear.Enabled = false;
                menuSelectionSelectAll.Enabled = false;
                menuViewGoTo.Enabled = false;
                toolSave.Enabled = false;
                toolEdit.Enabled = false;
                toolZoom.Enabled = false;
                toolObjects.Enabled = false;
                toolGroups.Enabled = false;
                toolCopyRoom.Enabled = false;
                toolCompile.Enabled = false;
            }

			menuViewRoomIDs.Checked = optStandard.DrawIDs;
        }

        private void DrawExit(Exit e, Common.RenderOptions opt, int level,int xo, int yo) {
			CanvasPen pen = null;
            bool shadow;
            int offset;
            int offX;
            int offY;

            if (level != 0) shadow = true; else shadow = false;
            offset = (shadow ? SHADOW_OFFSET * level : 0);
            offX = xo + offset;
            offY = yo + offset;
            int x1, x2, y1, y2;
            x1 = e.p1.X + offX;
            x2 = e.p2.X + offX;
            y1 = e.p1.Y + offY;
            y2 = e.p2.Y + offY;

            if (e.isCommon()) {
                // draw exit line
                if (e.invFrom || e.invTo) {
                    //oldPen = SelectObject(src, penInvisible);
                }
				if (shadow) pen = penExitShadow; else pen = penExit;
                canvasMain.Line(pen, x1, y1, x2, y2);

                if (opt.DrawExitSquares && !shadow && ZoomLevel >= 2) {
                    if (e.canFrom) {
                        canvasMain.Rectangle(brushExitSquare, penExit, x1 - 2, y1 - 2, x1 + 2, y1 + 2);
                    }
                    if (e.canTo) {
                        canvasMain.Rectangle(brushExitSquare, penExit, x2 - 2, y2 - 2, x2 + 2, y2 + 2);
                    }
                }
            } else if (LevelExits.Contains(e.dirFrom) && LevelExits.Contains(e.dirTo) && !shadow) {
                // draw +/-
                if (e.dirFrom == "up" && e.zFrom == currentRoom.z && e.canFrom) {
                    canvasMain.Line(penLevelExit, x1 - LEVELEXIT_OFFSET - 1, y1, x1 + LEVELEXIT_OFFSET, y1);
                    canvasMain.Line(penLevelExit, x1, y1 - LEVELEXIT_OFFSET - 1, x1, y1 + LEVELEXIT_OFFSET);
                } else if (e.dirFrom == "down" && e.zFrom == currentRoom.z && e.canFrom) {
                    canvasMain.Line(penLevelExit, x1 - LEVELEXIT_OFFSET - 1, y1, x1 + LEVELEXIT_OFFSET, y1);
                } else if (e.dirTo == "up" && e.zTo == currentRoom.z && e.canTo) {
                    canvasMain.Line(penLevelExit, x2 - LEVELEXIT_OFFSET - 1, y2, x2 + LEVELEXIT_OFFSET, y2);
                    canvasMain.Line(penLevelExit, x2, y2 - LEVELEXIT_OFFSET - 1, x2, y2 + LEVELEXIT_OFFSET);
                } else if (e.dirTo == "down" && e.zTo == currentRoom.z && e.canTo) {
                    canvasMain.Line(penLevelExit, x2 - LEVELEXIT_OFFSET - 1, y2, x2 + LEVELEXIT_OFFSET, y2);
                }

            } else if (!shadow) {
				// draw custom exit

                if (e.invFrom || e.invTo) {
                    //oldPen = SelectObject(src, penCustomExitInvis);
                } else {
                    //oldPen = SelectObject(src, penCustomExit);
                }
                if (e.canTo && e.canFrom) {
					canvasMain.Line(penCustomExit, x1, y1, x2, y2);
                }
                if (e.canFrom) {
                    //Rectangle(src, x1 - 2, y1 - 2, x1 + 2, y1 + 2);
                }
                if (e.canTo) {
                    //Rectangle(src, x2 - 2, y2 - 2, x2 + 2, y2 + 2);
                }
            }
        }

        private int GetExitX(string dir, int x) {
            int newX;

            newX = x;

            switch (dir) {
                case "north":
                    newX += (RoomWidth / 2);
                    break;
                case "northeast":
                    newX += RoomWidth;
                    break;
                case "east":
                    newX += RoomWidth;
                    break;
                case "southeast":
                    newX += RoomWidth;
                    break;
                case "south":
                    newX += (RoomWidth / 2);
                    break;
                case "southwest":
                    break;
                case "west":
                    break;
                case "northwest":
                    break;
            }

            return newX;
        }

        private int GetExitY(string dir, int y) {
            int newY;

            newY = y;

            switch (dir) {
                case "north":
                    break;
                case "northeast":
                    break;
                case "east":
                    newY += (RoomHeight / 2);
                    break;
                case "southeast":
                    newY += RoomHeight;
                    break;
                case "south":
                    newY += RoomHeight;
                    break;
                case "southwest":
                    newY += RoomHeight;
                    break;
                case "west":
                    newY += (RoomHeight / 2);
                    break;
                case "northwest":
                    break;
            }

            return newY;
        }

        public int GetNewId() {
            int i, id;

            if (_area.Rooms.Count == 0) {
                return 1;
            } else {
                id = 0;
                i = 1;

                while (id == 0) {
                    if (!_area.Rooms.ContainsKey(i)) id = i;
                    i++;
                }
                return id;
            }

        }

        public Room GetRoomAt(int x, int y, int z) {
            foreach (var r in _area.Rooms.Values) {
                if (r.x == x && r.y == y && r.z == z) return r;
            }

            return null;
        }

        public int ConvertScreenToMapX(int val) {
            return val - (optStandard.scrollOffsetX);
        }

        public int ConvertScreenToMapY(int val) {
            return val - (optStandard.scrollOffsetY);
        }

        public Room GetRoomAtXY(int x, int y) {
            foreach (var r in _area.Rooms.Values) {
				if (r.z != currentRoom.z) continue;

                if (x >= r.rx && x <= r.rx + RoomWidth &&
                    y >= r.ry && y <= r.ry + RoomHeight) {
                    return r;
                }
            }

            return null;
        }

        public Room GetRoom(int id) {
            if (_area.Rooms.ContainsKey(id)) {
                return _area.Rooms[id];
            }

            return null;
        }

        public void ToggleSelection(Room r, bool clear) {
            if (clear) ClearSelected();
            if (SelectedRooms.Contains(r)) SelectedRooms.Remove(r); else SelectedRooms.Add(r);
            UpdateStatus();
        }

        public void ToggleSelection(Exit e, bool clear) {
            if (clear) ClearSelected();
            if (SelectedExits.Contains(e)) SelectedExits.Remove(e); else SelectedExits.Add(e);
            UpdateStatus();
        }

        public void ClearSelected() {
            SelectedRooms = new List<Room>();
            SelectedExits = new List<Exit>();
            UpdateStatus();
        }

        public void RemoveSelected(Room r) {
            SelectedRooms.Remove(r);
        }

        private void formMain_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
            UpdateModifiers();

            if (currentRoom == null) return;

            HandleCursorMove(e.KeyCode);
        }

        public void HandleCursorMove(Keys k) {
            int cX, cY, cZ;
            Room oldR, newR;
            string dir;
            int MOVE = 1;
            bool modified = false;

            oldR = currentRoom;
            newR = null;
            cX = 0;
            cY = 0;
            cZ = 0;
            dir = "";

            switch (k) {
                case Keys.NumPad1:
                    cY += MOVE;
                    cX -= MOVE;
                    dir = "southwest";
                    break;
                case Keys.NumPad2:
                    cY += MOVE;
                    dir = "south";
                    break;
                case Keys.NumPad3:
                    cY += MOVE;
                    cX += MOVE;
                    dir = "southeast";
                    break;
                case Keys.NumPad4:
                    cX -= MOVE;
                    dir = "west";
                    break;
                case Keys.NumPad6:
                    cX += MOVE;
                    dir = "east";
                    break;
                case Keys.NumPad7:
                    cY -= MOVE;
                    cX -= MOVE;
                    dir = "northwest";
                    break;
                case Keys.NumPad8:
                    cY -= MOVE;
                    dir = "north";
                    break;
                case Keys.NumPad9:
                    cY -= MOVE;
                    cX += MOVE;
                    dir = "northeast";
                    break;
                case Keys.Add:
                    cZ -= 1;
                    dir = "down";
                    break;
                case Keys.Subtract:
                    cZ += 1;
                    dir = "up";
                    break;
                default:
                    return;
            }

            // try to find exit first
            foreach (Exit ex in _area.Exits) {
                if (ex.idFrom == oldR.id && ex.dirFrom == dir) {
                    newR = _area.Rooms[ex.idTo];
                    if (cZ != 0) modified = true;
                } else if (ex.idTo == oldR.id && ex.dirTo == dir) {
                    newR = _area.Rooms[ex.idFrom];
                    if (cZ != 0) modified = true;
                }
            }

            if (newR == null) {
                // see if a room is at the new coordinates
                newR = GetRoomAt(currentRoom.x + cX, currentRoom.y + cY, currentRoom.z + cZ);

                if (newR == null) {
                    // room is not there, make if in edit mode
                    if (_ControlPressed) {
                        newR = AddRoom(currentRoom.x + cX, currentRoom.y + cY, currentRoom.z + cZ);
                        modified = true;
                        // add new exit
                        AddExit(dir, oldR, Common.ReverseDirection(dir), newR);
                        UpdateEditorExits();
                    }
                } else {
                    // room is already there, but no exit was found so add exit if in edit mode
                    if (_ControlPressed) {
                        AddExit(dir, oldR, Common.ReverseDirection(dir), newR);
                        UpdateEditorExits();
                        modified = true;
                    } else if (!_ShiftPressed) {
                        // allow movement to room without existing exit or making one
                        newR = null;
                    }
                }
            }

            if (newR != null) {
                // new room is set so move there
                MakeCurrent(newR);
                UpdateStatus();
                // check to see if new room is in the view window
                Redraw(FocusOn(newR, false) || modified);
            } else {
                // no new room so stay put
            }
        }

        public int getRealX(int x) {
            return x;
        }

        public int getRealY(int y) {
            return y;
        }

        private bool FocusOn(Room r, bool center) {
            int x = getRealX(r.rx);
            int y = getRealY(r.ry);
            bool moved = false;

            if (center) {
                optStandard.scrollOffsetX = 0 - x + (ViewportWidth / 2);
                optStandard.scrollOffsetY = 0 - y + (ViewportHeight / 2);
                return true;
            }

            if (x + optStandard.scrollOffsetX < 0 + FOCUS_SPACING) {
                optStandard.scrollOffsetX = 0 - (x - FOCUS_OFFSET);
                moved = true;
            } else if (x + optStandard.scrollOffsetX > ViewportWidth - FOCUS_SPACING) {
                optStandard.scrollOffsetX = 0 - x + ViewportWidth - FOCUS_OFFSET;
                moved = true;
            }

            if (y + optStandard.scrollOffsetY < 0 + FOCUS_SPACING) {
                optStandard.scrollOffsetY = 0 - (y - FOCUS_OFFSET);
                moved = true;
            } else if (y + optStandard.scrollOffsetY > ViewportHeight - FOCUS_SPACING - statusMain.Height) {
                optStandard.scrollOffsetY = 0 - y + ViewportHeight - statusMain.Height - FOCUS_OFFSET;
                moved = true;
            }

            return moved;
        }

        public void UpdateStatus() {
            switch (editMode) {
                case CustomActions.CustomExit:
                    switch (editStep) {
                        case 1:
                            panelStatus.Text = "Select room 1";
                            break;
                        case 2:
                            panelStatus.Text = "Select room 2";
                            break;
                    }
                    break;
                case CustomActions.CopyRoom:
                    panelStatus.Text = "Select the room to copy properties from";
                    break;
                case CustomActions.SetTemplate:
                    panelStatus.Text = "Select the room to use as a template for new rooms";
                    break;
                default:
                    if (ShiftPressed) {
                        panelStatus.Text = "Drag a rectangle to select one or more rooms. Holding down Control will add to the selection.";
                    } else if (ControlPressed) {
                        panelStatus.Text = "Use the NumPad to create rooms. Use the cursor keys to move the current selection.";
                    } else {
                        if (SelectedRooms.Count > 0 && SelectedExits.Count > 0) {
                            panelStatus.Text = SelectedRooms.Count.ToString() + " room" + (SelectedRooms.Count == 1 ? "" : "s") + " selected, " + SelectedExits.Count.ToString() + " exit" + (SelectedExits.Count == 1 ? "" : "s") + " selected";
                        } else if (SelectedRooms.Count > 0) {
                            panelStatus.Text = SelectedRooms.Count.ToString() + " room" + (SelectedRooms.Count == 1 ? "" : "s") + " selected";
                        } else if (SelectedExits.Count > 0) {
                            panelStatus.Text = SelectedExits.Count.ToString() + " exit" + (SelectedExits.Count == 1 ? "" : "s") + " selected";
                        } else {
                            panelStatus.Text = "Ready";
                        }
                    }
                    break;
            }
        }

        private void formMain_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e) {
            UpdateModifiers();

            if (currentRoom == null) return;

            switch (e.KeyCode) {
                case Keys.Space:
                    if (!SelectedRooms.Contains(currentRoom)) {
                        ToggleSelection(currentRoom, false);
                    } else {
                        RemoveSelected(currentRoom);
                    }
                    //Redraw(true);
                    Redraw(false);
                    break;
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    if (ControlPressed) MoveRooms(e.KeyCode);
                    if (_ShiftPressed) MoveView(e.KeyCode);
                    break;
                case Keys.Escape:
                    ResetEditMode();
                    ClearSelected();
                    Redraw(false);
                    break;
                case Keys.Tab:
                    if (Editor != null) Editor.Focus();
                    break;
                case Keys.NumPad5:
                    CenterOnCurrent();
                    Redraw(true);
                    break;
            }
        }

        public void MoveView(System.Windows.Forms.Keys k) {
            if (SelectedRooms.Count != 0) return;

            // TODO: use xTempOffset and then updatescreen afterwards
            switch (k) {
                case Keys.Up:
                    yTempOffset += (int)(ViewportHeight * 1/2);
                    break;
                case Keys.Down:
                    yTempOffset -= (int)(ViewportHeight * 1 / 2);
                    break;
                case Keys.Left:
                    xTempOffset += (int)(ViewportWidth * 1 / 2);
                    break;
                case Keys.Right:
                    xTempOffset -= (int)(ViewportWidth * 1 / 2);
                    break;
            }

            // do a light update first - appears faster
            Redraw(false);

            optStandard.scrollOffsetY += yTempOffset;
            yTempOffset = 0;
            optStandard.scrollOffsetX += xTempOffset;
            xTempOffset = 0;

            // now redraw to update backbuffer
            Redraw(true);
        }

        public void MoveRooms(System.Windows.Forms.Keys k) {
            Room newR;
            List<int> al = new List<int>();
            int cX, cY;
            int MOVE = 1;

            if (SelectedRooms.Count == 0) return;

            newR = null;
            cX = 0;
            cY = 0;

            switch (k) {
                case Keys.Up:
                    cY -= MOVE;
                    break;
                case Keys.Down:
                    cY += MOVE;
                    break;
                case Keys.Left:
                    cX -= MOVE;
                    break;
                case Keys.Right:
                    cX += MOVE;
                    break;
            }

            // move selected rooms if not blocked
            foreach (Room r in SelectedRooms) {
                newR = GetRoomAt(r.x + cX, r.y + cY, r.z);
                if (newR != null && !SelectedRooms.Contains(newR)) {
                    System.Media.SystemSound ss = System.Media.SystemSounds.Beep;
                    ss.Play();
                    return;
                }
            }

            // move is okay
            foreach (Room r in SelectedRooms) {
                r.x += cX;
                r.y += cY;
                r.UpdateCoordinates(RoomDistance);
                al.Add(r.id);
            }

            Saved = false;

            foreach (Exit ex in _area.Exits) {
                if (al.Contains(ex.idFrom) || al.Contains(ex.idTo)) {
                    SetExitCoordinates(ex);
                }
            }

            Redraw(true);
        }

        public void DeleteRoom(Room r) {
            List<Exit> exitsToDelete;

            exitsToDelete = new List<Exit>();

            if (r == currentRoom) currentRoom = null;

            foreach (Exit e in _area.Exits) {
                if (e.idFrom == r.id) {
                    if (currentRoom == null) currentRoom = _area.Rooms[e.idTo];
                    exitsToDelete.Add(e);

                } else if (e.idTo == r.id) {
                    if (currentRoom == null) currentRoom = _area.Rooms[e.idFrom];
                    exitsToDelete.Add(e);
                }
            }

            DeleteExits(exitsToDelete);

            _area.Rooms.Remove(r.id);
        }

        public void DeleteExits(List<Exit> e) {
            foreach (Exit ex in e) {
                _area.Exits.Remove(ex);
            }
        }

        public void LoadEditor() {
            if (Editor == null) {
                Editor = new formEditor(this, _area);
                Editor.Opacity = EditorOpacity;
                Editor.StartPosition = FormStartPosition.Manual;
                if (options.DockEditor == true && this.WindowState != FormWindowState.Maximized) {
                    Editor.Left = this.Left + this.Width;
                    Editor.Top = this.Top;
                } else {
                    Editor.Left = EditorLeft;
                    Editor.Top = EditorTop;
                }
                Editor.Height = EditorHeight;
                Editor.Width = EditorWidth;
                Editor.Show();
            }
        }

        public void LoadCurrentRoom(bool focus) {
            if (currentRoom == null) return;

            Editor.LoadRoom(ref currentRoom);

            if (!focus) this.Focus();
        }

        public void ClearArea(bool KeepName) {
            _area.Clear();
            if (!KeepName) _area.SaveFile = string.Empty;
            SelectedRooms.Clear();
            AreaLoaded = false;
        }

        public void LoadNewArea() {
            int x, y;

            if (AreaLoaded) {
                ClearArea(false);
            }

            AreaLoaded = true;
            Saved = false;

            x = 0;
            y = 0;
            currentRoom = AddRoom(x, y, 0);

            optStandard.scrollOffsetX = (int)Width / 2;
            optStandard.scrollOffsetY = (int)Height / 2;

            UpdateMenus();
            UpdateStatus();

            Redraw(true);
        }

        public void UpdateTitle() {
            if (!AreaLoaded) {
                Text = Common.APP_NAME;
            } else {
                Text = Common.APP_NAME + " - " + Path.GetFileName(_area.SaveFile);
                if (!Saved) {
                    Text += "*";
                }
                if (currentRoom != null) Text += " [" + currentRoom.z + "]";
            }
        }

        private void NewArea() {
            if (!Saved) {
                if (MessageBox.Show("The current area has not been saved. Are you sure you want to start a new area?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    return;
                }
            }
            var dialog = new formNewArea();
            if (dialog.ShowDialog() == DialogResult.Cancel) return;
            LoadNewArea();
            if (dialog.SelectedTemplate != formNewArea.BlankTemplateLabel) {
                var d = new XmlDocument();
                try {
                    d.Load(Common.GetTemplatesPath() + dialog.SelectedTemplate + Common.EXT_AREATEMPLATE);
                } catch (Exception e) {
                    Log.error(e);
                    return;
                }
                XmlNodeList nodeList;
                nodeList = d.SelectNodes("/template/constants/*");
                LoadConstantsFromXml(nodeList);

                nodeList = d.SelectNodes("/template/groups/*");
                LoadGroupsFromXml(nodeList);
            }
            dialog.Close();
        }

        private void LoadConstantsFromXml(XmlNodeList nodeList) {
            string nodeName, nodeValue;

            foreach (XmlNode n in nodeList) {
                nodeName = n.Name;
                nodeValue = n.InnerText;
                _area.Constants.Add(nodeName, nodeValue);
            }
        }

        private void LoadGroupsFromXml(XmlNodeList nodeList) {
            string nodeName, nodeValue;
            Group group;

            foreach (System.Xml.XmlNode node in nodeList) {
                group = new Group();
                foreach (System.Xml.XmlNode currentNode in node.ChildNodes) {
                    nodeName = currentNode.Name;
                    nodeValue = currentNode.InnerText;
                    switch (nodeName) {
                        case "name":
                            group.Name = nodeValue;
                            break;
                        case "template":
                            group.Template = StripExtensionAndPath(nodeValue);
                            break;
                        case "folder":
                            group.Folder = StripPath(nodeValue);
                            break;
                        case "layoutfile":
                            group.LayoutFile = StripExtensionAndPath(nodeValue);
                            break;
                    }
                }
                AddGroup(group);
            }
        }

        private void formMain_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            DialogResult r;

            if (!Saved) {
                r = MessageBox.Show("The current area has not been saved. Would you like to save before closing " + Common.APP_NAME + "?", Common.APP_NAME, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (r == DialogResult.Cancel) {
                    e.Cancel = true;
                } else if (r == DialogResult.Yes) {
                    SaveArea(false, false);
                }
            }
        }

        private bool SaveArea(bool forceSaveAs, bool cancelOnNew) {
            XmlTextWriter x;

            if (_area.SaveFile == string.Empty || forceSaveAs) {
                if (cancelOnNew) return true;

                // no name yet or save as request, open save as dialog
                fileSave.AddExtension = true;
                fileSave.DefaultExt = Common.EXT_MAP;
                fileSave.Filter = "Genesis Map Files (*" + Common.EXT_MAP + ")|*" + Common.EXT_MAP;
                fileSave.OverwritePrompt = true;
                fileSave.ValidateNames = true;

                if (fileSave.ShowDialog() == DialogResult.Cancel) {
                    return false;
                }

                _area.SaveFile = fileSave.FileName;
            }

            // write file
            x = new System.Xml.XmlTextWriter(_area.SaveFile, System.Text.Encoding.Default);

            x.Formatting = System.Xml.Formatting.Indented;
            x.Indentation = 4;
            x.IndentChar = ' ';

            x.WriteRaw("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");

            // base element
            x.WriteStartElement("area");
            x.WriteAttributeString("version", Common.APP_VERSION);

            // write area exits
            x.WriteStartElement("exits");
            foreach (Exit e in _area.Exits) {
                x.WriteStartElement("exit");

                WriteXMLValue(x, "zFrom", e.zFrom.ToString());
                WriteXMLValue(x, "zTo", e.zTo.ToString());
                WriteXMLValue(x, "idFrom", e.idFrom.ToString());
                WriteXMLValue(x, "idTo", e.idTo.ToString());
                WriteXMLValue(x, "dirFrom", e.dirFrom);
                WriteXMLValue(x, "dirTo", e.dirTo);
                WriteXMLValue(x, "invFrom", e.invFrom.ToString());
                WriteXMLValue(x, "invTo", e.invTo.ToString());
                WriteXMLValue(x, "canFrom", e.canFrom.ToString());
                WriteXMLValue(x, "canTo", e.canTo.ToString());

                x.WriteEndElement();
            }
            x.WriteEndElement();

            // write area rooms
            x.WriteStartElement("rooms");
            foreach (var r in _area.Rooms.Values) {
                x.WriteStartElement("room");

                WriteXMLValue(x, "id", r.id.ToString());
                WriteXMLValue(x, "x", r.x.ToString());
                WriteXMLValue(x, "y", r.y.ToString());
                WriteXMLValue(x, "z", r.z.ToString());
                WriteXMLValue(x, "include", r.include.ToString());

                x.WriteStartElement("properties");
                foreach (KeyValuePair<String,String> DE in r.properties) {
                    WriteXMLValue(x, DE.Key, DE.Value);
                }
                x.WriteEndElement();

                x.WriteStartElement("objects");
                foreach (GenesisObject go in r.objects) {
                    WriteXMLValue(x, "object", go.id);
                }
                x.WriteEndElement();

                x.WriteEndElement();
            }
            x.WriteEndElement();

            x.WriteStartElement("objects");
            foreach (var o in _area.Objects.Values) {
                x.WriteStartElement("object");

                WriteXMLValue(x, "id", o.id);
                WriteXMLValue(x, "group", o.Group.Name);

                x.WriteStartElement("properties");
                foreach (KeyValuePair<String,String> DE2 in o.properties) {
                    WriteXMLValue(x, DE2.Key, DE2.Value);
                }
                x.WriteEndElement();

                x.WriteStartElement("objects");
                foreach (GenesisObject go in o.objects) {
                    WriteXMLValue(x, "object", go.id);
                }
                x.WriteEndElement();

                x.WriteEndElement();
            }
            x.WriteEndElement();

            // groups
            WriteGroupsToXml(x);

            // constants
            WriteConstantsToXml(x);

            // write area properties
            x.WriteStartElement("settings");

            WriteXMLValue(x, "xOffset", optStandard.scrollOffsetX.ToString());
            WriteXMLValue(x, "yOffset", optStandard.scrollOffsetY.ToString());
            if (currentRoom != null) {
                WriteXMLValue(x, "CurrentID", currentRoom.id.ToString());
            }
            WriteXMLValue(x, "AreaFolder", _area.RoomFolderName);
            WriteXMLValue(x, "AreaTemplate", _area.DefaultRoomTemplate);
            WriteXMLValue(x, "AreaFilename", _area.DefaultRoomFileNamePrefix);
            WriteXMLValue(x, "IDStart", _area.RoomIdOffset.ToString());

            if (Template != null) {
                WriteXMLValue(x, "Template", Template.id.ToString());
            }

            if (SelectedRooms.Count > 0) {
                var s = "";
                foreach (Room room in SelectedRooms) {
                    if (s.Length != 0) s += ";";
                    s += room.id.ToString();
                }
                WriteXMLValue(x, "Selected", s);
            }

            x.WriteEndElement();

            // write closing area element
            x.WriteEndElement();

            x.Close();

            Saved = true;

            return true;
        }

        private void WriteConstantsToXml(System.Xml.XmlTextWriter x) {
            x.WriteStartElement("constants");
            foreach (KeyValuePair<string, string> DE in _area.Constants) {
                WriteXMLValue(x, DE.Key, DE.Value);
            }
            x.WriteEndElement();
        }

        private void WriteGroupsToXml(System.Xml.XmlTextWriter x) {
            x.WriteStartElement("groups");
            foreach (var gg in _area.Groups.Values) {
                if (gg.Name == "") continue;

                x.WriteStartElement("group");

                WriteXMLValue(x, "name", gg.Name);
                WriteXMLValue(x, "template", gg.Template);
                WriteXMLValue(x, "folder", gg.Folder);
                WriteXMLValue(x, "layoutfile", gg.LayoutFile);

                x.WriteEndElement();
            }
            x.WriteEndElement();
        }

        private void WriteXMLValue(System.Xml.XmlTextWriter x, string n, string v) {
            x.WriteStartElement(n);
            x.WriteString(v);
            x.WriteEndElement();
        }

        private void LoadArea(bool SkipOpen) {
            DialogResult dr;

            fileOpen.AddExtension = true;
            fileOpen.CheckFileExists = true;
            fileOpen.DefaultExt = Common.EXT_MAP;
            fileOpen.Filter = "Map files (*" + Common.EXT_MAP + ")|*" + Common.EXT_MAP;
            fileOpen.Multiselect = false;
            fileOpen.ShowReadOnly = false;
            fileOpen.ValidateNames = true;

            if (!Saved) {
                dr = MessageBox.Show("The current area has not been saved. Would you like to save the current area?", Common.APP_NAME, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel) {
                    return;
                } else if (dr == DialogResult.Yes) {
                    if (!SaveArea(false, false)) {
                        // user canceled on save, cancel loading
                        return;
                    }
                }
            }

            if (!SkipOpen) {
                if (fileOpen.ShowDialog() == DialogResult.Cancel) {
                    return;
                }
                ClearArea(false);
                _area.SaveFile = fileOpen.FileName;
            } else {
                ClearArea(true);
            }

            LoadAreaFromFile(_area.SaveFile);
            UpdateSizes();
            UpdateMenus();
            UpdateStatus();
            Saved = true;
            Redraw(true);
        }

        private void LoadAreaFromFile(string FileName) {
            _area = new Area();

            System.Xml.XmlDocument d;
            System.Xml.XmlNodeList l;
            System.Xml.XmlNode xn = null;
            string sn, sv;
            bool fixCoordinates = false;
            Array a;
            Exit e;
            Room r;
            GenesisObject o;

            AreaLoaded = true;

            d = new System.Xml.XmlDocument();
            try {
                d.Load(FileName);
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
                return;
            }

            xn = d.SelectSingleNode("/area");
            if (xn == null) {
                fixCoordinates = true;
            } else {
                float v = float.Parse(xn.Attributes["version"].Value, System.Globalization.NumberStyles.AllowDecimalPoint);
                if (v < 0.97) {
                    fixCoordinates = true;
                }
            }

            int offsetX = 0;
            int offsetY = 0;
            if (fixCoordinates) {
                int value;
                l = d.GetElementsByTagName("x");
                if (l.Count > 0) {
                    value = Convert.ToInt32(l.Item(0).Value);
                    offsetX = Math.Abs(value % 50);
                }
                l = d.GetElementsByTagName("y");
                if (l.Count > 0) {
                    value = Convert.ToInt32(l.Item(0).Value);
                    offsetY = Math.Abs(value % 50);
                }
            }

            l = d.SelectNodes("/area/constants/*");
            LoadConstantsFromXml(l);

            l = d.SelectNodes("/area/groups/*");
            LoadGroupsFromXml(l);

            l = d.SelectNodes("/area/objects/*");
            foreach (System.Xml.XmlNode n in l) {
                o = new GenesisObject();
                foreach (System.Xml.XmlNode c in n.ChildNodes) {
                    sn = c.Name;
                    sv = c.InnerText;
                    switch (sn) {
                        case "id":
                            o.id = sv;
                            break;
                        case "group":
                            o.Group = FindGroupByName(Convert.ToString(sv));
                            break;
                        case "objects":
                            foreach (System.Xml.XmlNode c2 in c.ChildNodes) {
                                o.objects.Add(c2.InnerText);
                            }
                            break;
                        case "properties":
                            foreach (System.Xml.XmlNode c2 in c.ChildNodes) {
                                o.SetProperty(c2.Name, c2.InnerText);
                            }
                            break;
                    }
                }
                AddObject(o);
            }
            // update all strings to the actual object
            foreach (var go in _area.Objects.Values) {
                List<Object> strings = go.objects;
                go.objects = new List<Object>();
                foreach (string str in strings) {
                    o = FindObjectByName(str);
                    if (o == null) continue;
                    go.objects.Add(o);
                }
            }

            l = d.SelectNodes("/area/rooms/*");
            foreach (System.Xml.XmlNode n in l) {
                r = new Room();
                foreach (System.Xml.XmlNode c in n.ChildNodes) {
                    sn = c.Name;
                    sv = c.InnerText;
                    switch (sn) {
                        case "id":
                            r.id = Convert.ToInt32(sv);
                            break;
                        case "x":
                            if (fixCoordinates) {
                                r.x = (int)((Convert.ToInt32(sv) - offsetX) / 50);
                            } else {
                                r.x = Convert.ToInt32(sv);
                            }
                            break;
                        case "include":
                            r.include = Boolean.Parse(sv);
                            break;
                        case "y":
                            if (fixCoordinates) {
                                r.y = (int)((Convert.ToInt32(sv) - offsetY) / 50);
                            } else {
                                r.y = Convert.ToInt32(sv);
                            }
                            break;
                        case "z":
                            r.z = Convert.ToInt32(sv);
                            break;
                        case "objects":
                            foreach (System.Xml.XmlNode c2 in c.ChildNodes) {
                                r.objects.Add(c2.InnerText);
                            }
                            break;
                        case "properties":
                            foreach (System.Xml.XmlNode c2 in c.ChildNodes) {
                                r.SetProperty(c2.Name, c2.InnerText);
                            }
                            break;
                    }
                }
                AddRoom(r);
            }
            // update all strings to the actual object
            foreach (var ro in _area.Rooms.Values) {
                List<Object> strings = ro.objects;
                ro.objects = new List<Object>();
                foreach (string str in strings) {
                    o = FindObjectByName(str);
                    if (o == null) continue;
                    ro.objects.Add(o);
                }
            }

            l = d.SelectNodes("/area/exits/*");
            foreach (System.Xml.XmlNode n in l) {
                e = new Exit();

                foreach (System.Xml.XmlNode c in n.ChildNodes) {
                    sn = c.Name;
                    sv = c.InnerText;
                    switch (sn) {
                        case "zTo":
                            e.zTo = Convert.ToInt32(sv);
                            break;
                        case "zFrom":
                            e.zFrom = Convert.ToInt32(sv);
                            break;
                        case "idFrom":
                            e.idFrom = Convert.ToInt32(sv);
                            break;
                        case "idTo":
                            e.idTo = Convert.ToInt32(sv);
                            break;
                        case "dirFrom":
                            e.dirFrom = sv;
                            break;
                        case "dirTo":
                            e.dirTo = sv;
                            break;
                        case "canFrom":
                            e.canFrom = Boolean.Parse(sv);
                            break;
                        case "canTo":
                            e.canTo = Boolean.Parse(sv);
                            break;
                        case "invFrom":
                            e.invFrom = Boolean.Parse(sv);
                            break;
                        case "invTo":
                            e.invTo = Boolean.Parse(sv);
                            break;
                    }
                }
                AddExit(e);
            }

            l = d.SelectNodes("/area/settings/*");
            foreach (System.Xml.XmlNode c in l) {
                sn = c.Name;
                sv = c.InnerText;
                switch (sn) {
                    case "xOffset":
                        optStandard.scrollOffsetX = Convert.ToInt32(sv);
                        break;
                    case "yOffset":
                        optStandard.scrollOffsetY = Convert.ToInt32(sv);
                        break;
                    case "CurrentID":
                        r = GetRoom(Convert.ToInt32(sv));
                        if (r != null) MakeCurrent(r);
                        break;
                    case "AreaFolder":
                        _area.RoomFolderName = StripPath(sv);
                        break;
                    case "AreaTemplate":
                        _area.DefaultRoomTemplate = StripExtensionAndPath(sv);
                        break;
                    case "AreaFilename":
                        _area.DefaultRoomFileNamePrefix = sv;
                        break;
                    case "Template":
                        Template = GetRoom(Convert.ToInt32(sv));
                        break;
                    case "IDStart":
                        _area.RoomIdOffset = Convert.ToInt32(sv);
                        break;
                    case "Selected":
                        a = sv.Split(';');
                        foreach (String s in a) {
                            r = GetRoom(Convert.ToInt32(s));
                            if (r != null) SelectedRooms.Add(r);
                        }
                        break;
                }
            }

            UpdateGroups();
            ValidateRooms();
        }

        private string StripPath(string file) {
            return Path.GetFileName(file);
        }

        private string StripExtensionAndPath(string file) {
            return Path.GetFileNameWithoutExtension(file);
        }

        private string StripExtension(string file) {
            return Path.Combine(Path.GetDirectoryName(file), Path.GetFileNameWithoutExtension(file));
        }

        private void UpdateGroups() {
            foreach (var group in _area.Groups.Values) {
                UpdateGroup(group);
            }
        }

        public void UpdateGroup(Group g) {
            // TODO:
            /*
            if (g.LayoutFile == null || g.LayoutFile.Length == 0 || (g.layout = LoadLayoutFile(g.LayoutFile)) == null) {
                g.layout = defaultObjectLayout;
            }
            */
        }

        private void InvertSelection() {
            List<Room> oldSelected = new List<Room>();

            foreach (Room r2 in SelectedRooms) {
                oldSelected.Add(r2);
            }

            ClearSelected();

            foreach (var r in _area.Rooms.Values) {
                if (!oldSelected.Contains(r) && r.z == currentRoom.z) SelectedRooms.Add(r);
            }

            Redraw(false);
            UpdateStatus();
        }

        private void HandleDelete() {
            if (SelectedRooms.Count > 0) {
                foreach (Room r in SelectedRooms) {
                    DeleteRoom(r);
                }
            } else if (SelectedExits.Count > 0) {
                DeleteExits(SelectedExits);
            }

			if (currentRoom == null) {
                currentRoom = _area.Rooms.GetEnumerator().Current.Value;
			}

            Saved = false;
            ClearSelected();
            Redraw(true);
        }

        private void formMain_Load(object sender, System.EventArgs e) {
            Array a;

            UpdateMenus();

            // parse command line arguments
            a = Environment.GetCommandLineArgs();
            if (a.Length > 0) {
                foreach (string s in a) {
                    if (s.EndsWith(Common.EXT_MAP)) {
                        _area.SaveFile = s;
                        LoadArea(true);
                    }
                }
            }

            // load settings
            if (!LoadSettings()) {
                options.SetDefaults();
            }

            Redraw(true);
        }

        private void SetupDefaultLayouts() {
            if (options.DefaultRoomLayoutFile == null || options.DefaultRoomLayoutFile.Length == 0 || (defaultRoomLayout = Genesis.Layouts.GetLayout(options.DefaultRoomLayoutFile)) == null) {
                DetailField field;
                defaultRoomLayout = new DetailLayout();
                field = new FieldText();
                field.Name = "Short";
                field.Source = "short";
                defaultRoomLayout.Add(field);
                field = new FieldTextArea();
                ((FieldTextArea)field).Height = 120;
                field.Name = "Long";
                field.Source = "long";
                defaultRoomLayout.Add(field);
                field = new FieldTextArea();
                ((FieldTextArea)field).Height = 80;
                field.Name = "Long (Night)";
                field.Source = "long_night";
                defaultRoomLayout.Add(field);
            }
            if (options.DefaultObjectLayoutFile == null || options.DefaultObjectLayoutFile.Length == 0 || (defaultObjectLayout = Genesis.Layouts.GetLayout(options.DefaultObjectLayoutFile)) == null) {
                DetailField field;
                defaultObjectLayout = new DetailLayout();
                field = new FieldText();
                field.Name = "Short";
                field.Source = "short";
                defaultObjectLayout.Add(field);
                field = new FieldTextArea();
                ((FieldTextArea)field).Height = 120;
                field.Name = "Long";
                field.Source = "long";
                defaultObjectLayout.Add(field);
            }
        }

        public void Redraw(bool modified) {
            if (modified) UpdateBuffer();
            UpdateScreen();
        }

        private void ResetEditMode() {
            editMode = CustomActions.None;
            editStep = 0;
            UpdateStatus();
            _ControlPressed = false;
            _ShiftPressed = false;
        }

        private void CreateCustomExit(List<BaseObject> l) {
            Room r1, r2;
            formExit fExit;

            // create custom exit between the two rooms passed in arraylist
            r1 = (Room)l[0];
            r2 = (Room)l[1];
            if (r1 == null || r2 == null) return;

            fExit = new formExit();
            fExit.textFrom.Text = oldDir1;
            fExit.textFrom.SelectAll();
            fExit.textTo.Text = oldDir2;
            fExit.textTo.SelectAll();
            fExit.labelFrom.Text = r1.id.ToString();
            fExit.labelTo.Text = r2.id.ToString();

            if (fExit.ShowDialog() == DialogResult.OK) {
                AddExit(fExit.textFrom.Text, r1, fExit.textTo.Text, r2);
                oldDir1 = fExit.textFrom.Text;
                oldDir2 = fExit.textTo.Text;
            }

            Redraw(true);
        }

        private void HandleMouseDown(int x, int y, MouseButtons mb) {
            UpdateModifiers();

            if (mb == MouseButtons.Right) {
                IsDragging = true;
                oldX = x;
                oldY = y;
                xTempOffset = 0;
                yTempOffset = 0;
                HasMoved = false;
            } else if (mb == MouseButtons.Left) {
                if (_ShiftPressed) {
                    IsSelecting = true;
                    oldX = x;
                    oldY = y;
                }
            }
        }

        private Exit GetExitXY(int x, int y, int z) {
            foreach (Exit ex in _area.Exits) {
                //if (!ex.visible) continue;
                if (ex.zTo != z || ex.zFrom != z) continue;
                if (Common.Distance(ex.p1.X, ex.p1.Y, x, y) < CLICK_DISTANCE
                    || Common.Distance(ex.p2.X, ex.p2.Y, x, y) < CLICK_DISTANCE) {

                    return ex;
                }
            }

            return null;
        }

        private void formMain_Paint(object sender, System.Windows.Forms.PaintEventArgs e) {
            //Render(ref gBuffer, ref gScreen, ref iBuffer, optStandard);
            Redraw(false);
			//RenderAlt(optStandard);
		}

        private void HandleMouseMove(int x, int y) {
            if (IsDragging) {
                if (!HasMoved) this.Cursor = Cursors.SizeAll;
                xTempOffset = (x - oldX);
                yTempOffset = (y - oldY);
                HasMoved = true;
                Redraw(false);
            }
            if (IsSelecting) {
                newX = x;
                newY = y;
                Redraw(false);
            }
        }

        private void HandleMouseUp(int x, int y, MouseButtons mb) {
            Exit ex;
            formCopy f;
            bool modified = false;

            if (currentRoom == null) return;

            this.Cursor = Cursors.Default;

            UpdateModifiers();

            if (IsDragging && HasMoved) {
                IsDragging = false;
                xTempOffset = (x - oldX);
                yTempOffset = (y - oldY);
                optStandard.scrollOffsetX += xTempOffset;
                optStandard.scrollOffsetY += yTempOffset;
                xTempOffset = 0;
                yTempOffset = 0;
                Redraw(true);
                return;
            } else {
                IsDragging = false;
            }

            if (IsSelecting) {
                if (!_ControlPressed) ClearSelected();
                IsSelecting = false;
                newX = 0;
                newY = 0;
                int x1 = ConvertScreenToMapX(oldX);
                int y1 = ConvertScreenToMapY(oldY);
                int x2 = ConvertScreenToMapX(x);
                int y2 = ConvertScreenToMapY(y);
                if (x1 > x2) {
                    int t = x1;
                    x1 = x2;
                    x2 = t;
                }
                if (y1 > y2) {
                    int t = y1;
                    y1 = y2;
                    y2 = t;
                }
                foreach (var room in _area.Rooms.Values) {
                    if (!SelectedRooms.Contains(room) && currentRoom.z == room.z && room.isWithin(x1, y1, x2, y2, RoomWidth, RoomHeight)) {
                        ToggleSelection(room, false);
                    }
                }
                foreach (Exit exit in _area.Exits) {
                    if (!SelectedExits.Contains(exit) && exit.isCommon() && exit.isWithin(x1, y1, x2, y2) && (exit.zFrom == currentRoom.z || exit.zTo == currentRoom.z)) {
                        ToggleSelection(exit, false);
                    }
                }
                Redraw(false);
                return;
            }

            var r = GetRoomAtXY(ConvertScreenToMapX(x), ConvertScreenToMapY(y));

            switch (editMode) {
                case CustomActions.CustomExit:
                    if (r != null) {
                        if (editStep == 1) {
                            editObjects.Add(r);
                            editStep++;
                            UpdateStatus();
                        } else if (editStep == 2) {
                            if (editObjects.Contains(r)) return;
                            editObjects.Add(r);
                            if (editObjects.Count != 2) {
                                ResetEditMode();
                                return;
                            }
                            CreateCustomExit(editObjects);
                            editObjects.Clear();
                            ResetEditMode();
                        }
                    }
                    return;
                case CustomActions.CopyRoom:
                    if (r != null) {
                        if (editStep == 1 && SelectedRooms.Count > 0) {
                            f = new formCopy(this);
                            f.LoadRoom(r);
                            f.ShowDialog();
                            ResetEditMode();
                        } else if (editStep == 1 && SelectedRooms.Count < 1) {
                            MessageBox.Show("You need to have rooms selected for this function to work.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    return;
                case CustomActions.SetTemplate:
                    if (r != null) {
                        if (Template == r) Template = null; else Template = r;
                        ResetEditMode();
                        Redraw(true);
                    }
                    return;
            }

            if (mb == MouseButtons.Left) {
                if (r != null) {
                    ToggleSelection(r, !_ControlPressed);
                } else {
                    // see if an exit was clicked
                    ex = GetExitXY(ConvertScreenToMapX(x), ConvertScreenToMapY(y), currentRoom.z);
                    if (ex != null && ex.isCommon()) {
                        ToggleSelection(ex, !_ControlPressed);
                    } else if (!_ControlPressed) ClearSelected();
                }
            } else if (mb == MouseButtons.Right) {
                if (r != null) {
                    MakeCurrent(r);
                    if (_ControlPressed) {
                        LoadEditor();
                        LoadCurrentRoom(_ShiftPressed);
                    }
                }
            }

            //UpdateStatus();
            Redraw(modified);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent) {
            // do not draw background here
            // flicker/redraw fix
        }

        private void ValidateRooms() {
            foreach (var room in _area.Rooms.Values) {
                room.Validate(EmptyProperties);
            }
        }

        private bool CheckFolderExists(string Folder) {
            var fullFolder = Path.Combine(_area.SavePath, Folder);

            if (Directory.Exists(fullFolder)) return true;

            try {
                Directory.CreateDirectory(fullFolder);
            } catch {
                return false;
            }

            if (!Directory.Exists(fullFolder)) return false;

            return true;
        }

        private void CompileRooms() {
            var unselect = false;

            if (!Saved) {
                Common.Warn("You must save your area before compiling.");
                return;
            }

            if (!CheckFolderExists(_area.RoomFolderName)) {
                Common.Warn("Unable to create target folder for rooms.");
                return;
            }

            if (_area.DefaultRoomTemplate.Length == 0
                || _area.RoomFolderName.Length == 0
                || _area.DefaultRoomFileNamePrefix.Length == 0) {

                Common.Error("Some required settings are not set yet for this area. Please go to Area Setup to configure them.");
                return;
            }

            var progressForm = new formProgress();
            progressForm.StartPosition = FormStartPosition.CenterParent;
            progressForm.Text = "Compile Progress";

            if (SelectedRooms.Count == 0) {
                SelectAll();
                unselect = true;
            }

            progressForm.labelDescription.Text = "Compiling rooms...";
            progressForm.barProgress.Minimum = 0;
            progressForm.barProgress.Value = 0;
            progressForm.barProgress.Maximum = SelectedRooms.Count;
            progressForm.Show();
            Application.DoEvents(); // ensure form is displayed

            try {
                var compiler = new AreaCompiler(_area, ExcludeOld, options, () => { progressForm.barProgress.Value += 1; });
                var compileResult = compiler.CompileRooms(SelectedRooms);

#if DEBUG
                MessageBox.Show("Compiled Rooms: " + compileResult, Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
            } finally {
                progressForm.Close();
            }

            if (unselect) ClearSelected();
        }

        public GenesisObject FindObject(string name) {
            if (_area.Objects.ContainsKey(name)) {
                return _area.Objects[name];
            }
            
            return null;
        }

        private void formMain_LocationChanged(object sender, System.EventArgs e) {
            if (Editor != null && this.WindowState != FormWindowState.Maximized && DockEditor) {
                Editor.Left = this.Left + this.Width;
                Editor.Top = this.Top;
            }
        }

        public void UpdateSizes() {
            int minX, minY, maxX, maxY;

            minX = Int32.MaxValue;
            minY = Int32.MaxValue;
            maxX = Int32.MinValue;
            maxY = Int32.MinValue;

            foreach (var r in _area.Rooms.Values) {
                if (r.rx < minX) minX = r.rx;
                if (r.ry < minY) minY = r.ry;
                if (r.rx > maxX) maxX = r.rx;
                if (r.ry > maxY) maxY = r.ry;
            }

            AreaHeight = maxY - minY;
            AreaWidth = maxX - minX;
            AreaOffsetX = minX;
            AreaOffsetY = minY;
        }

        public void GetMapSize(out int h, out int w, out int xo, out int yo) {
            int minX, minY, maxX, maxY;

            minX = Int32.MaxValue;
            minY = Int32.MaxValue;
            maxX = Int32.MinValue;
            maxY = Int32.MinValue;

            foreach (var r in _area.Rooms.Values) {
                if (r.x < minX) minX = r.x;
                if (r.y < minY) minY = r.y;
                if (r.x > maxX) maxX = r.x;
                if (r.y > maxY) maxY = r.y;
            }

            // SHADOW_OFFSET is for extra space for shadows
            // EXIT_DISTANCE is for when exits turn around at the edge (moved rooms)
            // 10 is little padding
            h = maxY - minY + (SHADOW_OFFSET + (EXIT_DISTANCE * 2) + RoomHeight + 10);
            w = maxX - minX + (SHADOW_OFFSET + (EXIT_DISTANCE * 2) + RoomWidth + 10);
            AreaHeight = maxY - minY;
            AreaWidth = maxX - minX;

            xo = minX - BORDER_OFFSET;
            yo = minY - BORDER_OFFSET;
            AreaOffsetX = minX;
            AreaOffsetY = minY;
        }

        public bool SaveAreaBitmap(string f, int scale, System.Drawing.Imaging.ImageFormat format, Common.RenderOptions opt) {
            int h, w, xo, yo; // height, width
            Graphics g;
            Bitmap b;

            GetMapSize(out h, out w, out xo, out yo);
            h = h * scale / 100;
            w = w * scale / 100;
            opt.scrollOffsetX = 0 - xo;
            opt.scrollOffsetY = 0 - yo;

            if (h < 1 || w < 1) {
                MessageBox.Show("The resulting image is too small to save.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            b = new Bitmap(w, h);
            g = Graphics.FromImage(b);
            g.SmoothingMode = SmoothingMode.AntiAlias;

            g.ScaleTransform((float)(1 * scale) / 100, (float)(1 * scale) / 100);

            // TODO: render using off screen bitmap (add param to render for screen to get hdc from, then continue as normal?, different backbuffer actually...)
            //Render(ref g, ref g, ref b, opt);

            try {
                b.Save(f, format);
            } catch {
                MessageBox.Show("An error occured while trying to save the image.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                g.Dispose();
                b.Dispose();
                return false;
            }

            g.Dispose();
            b.Dispose();

            return true;
        }

        private void StartCustomAction(CustomActions action) {
            editMode = action;
            editStep = 1;
            editObjects.Clear();
            UpdateStatus();
        }

        public void ValidateRoom(int id) {
            if (_area.Rooms.ContainsKey(id)) {
                _area.Rooms[id].Validate(EmptyProperties);
            }
        }

        private void CreateExitMenu() {
            MenuItem mi;

            // BUG: not all exits show up

            contextExits.MenuItems.Clear();

            foreach (Exit ex in _area.Exits) {
                if (ex.idFrom == currentRoom.id) {
                    mi = new MenuItem(ex.dirFrom, new System.EventHandler(HandleExitMenu));
                    contextExits.MenuItems.Add(mi);
                } else if (ex.idTo == currentRoom.id) {
                    mi = new MenuItem(ex.dirTo, new System.EventHandler(HandleExitMenu));
                    contextExits.MenuItems.Add(mi);
                }
            }

            contextExits.Show(this, new Point(currentRoom.rx + optStandard.scrollOffsetX + RoomWidth, currentRoom.ry + optStandard.scrollOffsetY + RoomHeight));
        }

        private void HandleExitMenu(object sender, System.EventArgs e) {
            string direction;
            Room r;

            direction = ((MenuItem)sender).Text;

            r = currentRoom;

            foreach (Exit ex in _area.Exits) {
                if (ex.idFrom == currentRoom.id && ex.dirFrom == direction) {
                    r = GetRoom(ex.idTo);
                } else if (ex.idTo == currentRoom.id && ex.dirTo == direction) {
                    r = GetRoom(ex.idFrom);
                }
            }

            if (r != null) {
                MakeCurrent(r);
                Redraw(true);
            }
        }

        private void timerAutoSave_Tick(object sender, System.EventArgs e) {
            SaveArea(false, true);
            statusMain.Text = "Auto saved.";
        }

        public void UpdateForm() {
            timerAutoSave.Enabled = AutoSave;
            if (Editor != null) {
                Editor.Opacity = EditorOpacity;
            }
            // TODO: update brushes after options change
			
            UpdateMenus();
            UpdateStatus();
            SetupDefaultLayouts();
            Redraw(true);
        }

        private void formMain_Closed(object sender, System.EventArgs e) {
        }

        public void SelectAll() {
            ClearSelected();

            foreach (var room in _area.Rooms.Values) {
                SelectedRooms.Add(room);
            }
        }

        public void UpdateOverview() {
            if (Overview == null) return;

            float widthRatio = (float)(Convert.ToDouble(AreaWidth) / Convert.ToDouble(ViewportWidth));
            float heightRatio = (float)(Convert.ToDouble(AreaHeight) / Convert.ToDouble(ViewportHeight));

            if (widthRatio < 1) widthRatio = 1;
            if (heightRatio < 1) heightRatio = 1;

            Overview.UpdateDisplay(widthRatio, heightRatio, optStandard.scrollOffsetX, optStandard.scrollOffsetY, AreaOffsetX, AreaOffsetY);
        }

        private void formMain_ResizeEnd(object sender, EventArgs e) {
			ResetCanvasSettings();
            Redraw(true);
        }

        public void UpdateOffsets(int x, int y) {
            optStandard.scrollOffsetX = x;
            optStandard.scrollOffsetY = y;
            Redraw(true);
        }

        private void panelViewport_MouseDown(object sender, MouseEventArgs e) {
            HandleMouseDown(e.X, e.Y, e.Button);
        }

        private void panelViewport_MouseUp(object sender, MouseEventArgs e) {
            HandleMouseUp(e.X, e.Y, e.Button);
        }

        private void panelViewport_Paint(object sender, PaintEventArgs e) {
            Redraw(false);
        }

        private void panelViewport_MouseMove(object sender, MouseEventArgs e) {
            HandleMouseMove(e.X, e.Y);
        }

        public void GoToRoom(string sid) {
            int id;
            try {
                id = Convert.ToInt32(sid);
            } catch {
                MessageBox.Show("The number you entered appears to be invalid.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Room r = GetRoom(id);
            if (r != null) {
                MakeCurrent(r);
                Redraw(FocusOn(currentRoom, true));
            }
        }

        private void formMain_ResizeBegin(object sender, EventArgs e) {
        }

        private void MakeCurrent(Room r) {
            currentRoom = r;
        }

        private void UpdateRoomCoordinates() {
            foreach (var room in _area.Rooms.Values) {
                room.UpdateCoordinates(RoomDistance);
            }
        }

        private void UpdateExitCoordinates() {
            foreach (Exit e in _area.Exits) {
                SetExitCoordinates(e);
            }
        }

        private void SetZoomLevel(int level) {
            ZoomLevel = level;

            if (ZoomLevel < 1) { ZoomLevel = 1; return; }
            if (ZoomLevel > 4) { ZoomLevel = 4; return; }

            switch (ZoomLevel) {
                case 1:
                    RoomWidth = RoomHeight = 5;
                    RoomDistance = 10;
                    break;
                case 2:
                    RoomWidth = RoomHeight = 10;
                    RoomDistance = 20;
                    break;
                default:
                case 3:
                    RoomWidth = RoomHeight = 20;
                    RoomDistance = 40;
                    break;
                case 4:
                    RoomWidth = RoomHeight = 40;
                    RoomDistance = 80;
                    break;
            }
            UpdateRoomCoordinates();
            UpdateExitCoordinates();
            CenterOnCurrent();
            Redraw(true);
        }

        private void UpdateModifiers() {
            if (GetKeyState(VirtualKeyStates.VK_SHIFT) < 0) ShiftPressed = true; else ShiftPressed = false;
            if (GetKeyState(VirtualKeyStates.VK_CONTROL) < 0) ControlPressed = true; else ControlPressed = false;
        }

        private void formMain_FormClosing(object sender, FormClosingEventArgs e) {
            SaveSettings();
        }

        private void menuFileClose_Click(object sender, EventArgs e) {
            ClearArea(false);
            UpdateStatus();
            UpdateMenus();
            Saved = true;
            Redraw(true);
        }

        private void menuFileExit_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void menuFileNew_Click(object sender, EventArgs e) {
            NewArea();
        }

        private void menuFileOpen_Click(object sender, EventArgs e) {
            LoadArea(false);
        }

        private void menuFileSave_Click(object sender, EventArgs e) {
            SaveArea(false, false);
        }

        private void menuFileSaveAs_Click(object sender, EventArgs e) {
            SaveArea(true, false);
        }

        private void menuSelectionClear_Click(object sender, EventArgs e) {
            ClearSelected();
            Redraw(false);
        }

        private void menuSelectionDelete_Click(object sender, EventArgs e) {
            HandleDelete();
        }

        private void menuSelectionInvert_Click(object sender, EventArgs e) {
            InvertSelection();
        }

        private void menuSelectionSelectAll_Click(object sender, EventArgs e) {
            SelectAll();
            Redraw(false);
        }

        private void menuToolsCustomExit_Click(object sender, EventArgs e) {
            StartCustomAction(CustomActions.CustomExit);
        }

        private void menuToolsOptions_Click(object sender, EventArgs e) {
            formOptions f;

            f = new formOptions(this);
            f.ShowDialog();
        }

        private void menuToolsRoomCopy_Click(object sender, EventArgs e) {
            StartCustomAction(CustomActions.CopyRoom);
        }

        private void menuToolsRoomEditor_Click(object sender, EventArgs e) {
            LoadEditor();
            LoadCurrentRoom(true);
        }

        private void menuToolsRoomSelect_Click(object sender, EventArgs e) {
            formSelect f = new formSelect(this, _area);

            f.ShowDialog();
        }

        private void menuToolsRoomSetter_Click(object sender, EventArgs e) {
            formSetter f;

            f = new formSetter(this, _area);
            f.ShowDialog();
        }

        private void menuToolsSetTemplate_Click(object sender, EventArgs e) {
            StartCustomAction(CustomActions.SetTemplate);
        }

        private void menuToolsWordReplace_Click(object sender, EventArgs e) {
            formReplace f;

            if (Editor != null) Editor.Close();

            f = new formReplace(this, _area);
            f.ShowDialog();
        }

        private void menuViewRoomsBelow_Click(object sender, EventArgs e) {
            ToolStripMenuItem mi = ((ToolStripMenuItem)sender);
            DrawRoomsBelow = mi.Checked;
            optStandard.DrawAboveBelow = mi.Checked;
            Redraw(true);
        }

        private void menuViewZones_Click(object sender, EventArgs e) {
            ToolStripMenuItem mi = ((ToolStripMenuItem)sender);
            DrawZones = mi.Checked;
            optStandard.DrawZones = mi.Checked;
            Redraw(true);
        }

        private void menuViewGrid_Click(object sender, EventArgs e) {
            ToolStripMenuItem mi = ((ToolStripMenuItem)sender);
            DrawGrid = mi.Checked;
            optStandard.DrawGrid = mi.Checked;
            Redraw(true);
        }

        private void menuHelpAbout_Click(object sender, EventArgs e) {
            formAbout f;

            f = new formAbout();
            f.ShowDialog();
        }

        private void toolEdit_Click(object sender, EventArgs e) {
            if (currentRoom == null) return;

            LoadEditor();
            LoadCurrentRoom(true);
        }

        private void toolNew_Click(object sender, EventArgs e) {
            NewArea();
        }

        private void toolOpen_Click(object sender, EventArgs e) {
            LoadArea(false);
        }

        private void toolSave_Click(object sender, EventArgs e) {
            SaveArea(false, false);
        }

        private void toolZoom_ButtonClick(object sender, EventArgs e) {
            UpdateModifiers();
            if (_ShiftPressed) SetZoomLevel(ZoomLevel - 1); else SetZoomLevel(ZoomLevel + 1);
        }

        private void toolZoom_DropDownOpening(object sender, EventArgs e) {
            foreach (ToolStripMenuItem mi in toolZoom.DropDown.Items) {
                mi.Checked = false;
            }

            switch (ZoomLevel) {
                case 1: toolZoom25.Checked = true; break;
                case 2: toolZoom50.Checked = true; break;
                default:
                case 3: toolZoom100.Checked = true; break;
                case 4: toolZoom200.Checked = true; break;
            }
        }

        private void toolZoom50_Click(object sender, EventArgs e) {
            SetZoomLevel(2);
        }

        private void toolZoom25_Click(object sender, EventArgs e) {
            SetZoomLevel(1);
        }

        private void toolZoom100_Click(object sender, EventArgs e) {
            SetZoomLevel(3);
        }

        private void toolZoom200_Click(object sender, EventArgs e) {
            SetZoomLevel(4);
        }

        private void menuToolsControlWindow_Click(object sender, EventArgs e) {
            if (Controller != null) {
                Controller.Focus();
                return;
            }

            Controller = new formControl(this);
            Controller.Show();
        }

        public void CenterOnCurrent() {
            FocusOn(currentRoom, true);
            Redraw(true);
        }

        private void menuView_DropDownOpening(object sender, EventArgs e) {
            menuViewGrid.Checked = DrawGrid;
            menuViewZones.Checked = DrawZones;
            menuViewRoomsBelow.Checked = DrawRoomsBelow;
        }

        private void menuViewGoTo_Click(object sender, EventArgs e) {
            formGoTo form = new formGoTo();

            form.comboID.Items.Clear();
            foreach (var room in _area.Rooms.Values) {
                form.comboID.Items.Add(room.id);
            }
            
            if (form.ShowDialog() == DialogResult.Cancel) return;

            var r = GetRoom(Convert.ToInt32(form.comboID.Text));
            if (r == null) {
                MessageBox.Show("No room exists with the ID '" + form.comboID.Text + "'", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            } else {
                MakeCurrent(r);
                FocusOn(currentRoom, true);
                Redraw(true);
            }
        }

        private void menuToolsShowExits_Click(object sender, EventArgs e) {
            CreateExitMenu();
        }

        private void toolObjects_Click(object sender, EventArgs e) {
            ShowObjects();
        }

        public void ShowObjects() {
            if (ObjectsForm != null) {
                ObjectsForm.Show();
                return;
            }

            ObjectsForm = new formObjects(this, _area);
            ObjectsForm.Show();
        }

        public void LoadObject(GenesisObject go) {
            if (Editor == null) return;

            Editor.LoadGenesisObject(ref go);
            Editor.Focus();
        }

        public GenesisObject CreateObject() {
            if (_area.Groups.Count == 0) return null;
            GenesisObject go = new GenesisObject();
            //go.id = newObjectId();
            //objects.Add(go.id, go);
            return go;
        }

        private string newObjectId() {
            int c = 1;
            string name = "";

            do {
                name = "unnamed" + c.ToString();
                c++;
            } while (_area.Objects.ContainsKey(name));

            return name;
        }

        public void AddObject(GenesisObject go) {
            _area.Objects.Add(go.id, go);
        }

        public bool ObjectIdTaken(string name, BaseObject exclude) {
            foreach (var go in _area.Objects.Values) {
                if (go == exclude) continue;
                if (go.id == name) return true;
            }
            return false;
        }

        private void menuAreaSetup_Click(object sender, EventArgs e) {
            formSetup f;

            f = new formSetup(this, _area);

            f.ShowDialog();
        }

        private void menuAreaCompileRooms_Click(object sender, EventArgs e) {
            CompileRooms();
        }

        private void menuAreaInformation_Click(object sender, EventArgs e) {
            formInformation f;

            f = new formInformation(this, _area);
            f.ShowDialog();
        }

        private void menuAreaObjects_Click(object sender, EventArgs e) {
            ShowObjects();
        }

        private void menuAreaGroups_Click(object sender, EventArgs e) {
            formGroups f = new formGroups(this, _area);

            f.ShowDialog();
        }

        public void AddGroup(string Key, Group gg) {
            _area.Groups.Add(Key, gg);
        }

        public void AddGroup(Group g) {
            _area.Groups.Add(g.Name, g);
        }

        public Group NewGroup() {
            Group gg = new Group();

            return gg;
        }

        public bool GroupNameTaken(string name) {
            return _area.Groups.ContainsKey(name);
        }

        public string DeleteGroup(string name) {
            if (!_area.Groups.ContainsKey(name)) return "";

            _area.Groups.Remove(name);

            return name;
        }

        private void toolGroups_Click(object sender, EventArgs e) {
            formGroups f = new formGroups(this, _area);

            f.ShowDialog();
        }

        public Group FindGroupByName(string name) {
            if (_area.Groups.ContainsKey(name)) {
                return _area.Groups[name];
            }
            
            return null;
        }

        public GenesisObject FindObjectByName(string name) {
            if (_area.Objects.ContainsKey(name)) {
                return _area.Objects[name];
            }
            
            return null;
        }

        private void menuAreaCompileAll_Click(object sender, EventArgs e) {
            CompileRooms();
            CompileObjects();
        }

        public void RemoveObjectFromCurrentRoom(string ob) {
            if (currentRoom == null) return;

            foreach (GenesisObject o in currentRoom.objects) {
                if (o.id == ob) {
                    currentRoom.objects.Remove(o);
                    if (Editor != null) Editor.LoadObjectList();
                    return;
                }
            }
        }

        private void UpdateEditorExits() {
            if (Editor == null) return;

            Editor.LoadExits();
        }

        private void CompileObjects() {
            if (!Saved) {
                Common.Warn("You must save your area before compiling.");
                return;
            }

            foreach (var group in _area.Groups.Values) {
                if (!CheckFolderExists(group.Folder)) {
                    Common.Warn("Unable to create target folder for group " + group.Name + ".");
                    return;
                }

            }

            var progressForm = new formProgress();
            progressForm.StartPosition = FormStartPosition.CenterParent;
            progressForm.Text = "Compile Progress";

            progressForm.labelDescription.Text = "Compiling objects...";
            progressForm.barProgress.Minimum = 0;
            progressForm.barProgress.Value = 0;
            progressForm.barProgress.Maximum = _area.Objects.Count;
            progressForm.Show();

            Application.DoEvents(); // ensure form is displayed properly
            try {
                var compiler = new AreaCompiler(_area, ExcludeOld, options, () => { progressForm.barProgress.Value += 1; });
                var compileResult = compiler.CompileObjects();

#if DEBUG
                MessageBox.Show("Compiled Objects: " + compileResult, Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
            } finally {
                progressForm.Close();
            }
        }

        private void toolCopyRoom_Click(object sender, EventArgs e) {
            StartCustomAction(CustomActions.CopyRoom);
        }

        private void toolCompile_Click(object sender, EventArgs e) {
            CompileRooms();
        }

        private void menuToolsLayoutEditor_Click(object sender, EventArgs e) {
            formLayouts form = new formLayouts();

			if (Editor != null) {
				Editor.Close();
				Editor = null;
			}

            form.ShowDialog();

			Genesis.Layouts.ClearCache();
			SetupDefaultLayouts();
        }

        private void menuFileSaveAsTemplate_Click(object sender, EventArgs e) {
            string fileName;
            XmlTextWriter x;

            formInput input = new formInput("Please enter a template name", "");

            if (input.ShowDialog() == DialogResult.Cancel) return;

            fileName = Common.GetTemplatesPath() + input.Value + Common.EXT_AREATEMPLATE;

            if (File.Exists(fileName)) {
                if (MessageBox.Show("This template already exists, are you sure you want to overwrite it?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    return;
                }
            }

            x = new System.Xml.XmlTextWriter(fileName, System.Text.Encoding.Default);
            x.Indentation = 4;
            x.IndentChar = ' ';

            x.WriteStartElement("template");
            WriteConstantsToXml(x);
            WriteGroupsToXml(x);
            x.WriteEndElement();

            x.Flush();
            x.Close();
        }

		private void menuViewRoomIDs_Click(object sender, EventArgs e) {
			ToolStripMenuItem mi = ((ToolStripMenuItem)sender);
			DrawRoomIDs = mi.Checked;
			optStandard.DrawIDs = mi.Checked;
			Redraw(true);
		}

        private void menuAreaMoveToUnfinishedRoom_Click(object sender, EventArgs e) {
            foreach (var r in _area.Rooms.Values) {
                if (r.empty) {
                    MakeCurrent(r);
                    CenterOnCurrent();
                    break;
                }
            }
        }
    }
}
