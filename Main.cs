using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Genesis
{
    /// <summary>
	/// Summary description for classMain.
	/// </summary>
	public class Common
	{
		public const string APP_NAME = "Genesis";
		public const string APP_VERSION = "0.99";
		public const string EXT_MAP = ".gmf";
		public const string EXT_TEMPLATE = ".gtf";
		public const string EXT_CONFIG = ".xml";
        public const string EXT_LAYOUT = ".glf";
        public const string EXT_AREATEMPLATE = ".gatf";

		public class RenderOptions {
			public bool DrawGrid;
			public bool DrawRooms;
			public bool DrawExits;
			public bool DrawShadows;
			public bool DrawIDs;
			public bool DrawInvExits;
			public bool DrawExitSquares;
			public bool DrawZones;
			public bool IgnoreScreenSize;
			public bool ShowSelection;
			public bool ShowEmpty;
			public bool DrawAboveBelow;
			public bool DrawExcluded;
			public bool DrawNotesAsLabel;
			public bool FlattenZones;
			public int scrollOffsetX;
			public int scrollOffsetY;

			public RenderOptions() {
			}
		}

		public Common() {
			// constructor code
		}

        public static string GetTemplatesPath() { return Application.StartupPath + "\\Templates\\"; }
        public static string GetLayoutsPath() { return Application.StartupPath + "\\Layouts\\"; }

        static Font EditorFont = null;
        static public Font GetEditorFont() {
            if (EditorFont == null) {
                EditorFont = new Font("Courier New", 9);
            }
            return EditorFont;
        }

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.Run(new formMain());
		}

		public static string ReverseDirection(string s) {
			switch (s) {
				case "north":
					return "south";
				case "northeast":
					return "southwest";
				case "east":
					return "west";
				case "southeast":
					return "northwest";
				case "south":
					return "north";
				case "southwest":
					return "northeast";
				case "west":
					return "east";
				case "northwest":
					return "southeast";
				case "up":
					return "down";
				case "down":
					return "up";
			}

			return "";
		}

		public static string GetMultiString(Hashtable h) {
			string p = "";

			foreach (DictionaryEntry DE in h) {
				if (((string)DE.Key).Length == 0) continue;
				if (p.Length != 0) p += "|";
				p += (String)DE.Key + "^" + (String)DE.Value;
			}

			return p;
		}

		public static Hashtable GetMultiHash(string s) {
			string[] a, ai;
            Hashtable h = new Hashtable();

			a = s.Split('|');

			foreach (string si in a) {
				ai = si.Split('^');
				if (si.Length == 0) continue;
				if (ai.GetUpperBound(0) == 1) h.Add(ai[0], ai[1]); else h.Add(ai[0], "");
			}

			return h;
		}

		public static bool IsEmptyString(string s) {
			if (s.Length == 0) return true; else return false;
		}

		public static void Error(string m) {
			// TODO: possibly write error log here
			MessageBox.Show(m, Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public static double Distance(int x1, int y1, int x2, int y2) {
			int d1 = x1 - x2;
			int d2 = y1 - y2;

			return System.Math.Sqrt(d1*d1+d2*d2);
		}

        public static String Implode(string c, List<String> list) {
            System.Text.StringBuilder ret = new System.Text.StringBuilder();
            bool first = true;
            foreach (String s in list) {
                if (first) {
                    first = false;
                } else {
                    ret.Append(c);
                }
                ret.Append(s);
            }

            return ret.ToString();
        }

        public static void SetComboSelected(ComboBox combo, string value) {
            for (int i = 0; i < combo.Items.Count; i++) {
                if (combo.Items[i].Equals(value)) {
                    combo.SelectedIndex = i;
                }
            }
        }

        public static void LoadLayouts(ComboBox combo, formMain parent) {
            combo.SuspendLayout();
            combo.Items.Clear();
            foreach (string layout in Layouts.Names) {
                combo.Items.Add(layout);
            }
            combo.ResumeLayout();
        }

        public static void LoadTemplates(ComboBox combo, formMain parent, string first) {
            combo.SuspendLayout();
            combo.Items.Clear();
            if (first != null) {
                combo.Items.Add(first);
            }
            foreach (string template in Templates.Names) {
                combo.Items.Add(template);
            }
            combo.ResumeLayout();
        }

        public static void Warn(string message) {
            MessageBox.Show(message, APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
