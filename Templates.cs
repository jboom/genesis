using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Genesis {
    class Templates {
        private static List<String> names;

        public static List<String> Names {
            get { Reload();  return Templates.names; }
            set { Templates.names = value; }
        }

        static Templates() {
        }

        public static void Reload() {
            DirectoryInfo dirList = new DirectoryInfo(Common.GetTemplatesPath());

            names = new List<string>();
            foreach (FileInfo file in dirList.GetFiles()) {
                if (file.Extension == Common.EXT_TEMPLATE) {
                    names.Add(file.Name.Substring(0, file.Name.Length - Common.EXT_TEMPLATE.Length));
                }
            }
        }
    }
}
