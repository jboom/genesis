using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Genesis
{
	/// <summary>
	/// Summary description for formSelect.
	/// </summary>
	public class formSelect : System.Windows.Forms.Form
	{
		private readonly formMain parent;
	    private readonly Area _area;
		private TextBox Current;

		private enum VirtualKeyStates : int	{
			VK_SHIFT = 0x10
		}

		[DllImport("user32.dll")]
		static extern short GetKeyState(VirtualKeyStates nVirtKey);

		private System.Windows.Forms.Button buttonOK;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textInclude;
		private System.Windows.Forms.ContextMenu menuCondition;
		private System.Windows.Forms.MenuItem menuConditionExit;
		private System.Windows.Forms.MenuItem menuConditionProperty;
		private System.Windows.Forms.MenuItem menuConditionAny;
		private System.Windows.Forms.Button buttonCondition1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formSelect(formMain p, Area area)
		{
			InitializeComponent();

			parent = p;
		    _area = area;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonOK = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textInclude = new System.Windows.Forms.TextBox();
			this.menuCondition = new System.Windows.Forms.ContextMenu();
			this.menuConditionExit = new System.Windows.Forms.MenuItem();
			this.menuConditionProperty = new System.Windows.Forms.MenuItem();
			this.menuConditionAny = new System.Windows.Forms.MenuItem();
			this.buttonCondition1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonOK
			// 
			this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonOK.Location = new System.Drawing.Point(224, 112);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new System.Drawing.Size(80, 24);
			this.buttonOK.TabIndex = 0;
			this.buttonOK.Text = "OK";
			this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonCancel.Location = new System.Drawing.Point(136, 112);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(80, 24);
			this.buttonCancel.TabIndex = 1;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Include";
			// 
			// textInclude
			// 
			this.textInclude.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textInclude.HideSelection = false;
			this.textInclude.Location = new System.Drawing.Point(8, 32);
			this.textInclude.Multiline = true;
			this.textInclude.Name = "textInclude";
			this.textInclude.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textInclude.Size = new System.Drawing.Size(296, 72);
			this.textInclude.TabIndex = 3;
			this.textInclude.Text = "";
			// 
			// menuCondition
			// 
			this.menuCondition.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.menuConditionExit,
																						  this.menuConditionProperty,
																						  this.menuConditionAny});
			// 
			// menuConditionExit
			// 
			this.menuConditionExit.Index = 0;
			this.menuConditionExit.Text = "&Exit";
			this.menuConditionExit.Click += new System.EventHandler(this.menuConditionExit_Click);
			// 
			// menuConditionProperty
			// 
			this.menuConditionProperty.Index = 1;
			this.menuConditionProperty.Text = "&Property";
			this.menuConditionProperty.Click += new System.EventHandler(this.menuConditionProperty_Click);
			// 
			// menuConditionAny
			// 
			this.menuConditionAny.Index = 2;
			this.menuConditionAny.Text = "&Any";
			this.menuConditionAny.Click += new System.EventHandler(this.menuConditionAny_Click);
			// 
			// buttonCondition1
			// 
			this.buttonCondition1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonCondition1.Location = new System.Drawing.Point(240, 8);
			this.buttonCondition1.Name = "buttonCondition1";
			this.buttonCondition1.Size = new System.Drawing.Size(64, 20);
			this.buttonCondition1.TabIndex = 6;
			this.buttonCondition1.Text = "Condition";
			this.buttonCondition1.Click += new System.EventHandler(this.buttonCondition1_Click);
			// 
			// formSelect
			// 
			this.AcceptButton = this.buttonOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(312, 144);
			this.Controls.Add(this.buttonCondition1);
			this.Controls.Add(this.textInclude);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.Name = "formSelect";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Room Select";
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonCancel_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private bool MatchesConditions(Room r, string conditionlist) {
			string k, v;
			System.Text.RegularExpressions.Regex rx;
			System.Text.RegularExpressions.Match m;
			Array conditions, list;
			Hashtable ht;
			bool matches = false, exclude = false;

			rx = new System.Text.RegularExpressions.Regex(@"([!]*)(\w*)\[([\w:]*)\]");

			conditions = conditionlist.Split(';');

			foreach (string condition in conditions) {
				m = rx.Match(condition);

				if (m.Success) {
					if (m.Groups.Count == 3) {
						k = m.Groups[1].Value; // condition name
						v = m.Groups[2].Value; // condition value
					} else if (m.Groups.Count == 4) {
						if (m.Groups[1].Value == "!") exclude = true; else exclude = false;
						k = m.Groups[2].Value; // condition name
						v = m.Groups[3].Value; // condition value
					} else {
						return false;
					}

					matches = exclude;

					switch (k) {
						case "has_exit":
							list = v.Split(':');
							if (list.Length == 2) {
								foreach (Exit ex in _area.Exits) {
									if (ex.idFrom == r.id && ex.dirFrom == (string)list.GetValue(0)) {	
										if ((string)list.GetValue(1) == "invis") {
											if (ex.invFrom) {
												matches = true;
												break;
											} else matches = false;
										}
									} else if (ex.idTo == r.id && ex.dirTo == (string)list.GetValue(0)) {
										if ((string)list.GetValue(1) == "invis") {
											if (ex.invTo) {
												matches = true;
												break;
											} else matches = false;
										}
									}
								}
							} else {
								foreach (Exit ex in _area.Exits) {
									if ((ex.idFrom == r.id && ex.dirFrom == v) || (ex.idTo == r.id && ex.dirTo == v)) {	
										matches = true;
										break;
									} else matches = false;
								}
							}
							break;
						case "has_property":
							// find the property
							list = v.Split(':');
							if (list.Length == 2) {
								ht = Common.GetMultiHash(r.GetProperty("custom"));
                                if (ht.ContainsKey((String)list.GetValue(0)) && (string)(ht[(string)list.GetValue(0)]) == (string)list.GetValue(1)) {
									matches = true;
								} else matches = false;
							}
							break;
						case "any":
							matches = true;
							break;
					}
				}

				if (matches == exclude) {
					return false;
				}
			}

			return true;
		}

		private void buttonOK_Click(object sender, System.EventArgs e) {
            List<Room> NewSelected = new List<Room>();

			if (parent.SelectedRooms.Count > 0) {
				foreach (Room r in parent.SelectedRooms) {
					if (MatchesConditions(r, textInclude.Text)) NewSelected.Add(r);
				}
			} else {
				foreach (var r in _area.Rooms.Values) {
					if (MatchesConditions(r, textInclude.Text)) NewSelected.Add(r);
				}
			}

			parent.SelectedRooms = NewSelected;
			parent.Redraw(true);

			this.Close();
		}

		private void menuConditionExit_Click(object sender, System.EventArgs e) {
			if (Current.Text != "") Current.Text += ";";
			if (GetKeyState(VirtualKeyStates.VK_SHIFT) < 0) Current.Text += "!";
			Current.Text += "has_exit[]";
			Current.SelectionStart = Current.Text.Length - 1;
			Current.Focus();
		}

		private void menuConditionProperty_Click(object sender, System.EventArgs e) {
			if (Current.Text != "") Current.Text += ";";
			if (GetKeyState(VirtualKeyStates.VK_SHIFT) < 0) Current.Text += "!";
			Current.Text += "has_property[]";
			Current.SelectionStart = Current.Text.Length - 1;
			Current.Focus();
		}

		private void menuConditionAny_Click(object sender, System.EventArgs e) {
			if (Current.Text != "") Current.Text += ";";
			if (GetKeyState(VirtualKeyStates.VK_SHIFT) < 0) Current.Text += "!";
			Current.Text += "any[]";
			Current.SelectionStart = Current.Text.Length - 1;
			Current.Focus();
		}

		private void buttonCondition1_Click(object sender, System.EventArgs e) {
			Current = textInclude;
			menuCondition.Show(buttonCondition1, new Point(0, buttonCondition1.Height));
		}
	}
}
