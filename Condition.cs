using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public class Condition {
        public enum ConditionType {
            Any,
            HasProperty,
            HasExit,
            Random
        }
        public bool exclude = false;
        public ConditionType type;
        public String key;
        public String value;

        public override string ToString() {
            String str = "";
            switch (type) {
                case ConditionType.Any:
                    str += "Any";
                    break;
                case ConditionType.HasProperty:
                    str += (exclude ? "Without" : "With") + " Property " + key + (value.Length > 0 ? " = " + value : "");
                    break;
                case ConditionType.HasExit:
                    str += (exclude ? "Without" : "With") + " Exit " + key + (value.Length > 0 ? " = " + value : "");
                    break;
                case ConditionType.Random:
                    str += "Random " + value + "%";
                    break;
            }

            return str;
        }

        public TreeNode ToTreeNode() {
            TreeNode node = new TreeNode();

            node.Text = ToString();
            node.Tag = this;

            return node;
        }
    }
}
