using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Genesis
{
	/// <summary>
	/// Summary description for formCopy.
	/// </summary>
	public class formCopy : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textValue;
		private System.Windows.Forms.CheckedListBox listProperties;

		public Room original;
		private formMain parent;
		private System.Windows.Forms.Button buttonAll;
		private System.Windows.Forms.Button buttonNone;
		private System.Windows.Forms.ImageList imageIcons;
		private Dictionary<String,String> t;

		public formCopy(formMain p)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;

			t = new Dictionary<String,String>();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formCopy));
            this.buttonCopy = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listProperties = new System.Windows.Forms.CheckedListBox();
            this.buttonAll = new System.Windows.Forms.Button();
            this.imageIcons = new System.Windows.Forms.ImageList(this.components);
            this.buttonNone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCopy
            // 
            this.buttonCopy.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCopy.Location = new System.Drawing.Point(216, 232);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(80, 24);
            this.buttonCopy.TabIndex = 1;
            this.buttonCopy.Text = "&Copy";
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCancel.Location = new System.Drawing.Point(128, 232);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 24);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "C&ancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textValue
            // 
            this.textValue.Location = new System.Drawing.Point(128, 24);
            this.textValue.Multiline = true;
            this.textValue.Name = "textValue";
            this.textValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textValue.Size = new System.Drawing.Size(168, 200);
            this.textValue.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Location = new System.Drawing.Point(128, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Value";
            // 
            // listProperties
            // 
            this.listProperties.Location = new System.Drawing.Point(8, 40);
            this.listProperties.Name = "listProperties";
            this.listProperties.ScrollAlwaysVisible = true;
            this.listProperties.Size = new System.Drawing.Size(112, 214);
            this.listProperties.TabIndex = 5;
            this.listProperties.SelectedIndexChanged += new System.EventHandler(this.listProperties_SelectedIndexChanged);
            // 
            // buttonAll
            // 
            this.buttonAll.ImageIndex = 1;
            this.buttonAll.ImageList = this.imageIcons;
            this.buttonAll.Location = new System.Drawing.Point(8, 8);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(24, 24);
            this.buttonAll.TabIndex = 6;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // imageIcons
            // 
            this.imageIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageIcons.ImageStream")));
            this.imageIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageIcons.Images.SetKeyName(0, "");
            this.imageIcons.Images.SetKeyName(1, "");
            // 
            // buttonNone
            // 
            this.buttonNone.ImageIndex = 0;
            this.buttonNone.ImageList = this.imageIcons;
            this.buttonNone.Location = new System.Drawing.Point(38, 8);
            this.buttonNone.Name = "buttonNone";
            this.buttonNone.Size = new System.Drawing.Size(24, 24);
            this.buttonNone.TabIndex = 7;
            this.buttonNone.Click += new System.EventHandler(this.buttonNone_Click);
            // 
            // formCopy
            // 
            this.AcceptButton = this.buttonCopy;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(306, 264);
            this.Controls.Add(this.buttonNone);
            this.Controls.Add(this.buttonAll);
            this.Controls.Add(this.listProperties);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textValue);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonCopy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formCopy";
            this.ShowInTaskbar = false;
            this.Text = "Room Copy";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void buttonCancel_Click(object sender, System.EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
		}

		private void buttonCopy_Click(object sender, System.EventArgs e) {
			List<String> selected = new List<String>();

			foreach (Room r in parent.SelectedRooms) {
				foreach (string s in listProperties.CheckedItems) {
					r.SetProperty(s, (string)t[s]);
					selected.Add(s);
				}
				parent.ValidateRoom(r.id);
			}

			parent.oldCopy = selected;
			parent.Saved = false;
			parent.Redraw(true);
			this.Close();
		}

		public void LoadRoom(Room r) {
			List<String> selected;

			selected = parent.oldCopy;

			original = r;

			listProperties.BeginUpdate();
			listProperties.Items.Clear();
            foreach (KeyValuePair<String,String> kv in original.properties) {
                listProperties.Items.Add(kv.Key, (selected.Contains(kv.Key) ? CheckState.Checked : CheckState.Unchecked));
                t.Add(kv.Key, kv.Value);
            }
            listProperties.EndUpdate();
			textValue.Enabled = false;
		}

		private void listProperties_SelectedIndexChanged(object sender, System.EventArgs e) {
			if (listProperties.SelectedIndex == -1) return;

            foreach (KeyValuePair<String,String> kv in original.properties) {
                if (kv.Key == (string)listProperties.SelectedItem) {
                    textValue.Text = kv.Value;
                    return;
                }
            }
		}

		private void buttonAll_Click(object sender, System.EventArgs e) {
			for (int i = 0;i < listProperties.Items.Count;i++) {
				listProperties.SetItemCheckState(i, CheckState.Checked);
			}
		}

		private void buttonNone_Click(object sender, System.EventArgs e) {
			for (int i = 0;i < listProperties.Items.Count;i++) {
				listProperties.SetItemCheckState(i, CheckState.Unchecked);
			}
		}
	}
}
