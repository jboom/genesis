using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public class Trigger {
        public List<Condition> conditions;
        public List<ActionList> actionsLists;

        public Trigger() {
            conditions = new List<Condition>();
            actionsLists = new List<ActionList>();
        }

        public TreeNode ToTreeNode() {
            TreeNode node = new TreeNode();
            TreeNode conditionsNode;
            TreeNode actionListsNode;

            node.Text = ToString();
            node.Tag = this;

            conditionsNode = new TreeNode("Conditions");

            foreach (Condition c in conditions) {
                conditionsNode.Nodes.Add(c.ToTreeNode());
            }

            node.Nodes.Add(conditionsNode);

            actionListsNode = new TreeNode("Actions");

            foreach (ActionList al in actionsLists) {
                actionListsNode.Nodes.Add(al.ToTreeNode());
            }

            node.Nodes.Add(actionListsNode);

            node.ExpandAll();

            return node;
        }

        public override string ToString() {
            return "Trigger";
        }
    }
}
