using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Genesis
{
	/// <summary>
	/// Summary description for formSetter.
	/// </summary>
    public enum NodeTypes {
        None = 0,
        Trigger = 1,
        Conditions = 2,
        Condition = 3,
        Actions = 4,
        ActionList = 5,
        Action = 6
    }

    public class formSetter : System.Windows.Forms.Form
	{
		private readonly formMain parent;
        private readonly Area _area;

        private Setter setter;
        private NodeTypes nodeType = NodeTypes.None;

        private Label label3;
        private ToolTip toolTipInfo;
        private Button buttonDeleteItem;
        private Button buttonSaveItem;
        private TextBox textValue;
        private ComboBox comboIndex;
        private TreeView treeTriggers;
        private Label label1;
        private CheckBox checkExclude;
        private ComboBox comboKey;
        private SplitContainer splitContainer1;
        private Button buttonMoveDown;
        private Button buttonMoveUp;
		private System.Random rand;

		private enum VirtualKeyStates : int	{
			VK_SHIFT = 0x10
		}

		[DllImport("user32.dll")]
        static extern short GetKeyState(VirtualKeyStates nVirtKey);
        private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonSaveSetter;
		private System.Windows.Forms.Button buttonLoadSetter;
		private System.Windows.Forms.SaveFileDialog fileSave;
		private System.Windows.Forms.OpenFileDialog fileOpen;
        private System.Windows.Forms.Button buttonNewItem;
        private System.Windows.Forms.Button buttonStart;
        private System.ComponentModel.IContainer components;

		public formSetter(formMain p, Area area)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			parent = p;
		    _area = area;
            rand = new Random();
            setter = new Setter();
			rand = new Random();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formSetter));
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonSaveSetter = new System.Windows.Forms.Button();
            this.buttonLoadSetter = new System.Windows.Forms.Button();
            this.fileSave = new System.Windows.Forms.SaveFileDialog();
            this.fileOpen = new System.Windows.Forms.OpenFileDialog();
            this.buttonNewItem = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.checkExclude = new System.Windows.Forms.CheckBox();
            this.textValue = new System.Windows.Forms.TextBox();
            this.comboIndex = new System.Windows.Forms.ComboBox();
            this.buttonDeleteItem = new System.Windows.Forms.Button();
            this.buttonSaveItem = new System.Windows.Forms.Button();
            this.treeTriggers = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.comboKey = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClose.Location = new System.Drawing.Point(3, 387);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(80, 24);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.Text = "&Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonStart.Location = new System.Drawing.Point(90, 387);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(80, 24);
            this.buttonStart.TabIndex = 7;
            this.buttonStart.Text = "S&tart";
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonSaveSetter
            // 
            this.buttonSaveSetter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveSetter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSaveSetter.Location = new System.Drawing.Point(100, 387);
            this.buttonSaveSetter.Name = "buttonSaveSetter";
            this.buttonSaveSetter.Size = new System.Drawing.Size(80, 24);
            this.buttonSaveSetter.TabIndex = 1;
            this.buttonSaveSetter.Text = "&Save";
            this.buttonSaveSetter.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoadSetter
            // 
            this.buttonLoadSetter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLoadSetter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonLoadSetter.Location = new System.Drawing.Point(12, 387);
            this.buttonLoadSetter.Name = "buttonLoadSetter";
            this.buttonLoadSetter.Size = new System.Drawing.Size(80, 24);
            this.buttonLoadSetter.TabIndex = 0;
            this.buttonLoadSetter.Text = "&Load";
            this.buttonLoadSetter.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonNewItem
            // 
            this.buttonNewItem.Enabled = false;
            this.buttonNewItem.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonNewItem.Location = new System.Drawing.Point(3, 12);
            this.buttonNewItem.Name = "buttonNewItem";
            this.buttonNewItem.Size = new System.Drawing.Size(47, 23);
            this.buttonNewItem.TabIndex = 0;
            this.buttonNewItem.Text = "&New";
            this.buttonNewItem.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Triggers";
            // 
            // toolTipInfo
            // 
            this.toolTipInfo.IsBalloon = true;
            // 
            // checkExclude
            // 
            this.checkExclude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkExclude.AutoSize = true;
            this.checkExclude.Enabled = false;
            this.checkExclude.Location = new System.Drawing.Point(3, 134);
            this.checkExclude.Name = "checkExclude";
            this.checkExclude.Size = new System.Drawing.Size(43, 17);
            this.checkExclude.TabIndex = 21;
            this.checkExclude.Text = "Not";
            this.toolTipInfo.SetToolTip(this.checkExclude, "This will include any room that does NOT match this condition");
            this.checkExclude.UseVisualStyleBackColor = true;
            // 
            // textValue
            // 
            this.textValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textValue.Enabled = false;
            this.textValue.Location = new System.Drawing.Point(3, 108);
            this.textValue.Multiline = true;
            this.textValue.Name = "textValue";
            this.textValue.Size = new System.Drawing.Size(167, 273);
            this.textValue.TabIndex = 5;
            // 
            // comboIndex
            // 
            this.comboIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIndex.Enabled = false;
            this.comboIndex.FormattingEnabled = true;
            this.comboIndex.Items.AddRange(new object[] {
            "None",
            "Has Property",
            "Has Exit",
            "Has Invisible Exit",
            "Random"});
            this.comboIndex.Location = new System.Drawing.Point(3, 41);
            this.comboIndex.Name = "comboIndex";
            this.comboIndex.Size = new System.Drawing.Size(168, 21);
            this.comboIndex.TabIndex = 3;
            this.comboIndex.SelectedIndexChanged += new System.EventHandler(this.comboIndex_SelectedIndexChanged);
            // 
            // buttonDeleteItem
            // 
            this.buttonDeleteItem.Enabled = false;
            this.buttonDeleteItem.Location = new System.Drawing.Point(119, 12);
            this.buttonDeleteItem.Name = "buttonDeleteItem";
            this.buttonDeleteItem.Size = new System.Drawing.Size(47, 23);
            this.buttonDeleteItem.TabIndex = 2;
            this.buttonDeleteItem.Text = "Delete";
            this.buttonDeleteItem.UseVisualStyleBackColor = true;
            this.buttonDeleteItem.Click += new System.EventHandler(this.buttonDeleteItem_Click);
            // 
            // buttonSaveItem
            // 
            this.buttonSaveItem.Enabled = false;
            this.buttonSaveItem.Location = new System.Drawing.Point(61, 12);
            this.buttonSaveItem.Name = "buttonSaveItem";
            this.buttonSaveItem.Size = new System.Drawing.Size(47, 23);
            this.buttonSaveItem.TabIndex = 1;
            this.buttonSaveItem.Text = "S&ave";
            this.buttonSaveItem.UseVisualStyleBackColor = true;
            this.buttonSaveItem.Click += new System.EventHandler(this.buttonSaveItem_Click);
            // 
            // treeTriggers
            // 
            this.treeTriggers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeTriggers.HideSelection = false;
            this.treeTriggers.Location = new System.Drawing.Point(12, 25);
            this.treeTriggers.Name = "treeTriggers";
            this.treeTriggers.ShowRootLines = false;
            this.treeTriggers.Size = new System.Drawing.Size(391, 356);
            this.treeTriggers.TabIndex = 4;
            this.treeTriggers.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeTriggers_AfterSelect);
            this.treeTriggers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treeTriggers_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Value";
            // 
            // comboKey
            // 
            this.comboKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboKey.Enabled = false;
            this.comboKey.FormattingEnabled = true;
            this.comboKey.Location = new System.Drawing.Point(3, 68);
            this.comboKey.Name = "comboKey";
            this.comboKey.Size = new System.Drawing.Size(167, 21);
            this.comboKey.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.buttonLoadSetter);
            this.splitContainer1.Panel1.Controls.Add(this.treeTriggers);
            this.splitContainer1.Panel1.Controls.Add(this.buttonSaveSetter);
            this.splitContainer1.Panel1.Controls.Add(this.buttonMoveDown);
            this.splitContainer1.Panel1.Controls.Add(this.buttonMoveUp);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.buttonNewItem);
            this.splitContainer1.Panel2.Controls.Add(this.comboKey);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.textValue);
            this.splitContainer1.Panel2.Controls.Add(this.checkExclude);
            this.splitContainer1.Panel2.Controls.Add(this.comboIndex);
            this.splitContainer1.Panel2.Controls.Add(this.buttonClose);
            this.splitContainer1.Panel2.Controls.Add(this.buttonDeleteItem);
            this.splitContainer1.Panel2.Controls.Add(this.buttonStart);
            this.splitContainer1.Panel2.Controls.Add(this.buttonSaveItem);
            this.splitContainer1.Size = new System.Drawing.Size(592, 423);
            this.splitContainer1.SplitterDistance = 406;
            this.splitContainer1.TabIndex = 22;
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveDown.Location = new System.Drawing.Point(323, 387);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(80, 24);
            this.buttonMoveDown.TabIndex = 3;
            this.buttonMoveDown.Text = "Move Down";
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveUp.Location = new System.Drawing.Point(237, 387);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(80, 24);
            this.buttonMoveUp.TabIndex = 2;
            this.buttonMoveUp.Text = "Move Up";
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // formSetter
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(592, 423);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(380, 300);
            this.Name = "formSetter";
            this.ShowInTaskbar = false;
            this.Text = "Room Setter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formSetter_FormClosing);
            this.Load += new System.EventHandler(this.formSetter_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void buttonClose_Click(object sender, System.EventArgs e) {
			this.Close();
		}

        private string AddText(string d, string s) {
			if (d.Length == 0) {
				d = s;
			} else {
				d += " " + s;
			}

			return d;
		}

        private Action StringToAction(String source) {
            Action a = new Action();
            if (source.Length == 0) return null;
            String[] list = source.Split(':');
            if (list.Length == 2) {
                a.key = list[0];
                a.value = list[1];
            } else if (list.Length > 0) {
                a.key = list[0];
            }
            return a;
        }

		private void buttonLoad_Click(object sender, System.EventArgs e) {
			/*
			if (setter.Count > 0) {
				if (MessageBox.Show("Are you sure you want to load a setter list?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
					return;
				}
			}
			*/

			fileOpen.AddExtension = true;
			fileOpen.CheckFileExists = true;
			fileOpen.DefaultExt = ".gsl";
			fileOpen.Filter = "Setter list (*.gsl)|*.gsl";
			fileOpen.Multiselect = false;
			fileOpen.ShowReadOnly = false;
			fileOpen.ValidateNames = true;

			if (fileOpen.ShowDialog() == DialogResult.Cancel) {
				return;
			}

            setter = Setter.Load(fileOpen.FileName);
            if (setter == null) return;
            UpdateTree();
		}

		private void buttonSave_Click(object sender, System.EventArgs e) {
			fileSave.AddExtension = true;
			fileSave.DefaultExt = ".gsl";
			fileSave.Filter = "Setter list (*.gsl)|*.gsl";
			fileSave.OverwritePrompt = true;
			fileSave.ValidateNames = true;

			if (fileSave.ShowDialog() == DialogResult.Cancel) {
				return;
			}

            Setter.Save(fileSave.FileName, setter);
		}

		private void WriteXMLValue(System.Xml.XmlTextWriter x, string n, string v) {
			x.WriteStartElement(n);
			x.WriteString(v);
			x.WriteEndElement();
		}

		private ActionList GetRandomItem(Trigger t) {
			return (ActionList)t.actionsLists[rand.Next(0, t.actionsLists.Count)];
		}

		private void buttonNew_Click(object sender, System.EventArgs e) {
            TreeNode node;

            switch (nodeType) {
                case NodeTypes.Conditions:
                case NodeTypes.Condition:
                    UpdateInterface(NodeTypes.Condition);
                    UpdateButtons(NodeTypes.Condition);
                    comboKey.Text = "";
                    break;
                case NodeTypes.Actions:
                    ActionList al = new ActionList();
                    TreeNode current = treeTriggers.SelectedNode;
                    TreeNode parent = GetParentNode(treeTriggers);
                    if (parent == null || current == null) return;
                    Trigger t = (Trigger)parent.Tag;
                    TreeNode newNode = al.ToTreeNode();
                    t.actionsLists.Add(al);
                    current.Nodes.Add(newNode);
                    current.ExpandAll();
                    treeTriggers.SelectedNode = newNode;
                    break;
                case NodeTypes.Action:
                case NodeTypes.ActionList:
                    UpdateInterface(NodeTypes.Action);
                    UpdateButtons(NodeTypes.Action);
                    comboKey.Text = "";
                    break;
                case NodeTypes.Trigger:
                default:
                    Trigger newTrigger = new Trigger();
                    setter.Add(newTrigger);
                    node = newTrigger.ToTreeNode();
                    treeTriggers.Nodes.Add(node);
                    treeTriggers.SelectedNode = node.FirstNode; 
                    break;
            }
        }

		private void buttonStart_Click(object sender, System.EventArgs e) {
			formProgress f;
			bool unselect = false;
            ActionList actionList;
            ArrayList previouslyCleared = new ArrayList();

            if (setter.Count == 0) {
				MessageBox.Show("You have not defined any conditions yet.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			if (parent.SelectedRooms.Count == 0) {
				unselect = true;
				parent.SelectAll();
			}

			f = new formProgress();
			f.labelDescription.Text = "Setting room properties...";
			f.barProgress.Minimum = 0;
			f.barProgress.Maximum = parent.SelectedRooms.Count;

			f.Show();
			Application.DoEvents(); // make sure the form is displayed properly

			foreach (Room r in parent.SelectedRooms) {
                foreach (Trigger t in setter) {
					if (!MatchesConditions(r, t)) continue;

                    if (!previouslyCleared.Contains(r)) {
                        r.ClearRoom();
                        previouslyCleared.Add(r);
                    }

					// select a random actionlist from actionlists
					actionList = GetRandomItem(t);

					foreach (Action action in actionList.actions) {
                        switch (action.type) {
                            case Action.ActionType.Set:
                                r.SetProperty(action.key, action.value);
                                break;
                            case Action.ActionType.Add:
                                r.AddToProperty(action.key, action.value);
                                break;

                        }
					}
				}
				parent.ValidateRoom(r.id);
				f.barProgress.Value += 1;
                Application.DoEvents();
			}

			f.Close();

			if (unselect) parent.ClearSelected();
			parent.Redraw(true);
			parent.Saved = false;
            System.Media.SystemSounds.Beep.Play();
		}

		private bool MatchesConditions(Room r, Trigger t) {
			string k, v;
            bool matches = false, exclude = false;

			foreach (Condition c in t.conditions) {
                exclude = c.exclude;
                k = c.key;
                v = c.value;

				matches = exclude;

				switch (c.type) {
                    case Condition.ConditionType.Random:
                        if (rand.Next(100) < Convert.ToInt32(v)) {
                            matches = true;
                        }
                        break;
					case Condition.ConditionType.HasExit:
						foreach (Exit ex in _area.Exits) {
							if ((ex.idFrom == r.id && ex.dirFrom == v) || (ex.idTo == r.id && ex.dirTo == v)) {	
								matches = true;
								break;
							} else matches = false;
						}
						break;
					case Condition.ConditionType.HasProperty:
                        if (r.GetProperty(k) == v) {
                            matches = true;
                        } else matches = false;
						break;
					case Condition.ConditionType.Any:
						matches = true;
						break;
				}

				if (matches == exclude) {
					return false;
				}
			}

			return true;
		}

        private void formSetter_FormClosing(object sender, FormClosingEventArgs e) {
            DialogResult result;

            result = MessageBox.Show("Are you sure you want to close the Room Setter?", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == DialogResult.No) {
                e.Cancel = true;
            }
        }

        private void formSetter_Load(object sender, EventArgs e) {
            UpdateButtons(nodeType);
        }

        private void UpdateTree() {
            treeTriggers.BeginUpdate();
            treeTriggers.Nodes.Clear();

            foreach (Trigger t in setter) {
                treeTriggers.Nodes.Add(t.ToTreeNode());
            }
            treeTriggers.ExpandAll();
            treeTriggers.EndUpdate();
        }

        private void treeTriggers_AfterSelect(object sender, TreeViewEventArgs e) {
            UpdateNodeType();
            UpdateButtons(nodeType);
            UpdateInterface(nodeType);
            UpdateKeyValuePair();
        }

        private void UpdateNodeType() {
            TreeNode node = treeTriggers.SelectedNode;
            TreeNode current;
            Trigger parent;

            current = GetParentNode(treeTriggers);

            if (current == null) return;

            parent = (Trigger)current.Tag;

            if (parent == node.Tag) {
                SetNodeType(NodeTypes.Trigger);
            } else {
                SetNodeType(GetTreeNodeType(node));
            }
        }

        private NodeTypes GetTreeNodeType(TreeNode node) {
            if (node.Tag is Trigger) {
                return NodeTypes.Trigger;
            } else if (node.Tag is Action) {
                return NodeTypes.Action;
            } else if (node.Tag is ActionList) {
                return NodeTypes.ActionList;
            } else if (node.Tag is Condition) {
                return NodeTypes.Condition;
            } else if (node.Text == "Conditions") {
                return NodeTypes.Conditions;
            } else if (node.Text == "Actions") {
                return NodeTypes.Actions;
            }

            return NodeTypes.None;
        }

        private TreeNode GetParentNode(TreeView view) {
            TreeNode current = view.SelectedNode;

            if (current == null) return null;

            while (current.Parent != null) {
                current = current.Parent;
            }

            return current;
        }

        private void SetNodeType(NodeTypes newType) {
            nodeType = newType;
        }

        private void UpdateInterface(NodeTypes type) {
            comboIndex.BeginUpdate();
            comboIndex.Items.Clear();
            switch (type) {
                case NodeTypes.Condition:
                    comboIndex.Items.Add("None");
                    comboIndex.Items.Add("Has Property");
                    comboIndex.Items.Add("Has Exit");
                    comboIndex.Items.Add("Random");
                    comboIndex.Enabled = true;

                    comboKey.Enabled = false;
                    comboKey.Text = "";
                    comboKey.Items.Clear();
                    textValue.Enabled = false;
                    textValue.Text = "";
                    checkExclude.Enabled = true;
                    checkExclude.Checked = false;
                    textValue.Multiline = false;
                    comboKey.Focus();
                    break;
                case NodeTypes.Action:
                    comboIndex.Items.Add("Set");
                    comboIndex.Items.Add("Add");
                    comboIndex.Enabled = true;
                    comboIndex.SelectedIndex = 0;

                    comboKey.Enabled = true;
                    comboKey.Text = "";
                    comboKey.Items.Clear();
                    comboKey.Focus();
                    textValue.Enabled = true;
                    textValue.Text = "";
                    checkExclude.Enabled = false;
                    checkExclude.Checked = false;
                    textValue.Multiline = true;
                    break;
                default:
                    comboIndex.Enabled = false;

                    comboKey.Enabled = false;
                    comboKey.Text = "";
                    comboKey.Items.Clear();
                    textValue.Enabled = false;
                    textValue.Text = "";
                    checkExclude.Enabled = false;
                    checkExclude.Checked = false;
                    textValue.Multiline = false;
                    break;
            }
            comboIndex.EndUpdate();
        }

        private void UpdateButtons(NodeTypes type) {
            switch (type) {
                case NodeTypes.Action:
                case NodeTypes.Condition:
                    buttonDeleteItem.Enabled = true;
                    buttonNewItem.Enabled = false;
                    buttonSaveItem.Enabled = true;
                    break;
                case NodeTypes.ActionList:
                    buttonDeleteItem.Enabled = true;
                    buttonNewItem.Enabled = true;
                    buttonSaveItem.Enabled = false;
                    break;
                case NodeTypes.None:
                    buttonDeleteItem.Enabled = false;
                    buttonNewItem.Enabled = true;
                    buttonSaveItem.Enabled = false;
                    break;
                case NodeTypes.Trigger:
                    buttonDeleteItem.Enabled = true;
                    buttonNewItem.Enabled = true;
                    buttonSaveItem.Enabled = false;
                    break;
                default:
                    buttonDeleteItem.Enabled = false;
                    buttonNewItem.Enabled = true;
                    buttonSaveItem.Enabled = false;
                    break;
            }
        }

        private void UpdateKeyValuePair() {
            TreeNode current = treeTriggers.SelectedNode;

            if (current == null) return;

            switch (nodeType) {
                case NodeTypes.Condition:
                    Condition c = (Condition)current.Tag;
                    switch (c.type) {
                        case Condition.ConditionType.Any:
                            comboIndex.SelectedIndex = 0;
                            break;
                        case Condition.ConditionType.HasProperty:
                            comboIndex.SelectedIndex = 1;
                            break;
                        case Condition.ConditionType.HasExit:
                            comboIndex.SelectedIndex = 2;
                            break;
                        case Condition.ConditionType.Random:
                            comboIndex.SelectedIndex = 3;
                            break;
                    }
                    comboKey.Text = c.key;
                    textValue.Text = c.value;
                    break;
                case NodeTypes.Action:
                    Action a = (Action)current.Tag;
                    switch (a.type) {
                        case Action.ActionType.Add:
                            comboIndex.SelectedIndex = 1;
                            break;
                        case Action.ActionType.Set:
                            comboIndex.SelectedIndex = 0;
                            break;
                    }
                    comboKey.Text = a.key;
                    textValue.Text = a.value;
                    break;
            }
        }

        private void comboIndex_SelectedIndexChanged(object sender, EventArgs e) {
            comboKey.BeginUpdate();
            switch (comboIndex.Text.ToLower()) {
                case "has property":
                    comboKey.Items.Clear();
                    comboKey.Enabled = true;
                    textValue.Enabled = true;
                    checkExclude.Enabled = true;
                    break;
                case "add":
                case "set":
                    comboKey.Items.Clear();
                    comboKey.Enabled = true;
                    comboKey.Items.Add("short");
                    comboKey.Items.Add("long");
                    comboKey.Items.Add("items");
                    comboKey.Items.Add("smells");
                    comboKey.Items.Add("listens");
                    textValue.Enabled = true;
                    checkExclude.Enabled = false;
                    break;
                case "has exit":
                    comboKey.Text = "";
                    comboKey.Enabled = false;
                    textValue.Enabled = true;
                    checkExclude.Enabled = true;
                    break;
                case "none":
                    comboKey.Enabled = false;
                    textValue.Enabled = false;
                    checkExclude.Enabled = false;
                    break;
                case "random":
                    comboKey.Text = "";
                    comboKey.Enabled = false;
                    textValue.Enabled = true;
                    checkExclude.Enabled = false;
                    break;
            }
            comboKey.EndUpdate();
        }

        private void buttonDeleteItem_Click(object sender, EventArgs e) {
            int index;
            TreeNode parent = GetParentNode(treeTriggers), current = treeTriggers.SelectedNode;
            Trigger t;

            if (parent == null || current == null) return;
            index = setter.IndexOf((Trigger)parent.Tag);
            t = (Trigger)setter[index];

            switch (nodeType) {
                case NodeTypes.Condition:
                    t.conditions.Remove((Condition)current.Tag);
                    treeTriggers.SelectedNode = current.PrevNode;
                    current.Parent.Nodes.Remove(current);
                    break;
                case NodeTypes.ActionList:
                    t.actionsLists.Remove((ActionList)current.Tag);
                    treeTriggers.SelectedNode = current.PrevNode;
                    current.Parent.Nodes.Remove(current);
                    break;
                case NodeTypes.Action:
                    TreeNode previous = current.Parent;
                    foreach (ActionList al in t.actionsLists) {
                        index = al.actions.IndexOf(current.Tag);
                        if (index == -1) continue;
                        al.actions.Remove(current.Tag);
                        treeTriggers.SelectedNode = current.PrevNode;
                        current.Parent.Nodes.Remove(current);
                        if (previous.Nodes.Count == 0) {
                            previous.Parent.Nodes.Remove(previous);
                        } else {
                            previous.Text = al.ToString();
                        }
                        break;
                    }
                    break;
                case NodeTypes.Trigger:
                    setter.Remove((Trigger)current.Tag);
                    treeTriggers.Nodes.Remove(current);
                    SetNodeType(GetTreeNodeType(treeTriggers.SelectedNode));
                    UpdateButtons(nodeType);
                    UpdateInterface(nodeType);
                    UpdateKeyValuePair();
                    break;
                default:
                    return;
            }
        }

        private void buttonSaveItem_Click(object sender, EventArgs e) {
            int index;
            TreeNode parent = GetParentNode(treeTriggers), current = treeTriggers.SelectedNode;
            Trigger t;
            TreeNode newNode;

            if (parent == null || current == null) return;
            index = setter.IndexOf((Trigger)parent.Tag);
            t = (Trigger)setter[index];

            switch (nodeType) {
                case NodeTypes.Condition:
                    Condition editCondition = (Condition)current.Tag;
                    editCondition.type = ComboToConditionType(comboIndex);
                    editCondition.key = comboKey.Text;
                    editCondition.value = textValue.Text;
                    editCondition.exclude = checkExclude.Checked;

                    current.Text = editCondition.ToString();
                    break;
                case NodeTypes.Conditions:
                    if (comboIndex.SelectedIndex < 0) {
                        Common.Error("You must select a Condition.");
                        return;
                    }
                    Condition newCondition = new Condition();
                    newCondition.type = ComboToConditionType(comboIndex);
                    newCondition.key = comboKey.Text;
                    newCondition.value = textValue.Text;
                    newCondition.exclude = checkExclude.Checked;

                    t.conditions.Add(newCondition);
                    newNode = newCondition.ToTreeNode();
                    current.Nodes.Add(newNode);
                    treeTriggers.SelectedNode = current;
                    current.ExpandAll();

                    UpdateInterface(nodeType);
                    UpdateButtons(nodeType);
                    UpdateKeyValuePair();
                    break;
                case NodeTypes.ActionList:
                    if (comboKey.Text.Length == 0) {
                        Common.Error("You must enter a property to set.");
                        return;
                    }

                    ActionList al = (ActionList)current.Tag;

                    Action newAction = new Action();
                    newAction.key = comboKey.Text;
                    newAction.value = textValue.Text;
                    newAction.type = ComboToActionType(comboIndex);

                    al.actions.Add(newAction);
                    newNode = newAction.ToTreeNode();
                    current.Nodes.Add(newNode);
                    current.ExpandAll();
                    current.Text = al.ToString();
                    treeTriggers.SelectedNode = current;

                    UpdateInterface(nodeType);
                    UpdateButtons(nodeType);
                    UpdateKeyValuePair();
                    break;
                case NodeTypes.Action:
                    if (comboKey.Text.Length == 0) {
                        Common.Error("You must enter a property to set.");
                        return;
                    }

                    Action editAction = (Action)current.Tag;

                    editAction.key = comboKey.Text;
                    editAction.value = textValue.Text;
                    editAction.type = ComboToActionType(comboIndex);

                    current.Text = editAction.ToString();
                    ActionList parentList = (ActionList)current.Parent.Tag;
                    current.Parent.Text = parentList.ToString();

                    break;
            }
        }

        private Action.ActionType ComboToActionType(ComboBox combo) {
            Action.ActionType value = Action.ActionType.Set;
            switch (combo.SelectedItem.ToString().ToLower()) {
                case "set":
                    value = Action.ActionType.Set;
                    break;
                case "add":
                    value = Action.ActionType.Add;
                    break;
            }

            return value;
        }

        public Condition.ConditionType ComboToConditionType(ComboBox combo) {
            Condition.ConditionType value = Condition.ConditionType.Any;
            switch (combo.SelectedItem.ToString().ToLower()) {
                case "none":
                    value = Condition.ConditionType.Any;
                    break;
                case "has property":
                    value = Condition.ConditionType.HasProperty;
                    break;
                case "has exit":
                    value = Condition.ConditionType.HasExit;
                    break;
                case "random":
                    value = Condition.ConditionType.Random;
                    break;
            }

            return value;
        }

        private void buttonMoveUp_Click(object sender, EventArgs e) {
            Trigger node1, node2;

            int i = treeTriggers.SelectedNode.Index;
            if (i == 0) return;
            node1 = setter[i];
            node2 = setter[i - 1];
            setter.RemoveAt(i - 1);
            setter.RemoveAt(i - 1);
            setter.Insert(i - 1, node2);
            setter.Insert(i - 1, node1);
            UpdateTree();
            treeTriggers.SelectedNode = treeTriggers.Nodes[i - 1];
        }

        private void buttonMoveDown_Click(object sender, EventArgs e) {
            Trigger node1, node2;

            int i = treeTriggers.SelectedNode.Index;
            if (i == treeTriggers.Nodes.Count - 1) return;
            node1 = setter[i];
            node2 = setter[i + 1];
            setter.RemoveAt(i);
            setter.RemoveAt(i);
            setter.Insert(i, node1);
            setter.Insert(i, node2);
            UpdateTree();
            treeTriggers.SelectedNode = treeTriggers.Nodes[i + 1];
        }

        private void treeTriggers_KeyPress(object sender, KeyPressEventArgs e) {

        }
	}
}
