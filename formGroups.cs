using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formGroups : Form {
        private readonly formMain parent;
        private readonly Area _area;

        public formGroups(formMain form, Area area) {
            InitializeComponent();
            parent = form;
            _area = area;
        }

        private void formGroups_Load(object sender, EventArgs e) {
            UpdateGroupList();
            UpdateButtons();
            Common.LoadTemplates(comboTemplate, parent, null);
            Common.LoadLayouts(comboLayout, parent);
        }

        private void UpdateGroupList() {
            listGroups.Items.Clear();
            foreach (var gg in _area.Groups.Values) {
                var item = new ListViewItem();
                item.Text = gg.Name;
                item.Tag = gg;
                listGroups.Items.Add(item);
            }
        }

        private void UpdateButtons() {
            if (listGroups.SelectedItems.Count == 0) buttonDelete.Enabled = false; else buttonDelete.Enabled = true;
            if (textName.Text.Length == 0) buttonSave.Enabled = false; else buttonSave.Enabled = true;
        }

        private void textName_TextChanged(object sender, EventArgs e) {
            UpdateButtons();
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            ClearFields();
			textName.Enabled = true;
        }

        private void ClearFields() {
            listGroups.SelectedItems.Clear();
            textName.Text = "";
            textFolder.Text = "";
            UpdateButtons();
            textName.Focus();
            comboTemplate.SelectedIndex = -1;
            comboLayout.SelectedIndex = -1;
        }

        private void buttonClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            Boolean isNew = true;

            if (listGroups.SelectedItems.Count > 0) isNew = false;

            if (textName.Text.Length == 0) {
                Common.Warn("You cannot save a group with an empty name.");
                textName.Focus();
                return;
            } else if (isNew && parent.GroupNameTaken(textName.Text)) {
                Common.Warn("A group with this name already exists.");
                textName.Focus();
                textName.SelectAll();
                return;
            }

            if (comboTemplate.SelectedIndex < 0) {
                Common.Warn("You must select a template.");
                return;
            }

			if (comboLayout.SelectedIndex < 0) {
				Common.Warn("You must select a layout.");
				return;
			}

			Group g;
			if (!isNew) {
				g = (Group)listGroups.SelectedItems[0].Tag;
			} else {
				g = parent.NewGroup();
			}

			g.Name = textName.Text.ToLower();
            g.Template = comboTemplate.Text;
            g.Folder = textFolder.Text;
            g.LayoutFile = comboLayout.Text;
            parent.UpdateGroup(g);

            if (isNew) {
                parent.AddGroup(g.Name, g);
            }
            parent.Saved = false;
            UpdateGroupList();
            ClearFields();
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            if (listGroups.SelectedItems.Count == 0) return;

            string group = parent.DeleteGroup(listGroups.SelectedItems[0].Text);
            UpdateGroupList();
            parent.Saved = false;
            ClearFields();
        }

        private void buttonFolder_Click(object sender, EventArgs e) {
            browseFolder.Description = "Choose a folder where the output for this group will be saved.";
            browseFolder.ShowNewFolderButton = true;

            if (browseFolder.ShowDialog() == DialogResult.Cancel) {
                return;
            }

            textFolder.Text = browseFolder.SelectedPath;
        }

        private void listGroups_SelectedIndexChanged(object sender, EventArgs e) {
            Group gg;

            if (listGroups.SelectedItems.Count == 0) {
                ClearFields();
                return;
            }

            if (!_area.Groups.ContainsKey(listGroups.SelectedItems[0].Text)) return;

            gg = _area.Groups[listGroups.SelectedItems[0].Text];

            textName.Text = gg.Name;
			textName.Enabled = false;
            Common.SetComboSelected(comboTemplate, gg.Template);
            textFolder.Text = gg.Folder;
            Common.SetComboSelected(comboLayout, gg.LayoutFile);
        }

    }
}