﻿using System.Collections.Generic;
using System.IO;

namespace Genesis {
    public class Area {
        public Dictionary<int, Room> Rooms { get; set; }
        public List<Exit> Exits { get; set; }
        public Dictionary<string, GenesisObject> Objects { get; set; }
        public Dictionary<string, Group> Groups { get; set; }
        public Dictionary<string, string> Constants { get; set; }

        public string SaveFile = string.Empty;
        public string DefaultRoomTemplate = string.Empty;
        public string DefaultRoomFileNamePrefix = string.Empty;
        public string RoomFolderName = string.Empty;
        public string FileLineBreak;
        public int RoomIdOffset = 0;

        public Area() {
            Rooms = new Dictionary<int, Room>();
            Exits = new List<Exit>();
            Objects = new Dictionary<string, GenesisObject>();
            Groups = new Dictionary<string, Group>();
            Constants = new Dictionary<string, string>();
        }

        public string SavePath {
            get { return Path.GetDirectoryName(SaveFile); }
        }

        public void Clear() {
            Rooms.Clear();
            Exits.Clear();
            Objects.Clear();
            Groups.Clear();
        }
    }
}
