using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formObjects : Form {
        private readonly formMain parent;
        private readonly Area _area;
        public string SelectedObject = "";

        public formObjects(formMain p, Area area) {
            InitializeComponent();
            parent = p;
            _area = area;
            UpdateObjectList();
            UpdateButtons();
        }

        public void UpdateObjectList() {
            listObjects.SuspendLayout();
            listObjects.Items.Clear();
            listObjects.Groups.Clear();
            foreach (var gg in _area.Groups.Values) {
                var lvg = new ListViewGroup(gg.Name);
                lvg.Tag = gg;
                listObjects.Groups.Add(lvg);

                foreach (var go in _area.Objects.Values) {
                    if (go.Group == null) continue;
                    if (!go.Group.Equals(gg)) continue;
                    var lvi = new ListViewItem(go.id);
                    lvi.Group = lvg;
                    lvi.Tag = go;
                    listObjects.Items.Add(lvi);
                }
            }
            listObjects.ResumeLayout();
        }

        private void UpdateButtons() {
            if (listObjects.SelectedItems.Count == 0) buttonEdit.Enabled = false; else buttonEdit.Enabled = true;
            if (parent.CurrentRoom != null && listObjects.SelectedItems.Count > 0) buttonAddToEditor.Enabled = true; else buttonAddToEditor.Enabled = false;
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            if (_area.Groups.Count == 0) {
                Common.Warn("You have not defined any groups yet.");
                return;
            }

            menuNewObject.Items.Clear();
            foreach (var group in _area.Groups.Values) {
                var item = new ToolStripMenuItem();
                item.Text = group.Name;
                item.Click += new EventHandler(item_Click);
                menuNewObject.Items.Add(item);
            }

            menuNewObject.Show(buttonNew, 0, buttonNew.Height);
        }

        void item_Click(object sender, EventArgs e) {
            GenesisObject go = parent.CreateObject();
            if (!_area.Groups.ContainsKey(((ToolStripMenuItem)sender).Text)) {
                return;
            }
            Group group = _area.Groups[((ToolStripMenuItem)sender).Text];
            go.Group = group;
            UpdateObjectList();
            parent.LoadEditor();
            parent.Editor.LoadGenesisObject(ref go);
            parent.Editor.SetSaved(false);
            //UpdateObjectList();
        }

        private void buttonClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void formObjects_FormClosing(object sender, FormClosingEventArgs e) {
            parent.ObjectsForm = null;
            parent.ObjectsFormLeft = this.Left;
            parent.ObjectsFormTop = this.Top;
        }

        private void listObjects_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateButtons();
            if (listObjects.SelectedItems.Count == 0) return;
            SelectedObject = listObjects.SelectedItems[0].Text;
        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            EditObject();
        }

        private void EditObject() {
            GenesisObject go = null;
            if (listObjects.SelectedItems.Count == 0) return;
            if (_area.Objects.ContainsKey(listObjects.SelectedItems[0].Text)) {
                go = _area.Objects[listObjects.SelectedItems[0].Text];
            }
            if (go == null) return;
            parent.LoadEditor();
            parent.Editor.LoadGenesisObject(ref go);
        }

        private void buttonAddToRoom_Click(object sender, EventArgs e) {
            ListViewItem item;
            if (parent.Editor == null) return;

			if (listObjects.SelectedItems.Count == 0) {
				return;
			}

            item = listObjects.SelectedItems[0];
            if (item.Tag != null) {
                parent.Editor.AddObjectToCurrentObject((GenesisObject)item.Tag);
            } else {
            }
            parent.Saved = false;
        }

        private void listObjects_DoubleClick(object sender, EventArgs e) {
            EditObject();
        }
    }
}