using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Genesis {
    abstract public class DetailField {
        protected String _name;
        public String Name {
            get { return _name; }
            set { _name = value; }
        }

        private String _source;
        public String Source {
            get { return _source; }
            set { _source = value; }
        }
        private Object _data;
        public Object Data {
            get { return _data; }
            set { _data = value; }
        }

        abstract public ContainerControl toControls(BaseObject ob, EventHandler changeHandler);
        abstract public void ProcessControls(ContainerControl source, BaseObject ob);
        abstract public String getTypeName();
    }

    public class FieldText : DetailField {
        public override ContainerControl toControls(BaseObject ob, EventHandler changeHandler) {
            ContainerControl list = new ContainerControl();
            list.Tag = this;
            TextBox textBox = new TextBox();

            textBox.Dock = DockStyle.Fill;
            textBox.Text = ob.GetProperty(this.Source);
            textBox.TextChanged += changeHandler;
            textBox.Font = Common.GetEditorFont();

            list.Dock = DockStyle.Fill;
            list.Height = textBox.Height + 6;
            list.Controls.Add(textBox);

            return list;
        }

        override public void ProcessControls(ContainerControl source, BaseObject ob) {
            ob.SetProperty(this.Source, source.Controls[0].Text);
        }

        public override string getTypeName() {
            return "Text";
        }
    }

    public class FieldTextArea : DetailField {
        private int _height = 100;

        public int Height {
            get { return _height; }
            set { _height = value; }
        }

        public override ContainerControl toControls(BaseObject ob, EventHandler changeHandler) {
            ContainerControl list = new ContainerControl();
            list.Tag = this;
            TextBox textBox = new TextBox();

            textBox.Dock = DockStyle.Fill;
            textBox.Height = Height;
            textBox.Multiline = true;
            textBox.ScrollBars = ScrollBars.Both;
            textBox.Text = ob.GetProperty(this.Source);
            textBox.TextChanged += changeHandler;
            textBox.Font = Common.GetEditorFont();

            list.Dock = DockStyle.Fill;
            list.Height = textBox.Height;
            list.Controls.Add(textBox);

            return list;
        }

        override public void ProcessControls(ContainerControl source, BaseObject ob) {
            ob.SetProperty(this.Source, source.Controls[0].Text);
        }

        public override string getTypeName() {
            return "Text Area";
        }
    }

    public class FieldNumber : DetailField {
        private int _min = 100;

        public int Min {
            get { return _min; }
            set { _min = value; }
        }
        private int _max = 100;

        public int Max {
            get { return _max; }
            set { _max = value; }
        }

        public override ContainerControl toControls(BaseObject ob, EventHandler changeHandler) {
            ContainerControl list = new ContainerControl();
            list.Tag = this;
            NumericUpDown upDown = new NumericUpDown();

            upDown.Dock = DockStyle.Fill;
            upDown.Maximum = _max;
            upDown.Minimum = _min;
            upDown.ValueChanged += changeHandler;
            if (ob.HasProperty(Source)) {
                upDown.Value = Int32.Parse(ob.GetProperty(this.Source));
            }
            upDown.Font = Common.GetEditorFont();

            list.Dock = DockStyle.Fill;
            list.Height = upDown.Height + 6;
            list.Controls.Add(upDown);

            return list;
        }

        override public void ProcessControls(ContainerControl source, BaseObject ob) {
            ob.SetProperty(this.Source, source.Controls[0].Text);
        }

        public override string getTypeName() {
            return "Number";
        }
    }

    public class FieldCheckbox : DetailField {
        public override ContainerControl toControls(BaseObject ob, EventHandler changeHandler) {
            ContainerControl list = new ContainerControl();
            list.Tag = this;
            CheckBox checkBox = new CheckBox();

            checkBox.Dock = DockStyle.Fill;
            checkBox.CheckedChanged += changeHandler;
            checkBox.Checked = Boolean.Parse(ob.GetProperty(this.Source) == "1" ? "True" : "False");
            checkBox.Font = Common.GetEditorFont();

            list.Dock = DockStyle.Fill;
            list.Height = checkBox.Height + 6;
            list.Controls.Add(checkBox);

            return list;
        }

        override public void ProcessControls(ContainerControl source, BaseObject ob) {
            ob.SetProperty(this.Source, ((CheckBox)source.Controls[0]).Checked ? "1" : "0");
        }

        public override string getTypeName() {
            return "Checkbox";
        }
    }

    public class FieldDropdown : DetailField {
        private List<String> values;

        public List<String> Values {
            get { return values; }
            set { values = value; }
        }

        public override ContainerControl toControls(BaseObject ob, EventHandler changeHandler) {
            ContainerControl list = new ContainerControl();
            list.Tag = this;
            ComboBox combo = new ComboBox();

            combo.Dock = DockStyle.Fill;
            combo.SelectedIndexChanged += changeHandler;
            foreach (String str in values) {
                combo.Items.Add(str);
            }
            Common.SetComboSelected(combo, ob.GetProperty(Source));
            combo.Font = Common.GetEditorFont();

            list.Dock = DockStyle.Fill;
            list.Height = combo.Height + 10;
            list.Controls.Add(combo);

            return list;
        }

        override public void ProcessControls(ContainerControl source, BaseObject ob) {
            ob.SetProperty(this.Source, ((ComboBox)source.Controls[0]).Text);
        }

        public override string getTypeName() {
            return "Dropdown";
        }
    }
}
