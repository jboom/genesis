﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genesis {
    public class TemplateCache {
        private Dictionary<string, List<string>> _cachedTemplates;

        public List<string> LoadTemplate(string name) {
            var templateLines = new List<string>();

            if (_cachedTemplates == null) _cachedTemplates = new Dictionary<String, List<String>>();

            if (_cachedTemplates.ContainsKey(name)) {
                return _cachedTemplates[name];
            }

            var filename = Common.GetTemplatesPath() + name + Common.EXT_TEMPLATE;

            if (!File.Exists(filename)) {
                Common.Error("An individual template could not be found." + Environment.NewLine + Environment.NewLine + name);
                return templateLines;
            }

            try {
                using (var inputStream = new StreamReader(filename)) {
                    string line;
                    while ((line = inputStream.ReadLine()) != null) {
                        templateLines.Add(line);
                    }
                }
            } catch {
                Common.Error("An error occured while trying to load a template.");

                return templateLines;
            }

            if (!_cachedTemplates.ContainsKey(name)) {
                _cachedTemplates.Add(name, templateLines);
            }

            return templateLines;
        }
    }
}
