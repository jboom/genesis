using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formAreaMap : Form {
        private Pen viewportPen;
        private formMain parent;
        float cwr, chr;
        bool isDragging;
        int startX, startY;
        int oldX, oldY;

        public formAreaMap(formMain p) {
            InitializeComponent();
            viewportPen = new Pen(Color.Red, 1);
            parent = p;
            isDragging = false;
        }

        private void panelViewPort_Paint(object sender, PaintEventArgs e) {
            e.Graphics.DrawRectangle(viewportPen, 0, 0, panelViewPort.Width - 1, panelViewPort.Height - 1);
        }

        private void formAreaMap_FormClosing(object sender, FormClosingEventArgs e) {
            parent.Overview = null;
        }

        public void UpdateDisplay(float wr, float hr, float x, float y, float xo, float yo) {
            
            int width = (int)(ClientRectangle.Width / wr);
            int height = (int)(ClientRectangle.Height / hr);
            cwr = (float)(Convert.ToDouble(parent.ViewportWidth) / Convert.ToDouble(ClientRectangle.Width));
            chr = (float)(Convert.ToDouble(parent.ViewportHeight) / Convert.ToDouble(ClientRectangle.Height));
            int nx = (int)((0 - x) / cwr);
            int ny = (int)((0 - y) / chr);

            if (nx + width > ClientRectangle.Width) nx = ClientRectangle.Width - width;
            if (ny + height > ClientRectangle.Height) ny = ClientRectangle.Height - height;
            if (nx < 0) nx = 0;
            if (ny < 0) ny = 0;

            panelViewPort.Width = width;
            panelViewPort.Height = height;
            panelViewPort.Left = nx;
            panelViewPort.Top = ny;
            panelViewPort.Invalidate();
        }

        private void panelViewPort_MouseDown(object sender, MouseEventArgs e) {
            if (isDragging) {
                isDragging = false;
                return;
            }

            isDragging = true;
            oldX = panelViewPort.Left;
            oldY = panelViewPort.Top;
            startX = e.X;
            startY = e.Y;
        }

        private void panelViewPort_MouseUp(object sender, MouseEventArgs e) {
            if (!isDragging) return;

            isDragging = false;
            int nx = panelViewPort.Left;
            nx = (int)(nx * cwr);
            int ny = panelViewPort.Top;
            ny = (int)(ny * chr);
            parent.UpdateOffsets(nx, ny);
        }

        private void panelViewPort_MouseMove(object sender, MouseEventArgs e) {
            if (!isDragging) return;
            panelViewPort.Left = oldX + (startX + e.X);
            panelViewPort.Top = oldY + (startY + e.Y);
            //startX = e.X;
            //startY = e.Y;
        }
    }
}