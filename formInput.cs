using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formInput : Form {
        public formInput(string question, string answer) {
            InitializeComponent();
            Text = question;
            textInput.Text = answer;
        }

        public string Value {
            get { return textInput.Text; }
            set { textInput.Text = value; }
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void formInput_Load(object sender, EventArgs e) {
            textInput.SelectAll();
        }
    }
}