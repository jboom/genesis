using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    public partial class formGoTo : Form {
        public int ID;

        public formGoTo() {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            ID = Convert.ToInt32(comboID.Text);
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}