using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Genesis {
    [System.Xml.Serialization.XmlRoot(ElementName = "Setter", IsNullable = false)]
    [System.Xml.Serialization.XmlInclude(typeof(Trigger))]
    [System.Xml.Serialization.XmlInclude(typeof(Action))]
    [System.Xml.Serialization.XmlInclude(typeof(ActionList))]
    [System.Xml.Serialization.XmlInclude(typeof(Condition))]
    public class Setter : List<Trigger> {
        public static void Save(string filename, Setter setter) {
            try {
                XmlSerializer xs = new XmlSerializer(typeof(Setter));
                TextWriter tw = new StreamWriter(filename);
                xs.Serialize(tw, setter);
                tw.Close();
            } catch (Exception e) {
                Log.error(e);
            }
        }

        public static Setter Load(string filename) {
            Setter setter;
            try {
                XmlSerializer xs = new XmlSerializer(typeof(Setter));
                FileStream fs = new FileStream(filename, FileMode.Open);
                setter = (Setter)xs.Deserialize(fs);
                fs.Close();
            } catch (Exception e) {
                Log.error(e);
                return null;
            }

            return setter;
        }
    }
}
