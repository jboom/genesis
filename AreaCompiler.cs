﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Genesis {
    public class AreaCompiler {
        private readonly Area _area;
        private readonly TemplateCache _templateCache;
        private readonly bool _excludeNotModified;
        private readonly System.Action _progressIncrementAction;
        private readonly ProgramOptions _options;

        public AreaCompiler(Area area, bool excludeNotModified, ProgramOptions options, System.Action progressIncrementAction) {
            _area = area;
            _templateCache = new TemplateCache();
            _excludeNotModified = excludeNotModified;
            _progressIncrementAction = progressIncrementAction;
            _options = options;
        }

        public int CompileRooms(List<Room> selectedRooms) {
            var compiledRooms = 0;
            var standardTemplate = _templateCache.LoadTemplate(_area.DefaultRoomTemplate);

            foreach (var room in selectedRooms) {
                if (!room.include) continue;
                if (_excludeNotModified && !room.updated) continue;
                room.updated = false;

                try {
                    // set template to custom or standard
                    List<string> templateLines;
                    if (!room.HasProperty("template")) {
                        templateLines = standardTemplate;
                    } else {
                        templateLines = _templateCache.LoadTemplate(room.GetProperty("template"));
                    }

                    // get hashtable of exits
                    var exitList = new Dictionary<string, string>();
                    var invisibleExistList = new Dictionary<string, string>();
                    GetRoomExits(room.id, exitList, invisibleExistList);

                    var extraLists = new Dictionary<string, object> {
                        {"exits", exitList}, 
                        {"invexits", invisibleExistList}
                    };

                    // create a list of all objects in this room
                    foreach (var group in _area.Groups.Values) {
                        // create an object list per object group
                        var tempHashTable = new Dictionary<string, List<string>>();

                        // for each object in this room
                        foreach (GenesisObject gObject in room.objects) {
                            if (gObject == null) continue;
                            if (!gObject.Group.Equals(group)) continue;
                            if (tempHashTable.ContainsKey(gObject.id)) {
                                // object list already contains an entry, add another item to the entry
                                List<String> arrayList = (List<String>)tempHashTable[gObject.id];
                                arrayList.Add(GetObjectFileName(gObject, group));
                                tempHashTable[gObject.id] = arrayList;
                            } else {
                                // object doesn't have an entry yet, make one
                                var arrayList = new List<string> {GetObjectFileName(gObject, group)};
                                tempHashTable.Add(gObject.id, arrayList);
                            }
                        }

                        if (tempHashTable.Count > 0) {
                            // add the list to the master list
                            extraLists.Add(group.Name.ToLower(), tempHashTable);
                        }
                    }

                    // Create output file and parse template lines.
                    using (var outputStream = new StreamWriter(Path.Combine(_area.SavePath, _area.RoomFolderName, GetRoomFileName(room)))) {
                        ApplyTemplate(room, templateLines, outputStream, extraLists);
                    }

                    _progressIncrementAction();
                    compiledRooms++;
                    Application.DoEvents(); // ensure form is displayed properly                    
                } catch {
                    // file failed to create
                    Common.Error("An error occured while trying to create a room file.");
                    return 0;
                }
            }

            return compiledRooms;
        }

        public int CompileObjects() {
            List<String> template = new List<String>();
            Dictionary<String, Object> el = new Dictionary<String, Object>();
            Hashtable ol;

            var CompiledObjects = 0;

            foreach (var go in _area.Objects.Values) {
                try {
                    using (var sw = new StreamWriter(Path.Combine(_area.SavePath, go.Group.Folder, GetObjectFileName(go)))) {
                        template = _templateCache.LoadTemplate(go.Group.Template);

                        // create a list of any contained objects
                        foreach (var group in _area.Groups.Values) {
                            // create an object list per object group
                            ol = new Hashtable();

                            // for each object in this object
                            foreach (GenesisObject ob in go.objects) {
                                if (ob.Group == null) continue;
                                if (!ob.Group.Equals(group)) continue;
                                if (ol.ContainsKey(ob.id)) {
                                    // object list already contains an entry, add another item to the entry
                                    ArrayList a = (ArrayList)ol[ob.id];
                                    a.Add(GetObjectFileName(ob, group));
                                    ol[ob.id] = a;
                                } else {
                                    // object doesn't have an entry yet, make one
                                    ArrayList a = new ArrayList();
                                    a.Add(GetObjectFileName(ob, group));
                                    ol.Add(ob.id, a);
                                }
                            }

                            if (ol.Count > 0) {
                                // add the list to the master list
                                el.Add(go.Group.Name.ToLower(), ol);
                            }
                        }

                        // parse template lines
                        ApplyTemplate(go, template, sw, el);
                        CompiledObjects++;
                        _progressIncrementAction();
                    }
                } catch {
                    // file failed to create
                    Common.Error("An error occured while trying to create an object file.");
                    return 0;
                }
            }

            return CompiledObjects;
        }

        private void ApplyTemplate(BaseObject ob, List<String> input, StreamWriter output, Dictionary<String, Object> extraLists) {
            string s, line;
            Room tempRoom;
            List<GroupCollection> Captures;
            MatchCollection mc;
            Match match;
            Hashtable h = new Hashtable();
            Regex rxRepeat, rxNormal, rxBlock, rxBlockEnd, rxConstant;
            bool inBlock = false, blockMatches = true, keepLine;

            rxRepeat = new Regex(@"@([^\[]*?)\[([^\]]*?)\]([!]{0,1})@");
            rxNormal = new Regex(@"\$([^\[]*?)\[([^\]]*?)\]([!]{0,1})\$");
            rxBlock = new Regex(@"^#(\w*)#$");
            rxBlockEnd = new Regex(@"^#end#$");
            rxConstant = new Regex(@"\$\!([^\$]+?)\$");

            foreach (string l in input) {
                if (l.StartsWith("@@")) continue;
                keepLine = true;
                Captures = new List<GroupCollection>();

                if (l.StartsWith("##include ")) {
                    string[] include = l.Split(new char[] { ' ' }, 2);
                    if (include.Length == 2) {
                        IncludeFileInTemplate(output, include[1]);
                    }
                    continue;
                }

                if (!inBlock) {
                    match = rxBlock.Match(l);
                    if (match.Success) {
                        inBlock = true;
                        blockMatches = false;
                        if (match.Groups.Count > 1 && ob.GetProperty(match.Groups[1].Value) != "") {
                            blockMatches = true;
                        } else if (match.Groups.Count > 1 && extraLists.ContainsKey(match.Groups[1].Value)) {
                            blockMatches = true;
                        }
                        continue;
                    }
                } else {
                    match = rxBlockEnd.Match(l);
                    if (match.Success) {
                        inBlock = false;
                        blockMatches = true;
                        continue;
                    }
                    if (!blockMatches) continue;
                }

                line = l;

                // parse out any single instances
                mc = rxNormal.Matches(line);
                if (mc.Count > 0) {
                    // line contains single line identifier
                    // collect the match collections
                    foreach (System.Text.RegularExpressions.Match m in mc) {
                        if (m.Groups.Count >= 3) {
                            Captures.Add(m.Groups);
                        }
                    }

                    if (h == null) continue;

                    // replace each captured property
                    foreach (System.Text.RegularExpressions.GroupCollection gc in Captures) {
                        if (ob.GetProperty(gc[2].Value).Length == 0) {
                            if (gc.Count < 4 || (gc.Count > 3 && gc[3].Value != "!")) {
                                line = line.Replace(gc[0].Value, "");
                                keepLine = false;
                                break;
                            }
                        }
                        if (gc[1].Value == "value") {
                            line = line.Replace(gc[0].Value, ob.GetProperty(gc[2].Value));
                        } else if (gc[1].Value.StartsWith("room")) {
                            // find the exit and the target
                            tempRoom = GetRoom(Convert.ToInt32(gc[2].Value));
                            if (tempRoom != null) {
                                line = line.Replace(gc[0].Value, GetRoomFileName(tempRoom));
                            }
                        }
                    }
                }

                // parse out any constants
                mc = rxConstant.Matches(line);
                string constant;
                if (mc.Count > 0) {
                    foreach (Match m in mc) {
                        constant = m.Groups[1].Value;
                        if (_area.Constants.ContainsKey(constant)) {
                            line = line.Replace(m.Groups[0].Value, _area.Constants[constant]);
                        }
                    }
                }

                // parse out any repeat instances
                mc = rxRepeat.Matches(line);
                if (mc.Count > 0) {
                    // line contains repeat identifier
                    // collect the match collections and the hashtable with the properties needed
                    foreach (Match m in mc) {
                        if (m.Groups.Count >= 3) {
                            if (extraLists.ContainsKey(m.Groups[2].Value)) {
                                h = (Hashtable)extraLists[m.Groups[2].Value];
                            } else {
                                h = Common.GetMultiHash(ob.GetProperty(m.Groups[2].Value));
                            }
                            Captures.Add(m.Groups);
                        }
                    }

                    // write lines for each entry in the properties list
                    if (h == null) continue;
                    foreach (DictionaryEntry prop in h) {
                        ArrayList list;
                        s = line;
                        if (prop.Value is ArrayList) {
                            // if prop is arraylist then it is from objects
                            list = (ArrayList)prop.Value;
                        } else if (prop.Value is List<String>) {
                            // if prop is arraylist then it is from objects
                            list = new ArrayList();
                            foreach (string str in (List<String>)prop.Value) {
                                list.Add(str);
                            }
                        } else if (prop.Value is String) {
                            // else it is from a normal list
                            list = new ArrayList();
                            list.Add((string)prop.Value);
                        } else {
                            list = new ArrayList();
                        }

                        foreach (string str in list) {
                            foreach (GroupCollection gc in Captures) {
                                if (gc[1].Value == "key" || gc[1].Value == "direction") {
                                    s = s.Replace(gc[0].Value, (string)prop.Key);
                                } else if (gc[1].Value == "value" || gc[1].Value == "target") {
                                    s = s.Replace(gc[0].Value, str);
                                }
                            }
                            // write the line to the file
                            output.Write(s + _area.FileLineBreak);
                        }
                    }
                } else {
                    if (keepLine) output.Write(line + _area.FileLineBreak);
                }
            }

            output.Close();
        }

        private void IncludeFileInTemplate(StreamWriter output, string include) {
            StreamReader sr;
            try {
                sr = new StreamReader(_area.RoomFolderName + @"\" + include);
            } catch {
                Common.Error("Could not find the file " + include + " specified in a template file.");
                return;
            }
            while (!sr.EndOfStream) {
                output.Write(sr.ReadLine() + _area.FileLineBreak);
            }
            sr.Close();
        }

        public Room GetRoom(int id) {
            if (_area.Rooms.ContainsKey(id)) {
                return _area.Rooms[id];
            }

            return null;
        }

        private void GetRoomExits(int id, Dictionary<string, string> exitList, Dictionary<string, string> invisibleExitList) {
            List<Exit> ex = new List<Exit>();

            // find all exits
            ex.Clear();
            foreach (Exit e in _area.Exits) {
                if (e.idFrom == id || e.idTo == id) {
                    ex.Add(e);
                }
            }

            // convert exits into hashtable
            exitList.Clear();
            invisibleExitList.Clear();
            foreach (Exit exit in ex) {
                if (exit.idFrom == id) {
                    var r = _area.Rooms[exit.idTo];
                    if (r == null) continue;
                    if (!exit.canFrom) continue;
                    if (exit.invFrom) {
                        invisibleExitList.Add(exit.dirFrom, GetRoomFileName(r));
                    }
                    if (_options.AddInvisible) exitList.Add(exit.dirFrom, GetRoomFileName(r));
                } else if (exit.idTo == id) {
                    var r = _area.Rooms[exit.idFrom];
                    if (r == null) continue;
                    if (!exit.canTo) continue;
                    if (exit.invTo) {
                        invisibleExitList.Add(exit.dirTo, GetRoomFileName(r));
                    }
                    if (_options.AddInvisible) exitList.Add(exit.dirTo, GetRoomFileName(r));
                }
            }
        }


        private static string GetObjectFileName(GenesisObject go) {
            return go.id + ".c";
        }

        private static string GetObjectFileName(GenesisObject go, Group gg) {
            return go.id + ".c";
        }

        private string GetRoomFileName(Room r) {
            var highestId = FindHighestId();
            int l = (highestId + _area.RoomIdOffset).ToString().Length;

            string name = r.GetProperty("filename");
            string formatted;
            if (name.Length == 0) {
                formatted = _area.DefaultRoomFileNamePrefix + "%N";
            } else {
                formatted = name;
            }

            formatted = formatted.Replace("%N", (r.id + _area.RoomIdOffset).ToString("D" + l.ToString()));
            formatted = formatted.Replace("%n", (r.id + _area.RoomIdOffset).ToString());
            formatted = formatted.Replace("%A", _area.DefaultRoomFileNamePrefix);
            return formatted + ".c";
        }

        private int FindHighestId() {
            int highestId = -1;

            if (highestId == -1) {
                foreach (var room in _area.Rooms.Values) {
                    if (room.id > highestId) highestId = room.id;
                }
            }

            return highestId;
        }
    }
}
