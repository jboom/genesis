using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Genesis {
    public partial class formNewArea : Form {
        public String SelectedTemplate = "[Blank]";

        public formNewArea() {
            InitializeComponent();
        }

        private void formNewArea_Load(object sender, EventArgs e) {
            UpdateTemplateList();   
        }

        public const string BlankTemplateLabel = "[Blank]";

        private void UpdateTemplateList() {
            listTemplates.Items.Clear();
            listTemplates.SuspendLayout();

            listTemplates.Items.Add(BlankTemplateLabel, 0);

            var templatesPath = Common.GetTemplatesPath();
            if (!Directory.Exists(templatesPath)) {
                Directory.CreateDirectory(templatesPath);
            }

            DirectoryInfo dirList = new DirectoryInfo(templatesPath);

            foreach (FileInfo file in dirList.GetFiles()) {
                if (file.Extension == Common.EXT_AREATEMPLATE) {
                    listTemplates.Items.Add(file.Name.Substring(0, file.Name.Length - Common.EXT_AREATEMPLATE.Length), 0);
                }
            }

            listTemplates.ResumeLayout();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void listTemplates_SelectedIndexChanged(object sender, EventArgs e) {
            if (listTemplates.SelectedItems.Count == 0) return;
            SelectedTemplate = listTemplates.SelectedItems[0].Text;
        }

        private void listTemplates_DoubleClick(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}