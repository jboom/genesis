using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Genesis
{
	/// <summary>
	/// Summary description for formEditor.
	/// </summary>
	public class formEditor : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        private enum Modes : int {
            None = 0,
            Room = 1,
            Object = 2
        }

		private bool hasSaved = false;
		private readonly formMain parent;
	    private readonly Area _area;
		private BaseObject currentObject = null;
		private bool isLoading = false;
        private Modes Mode = Modes.None;

		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonRevert;
		private System.Windows.Forms.TabPage tabDetailsRoom;
        private System.Windows.Forms.TabPage tabOptionsRoom;
		private System.Windows.Forms.Label labelID;
		private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabAdvanced;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox comboMulti;
        private System.Windows.Forms.OpenFileDialog fileOpen;
		private System.Windows.Forms.TextBox textNotes;
		private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listExits;
		private System.Windows.Forms.CheckBox checkVisible;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label labelZoneColour;
		private System.Windows.Forms.ColorDialog colorPick;
		private System.Windows.Forms.CheckBox checkInclude;
		private System.Windows.Forms.Button buttonDelete;
        private Label label8;
        private TextBox textFilename;
        private TextBox textBox1;
        private DataGridView gridAdvanced;
        private TabPage tabExits;
        private TabPage tabDetailsObject;
        private TabPage tabOptionsObject;
        private Label label12;
        private Label label13;
        private ComboBox comboGroup;
        private TabPage tabCode;
        private TabPage tabObjects;
        private TextBox textCode;
        private ComboBox comboCode;
        private Button buttonDeleteObject;
        private Button buttonSaveCode;
        private ListView listObjects;
        private Button buttonObjects;
        private ColumnHeader columnObject;
        private DataGridView gridProperties;
        private TabPage tabProperties;
        private DataGridViewTextBoxColumn columnMultiKey;
        private Button buttonDeleteProperty;
        private Button buttonDeleteAdvanced;
        private DataGridViewTextBoxColumn columnAdvancedKey;
        private DataGridViewTextBoxColumn columnAdvancedValue;
        private TableLayoutPanel tableDetailTab;
        private TableLayoutPanel tableObjectDetailsLayout;
        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel3;
        private Label label1;
        private TextBox textID;
        private ComboBox comboTemplate;
        private ComboBox comboObjectTemplate;
        private DataGridViewTextBoxColumn columnMultiValue;

		public formEditor(formMain form, Area area) {
			InitializeComponent();
			parent = form;
		    _area = area;
            //LoadTemplates();
            Common.LoadTemplates(comboTemplate, parent, "[Default]");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formEditor));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabDetailsRoom = new System.Windows.Forms.TabPage();
            this.tableDetailTab = new System.Windows.Forms.TableLayoutPanel();
            this.tabDetailsObject = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableObjectDetailsLayout = new System.Windows.Forms.TableLayoutPanel();
            this.comboGroup = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textID = new System.Windows.Forms.TextBox();
            this.tabProperties = new System.Windows.Forms.TabPage();
            this.buttonDeleteProperty = new System.Windows.Forms.Button();
            this.gridProperties = new System.Windows.Forms.DataGridView();
            this.columnMultiKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnMultiValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboMulti = new System.Windows.Forms.ComboBox();
            this.tabOptionsRoom = new System.Windows.Forms.TabPage();
            this.comboTemplate = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textFilename = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkInclude = new System.Windows.Forms.CheckBox();
            this.labelZoneColour = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textNotes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabOptionsObject = new System.Windows.Forms.TabPage();
            this.comboObjectTemplate = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabCode = new System.Windows.Forms.TabPage();
            this.buttonSaveCode = new System.Windows.Forms.Button();
            this.textCode = new System.Windows.Forms.TextBox();
            this.comboCode = new System.Windows.Forms.ComboBox();
            this.tabObjects = new System.Windows.Forms.TabPage();
            this.buttonObjects = new System.Windows.Forms.Button();
            this.listObjects = new System.Windows.Forms.ListView();
            this.columnObject = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonDeleteObject = new System.Windows.Forms.Button();
            this.tabExits = new System.Windows.Forms.TabPage();
            this.listExits = new System.Windows.Forms.ListBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.checkVisible = new System.Windows.Forms.CheckBox();
            this.tabAdvanced = new System.Windows.Forms.TabPage();
            this.buttonDeleteAdvanced = new System.Windows.Forms.Button();
            this.gridAdvanced = new System.Windows.Forms.DataGridView();
            this.columnAdvancedKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAdvancedValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonRevert = new System.Windows.Forms.Button();
            this.labelID = new System.Windows.Forms.Label();
            this.fileOpen = new System.Windows.Forms.OpenFileDialog();
            this.colorPick = new System.Windows.Forms.ColorDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabMain.SuspendLayout();
            this.tabDetailsRoom.SuspendLayout();
            this.tabDetailsObject.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProperties)).BeginInit();
            this.tabOptionsRoom.SuspendLayout();
            this.tabOptionsObject.SuspendLayout();
            this.tabCode.SuspendLayout();
            this.tabObjects.SuspendLayout();
            this.tabExits.SuspendLayout();
            this.tabAdvanced.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAdvanced)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.tabDetailsRoom);
            this.tabMain.Controls.Add(this.tabDetailsObject);
            this.tabMain.Controls.Add(this.tabProperties);
            this.tabMain.Controls.Add(this.tabOptionsRoom);
            this.tabMain.Controls.Add(this.tabOptionsObject);
            this.tabMain.Controls.Add(this.tabCode);
            this.tabMain.Controls.Add(this.tabObjects);
            this.tabMain.Controls.Add(this.tabExits);
            this.tabMain.Controls.Add(this.tabAdvanced);
            this.tabMain.HotTrack = true;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(365, 436);
            this.tabMain.TabIndex = 0;
            // 
            // tabDetailsRoom
            // 
            this.tabDetailsRoom.AutoScroll = true;
            this.tabDetailsRoom.Controls.Add(this.tableDetailTab);
            this.tabDetailsRoom.Location = new System.Drawing.Point(4, 22);
            this.tabDetailsRoom.Name = "tabDetailsRoom";
            this.tabDetailsRoom.Size = new System.Drawing.Size(357, 410);
            this.tabDetailsRoom.TabIndex = 0;
            this.tabDetailsRoom.Text = "Details";
            this.tabDetailsRoom.UseVisualStyleBackColor = true;
            // 
            // tableDetailTab
            // 
            this.tableDetailTab.AutoScroll = true;
            this.tableDetailTab.AutoSize = true;
            this.tableDetailTab.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableDetailTab.ColumnCount = 2;
            this.tableDetailTab.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableDetailTab.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableDetailTab.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableDetailTab.Location = new System.Drawing.Point(0, 0);
            this.tableDetailTab.Name = "tableDetailTab";
            this.tableDetailTab.RowCount = 1;
            this.tableDetailTab.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableDetailTab.Size = new System.Drawing.Size(357, 30);
            this.tableDetailTab.TabIndex = 14;
            // 
            // tabDetailsObject
            // 
            this.tabDetailsObject.Controls.Add(this.tableLayoutPanel3);
            this.tabDetailsObject.Location = new System.Drawing.Point(4, 22);
            this.tabDetailsObject.Name = "tabDetailsObject";
            this.tabDetailsObject.Size = new System.Drawing.Size(357, 410);
            this.tabDetailsObject.TabIndex = 4;
            this.tabDetailsObject.Text = "Details";
            this.tabDetailsObject.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableObjectDetailsLayout, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.comboGroup, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textID, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(357, 410);
            this.tableLayoutPanel3.TabIndex = 16;
            // 
            // tableObjectDetailsLayout
            // 
            this.tableObjectDetailsLayout.AutoScroll = true;
            this.tableObjectDetailsLayout.AutoSize = true;
            this.tableObjectDetailsLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableObjectDetailsLayout.ColumnCount = 2;
            this.tableLayoutPanel3.SetColumnSpan(this.tableObjectDetailsLayout, 2);
            this.tableObjectDetailsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableObjectDetailsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableObjectDetailsLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableObjectDetailsLayout.Location = new System.Drawing.Point(3, 53);
            this.tableObjectDetailsLayout.Name = "tableObjectDetailsLayout";
            this.tableObjectDetailsLayout.RowCount = 1;
            this.tableObjectDetailsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableObjectDetailsLayout.Size = new System.Drawing.Size(351, 30);
            this.tableObjectDetailsLayout.TabIndex = 15;
            // 
            // comboGroup
            // 
            this.comboGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGroup.Enabled = false;
            this.comboGroup.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboGroup.FormattingEnabled = true;
            this.comboGroup.Location = new System.Drawing.Point(45, 28);
            this.comboGroup.Name = "comboGroup";
            this.comboGroup.Size = new System.Drawing.Size(309, 23);
            this.comboGroup.TabIndex = 7;
            this.comboGroup.SelectedIndexChanged += new System.EventHandler(this.comboGroup_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 25);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label13.Size = new System.Drawing.Size(36, 18);
            this.label13.TabIndex = 6;
            this.label13.Text = "Group";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label1.Size = new System.Drawing.Size(18, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID";
            // 
            // textID
            // 
            this.textID.Location = new System.Drawing.Point(45, 3);
            this.textID.Name = "textID";
            this.textID.Size = new System.Drawing.Size(306, 20);
            this.textID.TabIndex = 17;
            // 
            // tabProperties
            // 
            this.tabProperties.Controls.Add(this.buttonDeleteProperty);
            this.tabProperties.Controls.Add(this.gridProperties);
            this.tabProperties.Controls.Add(this.comboMulti);
            this.tabProperties.Location = new System.Drawing.Point(4, 22);
            this.tabProperties.Name = "tabProperties";
            this.tabProperties.Size = new System.Drawing.Size(357, 410);
            this.tabProperties.TabIndex = 8;
            this.tabProperties.Text = "Properties";
            this.tabProperties.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteProperty
            // 
            this.buttonDeleteProperty.Location = new System.Drawing.Point(272, 5);
            this.buttonDeleteProperty.Name = "buttonDeleteProperty";
            this.buttonDeleteProperty.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteProperty.TabIndex = 8;
            this.buttonDeleteProperty.Text = "Delete";
            this.buttonDeleteProperty.UseVisualStyleBackColor = true;
            this.buttonDeleteProperty.Click += new System.EventHandler(this.buttonDeleteProperty_Click);
            // 
            // gridProperties
            // 
            this.gridProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridProperties.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnMultiKey,
            this.columnMultiValue});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProperties.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridProperties.Location = new System.Drawing.Point(6, 33);
            this.gridProperties.Name = "gridProperties";
            this.gridProperties.RowHeadersWidth = 25;
            this.gridProperties.Size = new System.Drawing.Size(341, 374);
            this.gridProperties.TabIndex = 7;
            this.gridProperties.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProperties_CellEndEdit_1);
            this.gridProperties.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.gridProperties_UserDeletedRow);
            // 
            // columnMultiKey
            // 
            this.columnMultiKey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.columnMultiKey.HeaderText = "Key";
            this.columnMultiKey.Name = "columnMultiKey";
            this.columnMultiKey.Width = 50;
            // 
            // columnMultiValue
            // 
            this.columnMultiValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnMultiValue.HeaderText = "Value";
            this.columnMultiValue.Name = "columnMultiValue";
            // 
            // comboMulti
            // 
            this.comboMulti.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMulti.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMulti.Items.AddRange(new object[] {
            "Properties",
            "Items",
            "Smells",
            "Listens",
            "Custom"});
            this.comboMulti.Location = new System.Drawing.Point(6, 5);
            this.comboMulti.Name = "comboMulti";
            this.comboMulti.Size = new System.Drawing.Size(147, 23);
            this.comboMulti.TabIndex = 6;
            this.comboMulti.SelectedIndexChanged += new System.EventHandler(this.comboMulti_SelectedIndexChanged);
            // 
            // tabOptionsRoom
            // 
            this.tabOptionsRoom.Controls.Add(this.comboTemplate);
            this.tabOptionsRoom.Controls.Add(this.textBox1);
            this.tabOptionsRoom.Controls.Add(this.textFilename);
            this.tabOptionsRoom.Controls.Add(this.label8);
            this.tabOptionsRoom.Controls.Add(this.checkInclude);
            this.tabOptionsRoom.Controls.Add(this.labelZoneColour);
            this.tabOptionsRoom.Controls.Add(this.label7);
            this.tabOptionsRoom.Controls.Add(this.label5);
            this.tabOptionsRoom.Controls.Add(this.textNotes);
            this.tabOptionsRoom.Controls.Add(this.label4);
            this.tabOptionsRoom.Location = new System.Drawing.Point(4, 22);
            this.tabOptionsRoom.Name = "tabOptionsRoom";
            this.tabOptionsRoom.Size = new System.Drawing.Size(357, 410);
            this.tabOptionsRoom.TabIndex = 1;
            this.tabOptionsRoom.Text = "Options";
            this.tabOptionsRoom.UseVisualStyleBackColor = true;
            // 
            // comboTemplate
            // 
            this.comboTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTemplate.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTemplate.FormattingEnabled = true;
            this.comboTemplate.Location = new System.Drawing.Point(80, 6);
            this.comboTemplate.Name = "comboTemplate";
            this.comboTemplate.Size = new System.Drawing.Size(267, 23);
            this.comboTemplate.TabIndex = 12;
            this.comboTemplate.SelectedIndexChanged += new System.EventHandler(this.comboTemplate_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(80, 58);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ShortcutsEnabled = false;
            this.textBox1.Size = new System.Drawing.Size(175, 51);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "%N - Room ID, zero padded\r\n%n - Room ID\r\n%A - Default Filename (Area Setup)";
            // 
            // textFilename
            // 
            this.textFilename.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textFilename.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFilename.Location = new System.Drawing.Point(80, 32);
            this.textFilename.Name = "textFilename";
            this.textFilename.Size = new System.Drawing.Size(267, 21);
            this.textFilename.TabIndex = 10;
            this.textFilename.Tag = "filename";
            this.textFilename.TextChanged += new System.EventHandler(this.textFilename_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Filename";
            // 
            // checkInclude
            // 
            this.checkInclude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkInclude.AutoSize = true;
            this.checkInclude.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkInclude.Location = new System.Drawing.Point(80, 209);
            this.checkInclude.Name = "checkInclude";
            this.checkInclude.Size = new System.Drawing.Size(117, 18);
            this.checkInclude.TabIndex = 8;
            this.checkInclude.Tag = "";
            this.checkInclude.Text = "Include in compile";
            this.checkInclude.CheckedChanged += new System.EventHandler(this.checkInclude_CheckedChanged);
            // 
            // labelZoneColour
            // 
            this.labelZoneColour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelZoneColour.BackColor = System.Drawing.Color.White;
            this.labelZoneColour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelZoneColour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelZoneColour.Location = new System.Drawing.Point(80, 190);
            this.labelZoneColour.Name = "labelZoneColour";
            this.labelZoneColour.Size = new System.Drawing.Size(40, 16);
            this.labelZoneColour.TabIndex = 7;
            this.labelZoneColour.Click += new System.EventHandler(this.labelZoneColour_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Zone Colour";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Notes";
            // 
            // textNotes
            // 
            this.textNotes.AcceptsReturn = true;
            this.textNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textNotes.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNotes.Location = new System.Drawing.Point(80, 115);
            this.textNotes.Multiline = true;
            this.textNotes.Name = "textNotes";
            this.textNotes.Size = new System.Drawing.Size(267, 72);
            this.textNotes.TabIndex = 4;
            this.textNotes.Tag = "notes";
            this.textNotes.TextChanged += new System.EventHandler(this.textNotes_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Template";
            // 
            // tabOptionsObject
            // 
            this.tabOptionsObject.Controls.Add(this.comboObjectTemplate);
            this.tabOptionsObject.Controls.Add(this.label12);
            this.tabOptionsObject.Location = new System.Drawing.Point(4, 22);
            this.tabOptionsObject.Name = "tabOptionsObject";
            this.tabOptionsObject.Size = new System.Drawing.Size(357, 410);
            this.tabOptionsObject.TabIndex = 5;
            this.tabOptionsObject.Text = "Options";
            this.tabOptionsObject.UseVisualStyleBackColor = true;
            // 
            // comboObjectTemplate
            // 
            this.comboObjectTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboObjectTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboObjectTemplate.FormattingEnabled = true;
            this.comboObjectTemplate.Location = new System.Drawing.Point(91, 6);
            this.comboObjectTemplate.Name = "comboObjectTemplate";
            this.comboObjectTemplate.Size = new System.Drawing.Size(256, 21);
            this.comboObjectTemplate.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Template";
            // 
            // tabCode
            // 
            this.tabCode.Controls.Add(this.buttonSaveCode);
            this.tabCode.Controls.Add(this.textCode);
            this.tabCode.Controls.Add(this.comboCode);
            this.tabCode.Location = new System.Drawing.Point(4, 22);
            this.tabCode.Name = "tabCode";
            this.tabCode.Size = new System.Drawing.Size(357, 410);
            this.tabCode.TabIndex = 6;
            this.tabCode.Text = "Code";
            this.tabCode.UseVisualStyleBackColor = true;
            // 
            // buttonSaveCode
            // 
            this.buttonSaveCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveCode.Location = new System.Drawing.Point(274, 3);
            this.buttonSaveCode.Name = "buttonSaveCode";
            this.buttonSaveCode.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveCode.TabIndex = 2;
            this.buttonSaveCode.Text = "Save";
            this.buttonSaveCode.UseVisualStyleBackColor = true;
            this.buttonSaveCode.Click += new System.EventHandler(this.buttonSaveCode_Click);
            // 
            // textCode
            // 
            this.textCode.AcceptsReturn = true;
            this.textCode.AcceptsTab = true;
            this.textCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textCode.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCode.Location = new System.Drawing.Point(8, 32);
            this.textCode.Multiline = true;
            this.textCode.Name = "textCode";
            this.textCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textCode.Size = new System.Drawing.Size(339, 375);
            this.textCode.TabIndex = 1;
            this.textCode.WordWrap = false;
            // 
            // comboCode
            // 
            this.comboCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCode.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboCode.FormattingEnabled = true;
            this.comboCode.Location = new System.Drawing.Point(8, 5);
            this.comboCode.Name = "comboCode";
            this.comboCode.Size = new System.Drawing.Size(134, 23);
            this.comboCode.TabIndex = 0;
            this.comboCode.SelectedIndexChanged += new System.EventHandler(this.comboCode_SelectedIndexChanged);
            // 
            // tabObjects
            // 
            this.tabObjects.Controls.Add(this.buttonObjects);
            this.tabObjects.Controls.Add(this.listObjects);
            this.tabObjects.Controls.Add(this.buttonDeleteObject);
            this.tabObjects.Location = new System.Drawing.Point(4, 22);
            this.tabObjects.Name = "tabObjects";
            this.tabObjects.Size = new System.Drawing.Size(357, 410);
            this.tabObjects.TabIndex = 7;
            this.tabObjects.Text = "Objects";
            this.tabObjects.UseVisualStyleBackColor = true;
            // 
            // buttonObjects
            // 
            this.buttonObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonObjects.Location = new System.Drawing.Point(274, 3);
            this.buttonObjects.Name = "buttonObjects";
            this.buttonObjects.Size = new System.Drawing.Size(75, 23);
            this.buttonObjects.TabIndex = 4;
            this.buttonObjects.Text = "Objects...";
            this.buttonObjects.UseVisualStyleBackColor = true;
            this.buttonObjects.Click += new System.EventHandler(this.buttonObjects_Click);
            // 
            // listObjects
            // 
            this.listObjects.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listObjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnObject});
            this.listObjects.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listObjects.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listObjects.Location = new System.Drawing.Point(3, 3);
            this.listObjects.Name = "listObjects";
            this.listObjects.Size = new System.Drawing.Size(263, 404);
            this.listObjects.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listObjects.TabIndex = 3;
            this.listObjects.UseCompatibleStateImageBehavior = false;
            this.listObjects.View = System.Windows.Forms.View.Details;
            // 
            // columnObject
            // 
            this.columnObject.Text = "Object";
            this.columnObject.Width = 230;
            // 
            // buttonDeleteObject
            // 
            this.buttonDeleteObject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteObject.Location = new System.Drawing.Point(274, 32);
            this.buttonDeleteObject.Name = "buttonDeleteObject";
            this.buttonDeleteObject.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteObject.TabIndex = 2;
            this.buttonDeleteObject.Text = "Delete";
            this.buttonDeleteObject.UseVisualStyleBackColor = true;
            this.buttonDeleteObject.Click += new System.EventHandler(this.buttonDeleteObject_Click);
            // 
            // tabExits
            // 
            this.tabExits.Controls.Add(this.listExits);
            this.tabExits.Controls.Add(this.buttonDelete);
            this.tabExits.Controls.Add(this.checkVisible);
            this.tabExits.Location = new System.Drawing.Point(4, 22);
            this.tabExits.Name = "tabExits";
            this.tabExits.Size = new System.Drawing.Size(357, 410);
            this.tabExits.TabIndex = 3;
            this.tabExits.Text = "Exits";
            this.tabExits.UseVisualStyleBackColor = true;
            // 
            // listExits
            // 
            this.listExits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listExits.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listExits.ItemHeight = 15;
            this.listExits.Location = new System.Drawing.Point(3, 4);
            this.listExits.Name = "listExits";
            this.listExits.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listExits.Size = new System.Drawing.Size(120, 379);
            this.listExits.TabIndex = 1;
            this.listExits.SelectedIndexChanged += new System.EventHandler(this.listExits_SelectedIndexChanged);
            this.listExits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listExits_KeyDown);
            // 
            // buttonDelete
            // 
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDelete.Location = new System.Drawing.Point(129, 4);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(48, 20);
            this.buttonDelete.TabIndex = 4;
            this.buttonDelete.Text = "&Delete";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // checkVisible
            // 
            this.checkVisible.AutoSize = true;
            this.checkVisible.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkVisible.Location = new System.Drawing.Point(129, 30);
            this.checkVisible.Name = "checkVisible";
            this.checkVisible.Size = new System.Drawing.Size(62, 18);
            this.checkVisible.TabIndex = 3;
            this.checkVisible.Text = "Visible";
            this.checkVisible.CheckedChanged += new System.EventHandler(this.checkVisible_CheckedChanged);
            // 
            // tabAdvanced
            // 
            this.tabAdvanced.Controls.Add(this.buttonDeleteAdvanced);
            this.tabAdvanced.Controls.Add(this.gridAdvanced);
            this.tabAdvanced.Location = new System.Drawing.Point(4, 22);
            this.tabAdvanced.Name = "tabAdvanced";
            this.tabAdvanced.Size = new System.Drawing.Size(357, 410);
            this.tabAdvanced.TabIndex = 2;
            this.tabAdvanced.Text = "Advanced";
            this.tabAdvanced.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteAdvanced
            // 
            this.buttonDeleteAdvanced.Location = new System.Drawing.Point(272, 3);
            this.buttonDeleteAdvanced.Name = "buttonDeleteAdvanced";
            this.buttonDeleteAdvanced.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteAdvanced.TabIndex = 7;
            this.buttonDeleteAdvanced.Text = "Delete";
            this.buttonDeleteAdvanced.UseVisualStyleBackColor = true;
            this.buttonDeleteAdvanced.Click += new System.EventHandler(this.buttonDeleteAdvanced_Click);
            // 
            // gridAdvanced
            // 
            this.gridAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridAdvanced.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridAdvanced.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAdvanced.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnAdvancedKey,
            this.columnAdvancedValue});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridAdvanced.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridAdvanced.Location = new System.Drawing.Point(8, 32);
            this.gridAdvanced.MultiSelect = false;
            this.gridAdvanced.Name = "gridAdvanced";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridAdvanced.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridAdvanced.RowHeadersWidth = 25;
            this.gridAdvanced.Size = new System.Drawing.Size(339, 375);
            this.gridAdvanced.TabIndex = 6;
            this.gridAdvanced.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProperties_CellEndEdit);
            this.gridAdvanced.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.gridAdvanced_UserDeletedRow);
            // 
            // columnAdvancedKey
            // 
            this.columnAdvancedKey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.columnAdvancedKey.HeaderText = "Name";
            this.columnAdvancedKey.Name = "columnAdvancedKey";
            this.columnAdvancedKey.Width = 60;
            // 
            // columnAdvancedValue
            // 
            this.columnAdvancedValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnAdvancedValue.HeaderText = "Value";
            this.columnAdvancedValue.Name = "columnAdvancedValue";
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClose.Location = new System.Drawing.Point(271, 442);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(80, 24);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "&Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSave.Location = new System.Drawing.Point(183, 442);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(80, 24);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "&Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonRevert
            // 
            this.buttonRevert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRevert.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonRevert.Location = new System.Drawing.Point(95, 442);
            this.buttonRevert.Name = "buttonRevert";
            this.buttonRevert.Size = new System.Drawing.Size(80, 24);
            this.buttonRevert.TabIndex = 3;
            this.buttonRevert.Text = "&Revert";
            this.buttonRevert.Click += new System.EventHandler(this.buttonRevert_Click);
            // 
            // labelID
            // 
            this.labelID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelID.AutoSize = true;
            this.labelID.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelID.ForeColor = System.Drawing.Color.Navy;
            this.labelID.Location = new System.Drawing.Point(12, 444);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(54, 22);
            this.labelID.TabIndex = 4;
            this.labelID.Text = "1234";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(357, 30);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // formEditor
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(363, 475);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.buttonRevert);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.tabMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 332);
            this.Name = "formEditor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Room Editor";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formEditor_FormClosing);
            this.tabMain.ResumeLayout(false);
            this.tabDetailsRoom.ResumeLayout(false);
            this.tabDetailsRoom.PerformLayout();
            this.tabDetailsObject.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabProperties.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridProperties)).EndInit();
            this.tabOptionsRoom.ResumeLayout(false);
            this.tabOptionsRoom.PerformLayout();
            this.tabOptionsObject.ResumeLayout(false);
            this.tabOptionsObject.PerformLayout();
            this.tabCode.ResumeLayout(false);
            this.tabCode.PerformLayout();
            this.tabObjects.ResumeLayout(false);
            this.tabExits.ResumeLayout(false);
            this.tabExits.PerformLayout();
            this.tabAdvanced.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAdvanced)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void FieldChanged(object sender, EventArgs a) {
            if (!isLoading) SetSaved(false);
        }

        public bool CheckSaved() {
            DialogResult dr;

            if (currentObject != null && !hasSaved) {
                dr = MessageBox.Show("The current changes have not been saved yet, do you want to save them now?", Common.APP_NAME, System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel) {
                    return false;
                } else if (dr == DialogResult.Yes) {
                    SaveBaseObject();
                }
            }

            return true;
        }

		private void buttonClose_Click(object sender, System.EventArgs e) {
            if (!CheckSaved()) return;

			parent.Editor = null;
			this.Close();
		}

        /*
        private void CheckSaved() {
            DialogResult dr;
            string message = "";

            if (currentObject != null && !hasSaved) {
                message = "The current changes have not been saved yet, do you want to save them now?";
                dr = MessageBox.Show(message, Common.APP_NAME, System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel) {
                    return;
                } else if (dr == DialogResult.Yes) {
                    SaveBaseObject();
                }
            }

        }
         */

		private bool LoadCurrentObject() {
			try {
				// load default edit controls
				foreach (TabPage t in this.tabMain.TabPages) {
					foreach (Control c in t.Controls) {
						if (c is System.Windows.Forms.TextBox) {
							if (((string)((TextBox)c).Tag) != null) {
								((TextBox)c).Text = currentObject.GetProperty((string)((TextBox)c).Tag);
							}
						}
					}
				}

                UpdateAdvanced();

				if (comboMulti.SelectedIndex == -1 && comboMulti.Items.Count > 0) comboMulti.SelectedIndex = 0;

                LoadCodeCombo();
				UpdateMultiGrid();

			} catch (Exception e) {
                Common.Error(e.Message);
                return false;
			}

			return true;
		}

        private void UpdateAdvanced() {
            DataGridViewRow row;
            // load property list
            if (gridAdvanced.Columns.Count == 0) SetupAdvancedColumns();
            gridAdvanced.Rows.Clear();
            foreach (KeyValuePair<String,String> DE in currentObject.properties) {
                row = new DataGridViewRow();
                row.CreateCells(gridAdvanced);
                row.SetValues(new string[] { (string)DE.Key, (string)DE.Value });
                row.Tag = (string)DE.Key;
                gridAdvanced.Rows.Add(row);
            }
        }

        private void SetupAdvancedColumns() {
            Console.WriteLine("Missing Columns - Advanced");
            gridAdvanced.Columns.Clear();
            gridAdvanced.Columns.Add(columnAdvancedKey);
            gridAdvanced.Columns.Add(columnAdvancedValue);
        }

        private void SetupPropertiesColumns() {
            Console.WriteLine("Missing Columns - Properties");
            gridProperties.Columns.Clear();
            gridProperties.Columns.Add(columnMultiKey);
            gridProperties.Columns.Add(columnMultiValue);
        }

        public void LoadGenesisObject(ref GenesisObject o) {
            if (!CheckSaved()) return;

            isLoading = true;
            currentObject = o;
            if (Mode != Modes.Object) {
                Mode = Modes.Object;
                LoadComboList();
            }
            UpdateTabs();
            LoadCurrentObject();
            LoadObjectList();
            UpdateGroupCombo();
            Common.LoadTemplates(comboObjectTemplate, parent, null);
            textID.Text = ((GenesisObject)currentObject).id;
            SetSaved(true);
            textID.Focus();
            if (o.Group == null) {
                tableObjectDetailsLayout.Controls.Clear();
            } else {
                //LoadLayout(o.Group.layout, tableObjectDetailsLayout);
                LoadLayout(Layouts.GetLayout(o.Group.LayoutFile), tableObjectDetailsLayout);
            }
            Common.SetComboSelected(comboObjectTemplate, o.GetProperty("template"));
            isLoading = false;
        }

        public void UpdateGroupCombo() {
            int index;

            if (Mode != Modes.Object) return;

            comboGroup.BeginUpdate();
            comboGroup.Items.Clear();
            foreach (var gg in _area.Groups.Values) {
                index = comboGroup.Items.Add(gg.Name);
                if (gg.Equals(((GenesisObject)currentObject).Group)) comboGroup.SelectedIndex = index;
            }
            comboGroup.EndUpdate();
        }

        public void LoadRoom(ref Room r) {
            CheckSaved();
            isLoading = true;
            currentObject = r;
            if (Mode != Modes.Room) {
                Mode = Modes.Room;
                LoadComboList();
            }
            UpdateTabs();
            LoadCurrentObject();
            LoadObjectList();
            // set include check
            checkInclude.Checked = ((Room)currentObject).include;
            // load exits list
            LoadExits();
            // load zone colour
            if (currentObject.GetProperty("colour").Length > 0) {
                labelZoneColour.BackColor = Color.FromArgb(Convert.ToInt32(currentObject.GetProperty("colour")));
            }
            // set number label
            labelID.Text = ((Room)currentObject).id.ToString();
            SetSaved(true);
            LoadLayout(parent.defaultRoomLayout, tableDetailTab);
			isLoading = false;
		}

        private void LoadLayout(DetailLayout layout, TableLayoutPanel table) {
            Label label;
            ContainerControl container;
            RowStyle rowStyle;
            int c = 0, height = 0;

            table.SuspendLayout();
            table.Controls.Clear();
            table.RowStyles.Clear();
			foreach (DetailField field in layout) {
				label = new Label();
				label.Text = field.Name;
				label.Padding = new Padding(0, 5, 0, 0);
				container = field.toControls(currentObject, FieldChanged);
				rowStyle = new RowStyle(SizeType.Absolute, container.Height);
				table.RowStyles.Add(rowStyle);
				table.Controls.Add(label, 0, c);
				table.Controls.Add(container, 1, c);
				height += container.Height;
				c++;
			}
            table.Height = height;
            table.ResumeLayout();
        }

        public void AddObjectToCurrentObject(GenesisObject newObject) {
            currentObject.objects.Add(newObject);
            LoadObjectList();
        }

        public void LoadObjectList() {
            listObjects.Items.Clear();
            listObjects.Groups.Clear();

            if (currentObject.objects.Count == 0) return;

            foreach (var gg in _area.Groups.Values) {
                var lvg = new ListViewGroup(gg.Name);
                lvg.Tag = gg.Name;
                listObjects.Groups.Add(lvg);

                foreach (GenesisObject go in currentObject.objects) {
                    if (go.Group == null) continue;
                    if (!go.Group.Equals(gg)) continue;
                    var lvi = new ListViewItem(go.id);
                    lvi.Tag = go;
                    lvi.Group = lvg;
                    listObjects.Items.Add(lvi);
                }
            }
        }

        private void LoadCodeCombo() {
            comboCode.BeginUpdate();
            comboCode.Items.Clear();
            foreach (string s in parent.CodePropertyList) {
                comboCode.Items.Add(s);
            }
            if (comboCode.Items.Count > 0) comboCode.SelectedIndex = 0;
            comboCode.EndUpdate();
        }

        private void LoadComboList() {
            List<String> list;

            switch (Mode) {
                case Modes.Room:
                    list = parent.RoomPropertyTypeList;
                    break;
                case Modes.Object:
                    list = parent.ObjectPropertyTypeList;
                    break;
                default:
                    list = new List<String>();
                    break;
            }
            comboMulti.BeginUpdate();
            comboMulti.Items.Clear();
            foreach (string s in list) {
                comboMulti.Items.Add(s);
            }
            comboMulti.EndUpdate();
        }

        private void UpdateTabs() {
            string CurrentTab = tabMain.SelectedTab.Text;
            switch (Mode) {
                case Modes.Room:
                    if (tabMain.TabPages.Contains(tabDetailsObject)) tabMain.TabPages.Remove(tabDetailsObject);
                    if (tabMain.TabPages.Contains(tabOptionsObject)) tabMain.TabPages.Remove(tabOptionsObject);
                    if (!tabMain.TabPages.Contains(tabExits)) tabMain.TabPages.Insert(0, tabExits);
                    if (!tabMain.TabPages.Contains(tabOptionsRoom)) tabMain.TabPages.Insert(0, tabOptionsRoom);
                    if (!tabMain.TabPages.Contains(tabDetailsRoom)) tabMain.TabPages.Insert(0, tabDetailsRoom);
                    labelID.Visible = true;
                    break;
                case Modes.Object:
                    if (tabMain.TabPages.Contains(tabExits)) tabMain.TabPages.Remove(tabExits);
                    if (tabMain.TabPages.Contains(tabDetailsRoom)) tabMain.TabPages.Remove(tabDetailsRoom);
                    if (tabMain.TabPages.Contains(tabOptionsRoom)) tabMain.TabPages.Remove(tabOptionsRoom);
                    if (!tabMain.TabPages.Contains(tabOptionsObject)) tabMain.TabPages.Insert(0, tabOptionsObject);
                    if (!tabMain.TabPages.Contains(tabDetailsObject)) tabMain.TabPages.Insert(0, tabDetailsObject);
                    labelID.Visible = false;
                    break;
            }
            foreach (TabPage tp in tabMain.TabPages) {
                if (tp.Text == CurrentTab) {
                    tabMain.SelectedTab = tp;
                }
            }
        }

        private void SaveRoom() {
            currentObject.SetProperty("colour", labelZoneColour.BackColor.ToArgb().ToString());
            ((Room)currentObject).include = checkInclude.Checked;
            ((Room)currentObject).updated = true;
            parent.ValidateRoom(((Room)currentObject).id);
        }

        private void SaveObject() {
            ((GenesisObject)currentObject).id = textID.Text;
            ((GenesisObject)currentObject).Group = parent.FindGroupByName(comboGroup.Text);
            /*if (parent.ObjectsForm != null) {
                parent.ObjectsForm.UpdateObjectList();
            }*/
        }

		public void SaveBaseObject() {
            if (Mode == Modes.Object) {
                if (textID.Text.Length == 0) {
                    MessageBox.Show("You cannot save an object with an empty ID.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                } else if (parent.ObjectIdTaken(textID.Text, this.currentObject)) {
                    MessageBox.Show("An object with this ID already exists.", Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }

			// save default edit controls
			foreach (TabPage t in this.tabMain.TabPages) {
				foreach (Control c in t.Controls) {
					if (c is System.Windows.Forms.TextBox) {
						if (((string)((TextBox)c).Tag) != null) {
							currentObject.SetProperty((string)((TextBox)c).Tag, ((TextBox)c).Text);
						}
                    }
				}
			}


            switch (Mode) {
                case Modes.Room:
                    foreach (Control c in this.tableDetailTab.Controls) {
                        if (c.Tag != null) {
                            ((DetailField)c.Tag).ProcessControls((ContainerControl)c, currentObject);
                        }
                    }
                    SaveRoom();
                    if (comboTemplate.SelectedIndex > 0) {
                        currentObject.SetProperty("template", comboTemplate.Text);
                    }
                    parent.Redraw(true);
                    break;
                case Modes.Object:
                    foreach (Control c in this.tableObjectDetailsLayout.Controls) {
                        if (c.Tag != null) {
                            ((DetailField)c.Tag).ProcessControls((ContainerControl)c, currentObject);
                        }
                    }
                    SaveObject();
                    if (!_area.Objects.ContainsValue((GenesisObject)currentObject)) {
                        parent.AddObject((GenesisObject)currentObject);
                    }
                    if (parent.ObjectsForm != null) parent.ObjectsForm.UpdateObjectList();
                    currentObject.SetProperty("template", comboObjectTemplate.Text);
                    break;
            }
			SetSaved(true);
			parent.Saved = false;
		}

		public void LoadExits() {
			listExits.BeginUpdate();
			listExits.Items.Clear();
			foreach (Exit e in _area.Exits) {
				if (e.idFrom == ((Room)currentObject).id && e.canFrom) {
					listExits.Items.Add(e.dirFrom);
                } else if (e.idTo == ((Room)currentObject).id && e.canTo) {
					listExits.Items.Add(e.dirTo);
				}
			}
			listExits.EndUpdate();

			checkVisible.Checked = false;
			checkVisible.Enabled = false;
		}

		private void buttonSave_Click(object sender, System.EventArgs e) {
            SaveBaseObject();
		}

		public void SetSaved(bool s) {
			hasSaved = s;
			if (s) buttonSave.Enabled = false; else buttonSave.Enabled = true;
			UpdateTitle();
		}

		public void UpdateTitle() {
            string title = "";

            switch (Mode) {
                case Modes.Room:
                    title = "Room Editor";
                    if (currentObject != null) title += " - " + ((Room)currentObject).id.ToString();
                    break;
                case Modes.Object:
                    title = "Object Editor";
                    if (currentObject != null) title += " - " + ((GenesisObject)currentObject).id;
                    break;
            }
            if (!hasSaved) title += "*";

            if (this.Text != title) this.Text = title;
		}

		private void comboMulti_SelectedIndexChanged(object sender, System.EventArgs e) {
			UpdateMultiGrid();
		}

		public void UpdateMultiGrid() {
			Hashtable h;
			ArrayList l = new ArrayList();
            DataGridViewRow row;

			if (currentObject == null) return;

            if (gridProperties.Columns.Count == 0) SetupPropertiesColumns();

			h = Common.GetMultiHash(currentObject.GetProperty(comboMulti.Text.ToLower()));

            gridProperties.Rows.Clear();
            foreach (DictionaryEntry DE in h) {
                l.Add(DE.Key);
            }
            l.Sort();
            foreach (string s in l) {
                row = new DataGridViewRow();
                row.CreateCells(gridProperties);
                row.SetValues(new string[] { s, (string)h[s] });
                row.Tag = s;
                gridProperties.Rows.Add(row);
            }
		}

		private void DeleteSelectedExit() {
			bool deleted = false;

			if (currentObject == null) return;

			if (listExits.SelectedIndex == -1) return;

			string exit = (string)listExits.SelectedItem;

			foreach (Exit e in _area.Exits) {
                if (e.dirFrom == exit && e.idFrom == ((Room)currentObject).id) {
					_area.Exits.Remove(e);
					parent.Saved = false;
					deleted = true;
					break;
				}
                if (e.dirTo == exit && e.idTo == ((Room)currentObject).id) {
					_area.Exits.Remove(e);
					parent.Saved = false;
					deleted = true;
					break;
				}
			}

            if (deleted) {
                LoadExits();
                parent.Redraw(true);
            }
			UpdateMultiGrid();
		}

		private void textTemplate_TextChanged(object sender, System.EventArgs e) {
			if (!isLoading) SetSaved(false);
		}

		private void textShort_TextChanged(object sender, System.EventArgs e) {
			if (!isLoading) SetSaved(false);
		}

		private void textLong_TextChanged(object sender, System.EventArgs e) {
			if (!isLoading) SetSaved(false);
		}

		private void textLongNight_TextChanged(object sender, System.EventArgs e) {
			if (!isLoading) SetSaved(false);
		}

		private void listExits_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
			List<Exit> del;

			del = new List<Exit>();
			if (e.KeyCode == Keys.Delete && listExits.SelectedIndex > -1) {
				foreach (Exit ex in _area.Exits) {
                    if (ex.idFrom == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirFrom) {
						del.Add(ex);
                    } else if (ex.idTo == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirTo) {
						del.Add(ex);
					}
				}
				foreach (Exit ex in del) {
					if (e.Shift) {
                        if (ex.idFrom == ((Room)currentObject).id) {
							ex.canFrom = false;
                            /*
                            // delete exit since both ways are closed off
							if (!ex.canTo) {
								parent.DeleteExits();
							}
                             */
                        } else if (ex.idTo == ((Room)currentObject).id) {
							ex.canTo = false;
                            /*
							if (!ex.canFrom) {
								parent.DeleteExits(ex);
							}
                            */
						}
					} else {
						//parent.DeleteExits(ex);
                        List<Exit> delete = new List<Exit>();
                        delete.Add(ex);
                        parent.DeleteExits(delete);
					}
				}
				LoadExits();
				parent.Redraw(true);
			}
		}

		private void listExits_SelectedIndexChanged(object sender, System.EventArgs e) {
			if (listExits.SelectedIndex == -1) return;

			checkVisible.Enabled = true;

			foreach (Exit ex in _area.Exits) {
                if (ex.idFrom == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirFrom) {
					isLoading = true;
					checkVisible.Checked = !ex.invFrom;
					isLoading = false;
                } else if (ex.idTo == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirTo) {
					isLoading = true;
					checkVisible.Checked = !ex.invTo;
					isLoading = false;
				}
			}
		}

		private void checkVisible_CheckedChanged(object sender, System.EventArgs e) {
			if (listExits.SelectedItems.Count == 0) return;
			if (listExits.SelectedItems.Count > 1) return;
			if (isLoading) return;

			foreach (Exit ex in _area.Exits) {
                if (ex.idFrom == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirFrom) {
					ex.invFrom = !checkVisible.Checked;
					parent.Saved = false;
                    parent.Redraw(true);
                } else if (ex.idTo == ((Room)currentObject).id && (string)listExits.SelectedItem == ex.dirTo) {
					ex.invTo = !checkVisible.Checked;
					parent.Saved = false;
                    parent.Redraw(true);
                }
			}
		}

		private void labelZoneColour_Click(object sender, System.EventArgs e) {
			colorPick.Color = labelZoneColour.BackColor;

			if (colorPick.ShowDialog() == DialogResult.Cancel) {
				return;
			}

			labelZoneColour.BackColor = colorPick.Color;
            SetSaved(false);
		}

		private void checkInclude_CheckedChanged(object sender, System.EventArgs e) {
			SetSaved(false);
		}

		private void buttonDelete_Click(object sender, System.EventArgs e) {
			DeleteSelectedExit();
		}

		private void buttonRevert_Click(object sender, System.EventArgs e) {
            switch (Mode) {
                case Modes.Room:
                    parent.LoadCurrentRoom(true);
                    break;
            }
		}

        private void textFilename_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void textNotes_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void gridProperties_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            DataGridViewRow row;
            row = gridAdvanced.Rows[e.RowIndex];
            if (row.Tag != null) currentObject.RemoveProperty((String)row.Tag);
            currentObject.SetProperty((String)row.Cells[0].Value, (String)row.Cells[1].Value);
            parent.Saved = false;
        }

        private void textID_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void textShortObject_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void textBox2_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void textBox3_TextChanged(object sender, EventArgs e) {
            if (!isLoading) SetSaved(false);
        }

        private void buttonTemplateObject_Click(object sender, EventArgs e) {
            fileOpen.AddExtension = true;
            fileOpen.CheckFileExists = true;
            fileOpen.DefaultExt = Common.EXT_TEMPLATE;
            fileOpen.Filter = "Template files (*" + Common.EXT_TEMPLATE + ")|*" + Common.EXT_TEMPLATE;
            fileOpen.Multiselect = false;
            fileOpen.ShowReadOnly = false;
            fileOpen.ValidateNames = true;

            if (fileOpen.ShowDialog() == DialogResult.Cancel) {
                return;
            }

            //textTemplateRoom.Text = fileOpen.FileName;
            SetSaved(false);
        }

        private void comboGroup_SelectedIndexChanged(object sender, EventArgs e) {
            if (isLoading) return;
            if (!hasSaved) {
                if (MessageBox.Show("Are you sure you want to change the Group of this object? All changes made since you last saved will be lost.", Common.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) {
                    isLoading = true;
                    Common.SetComboSelected(comboGroup, ((GenesisObject)currentObject).Group.Name);
                    isLoading = false;
                    return;
                }
            }
            SetSaved(false);
        }

        private void comboCode_SelectedIndexChanged(object sender, EventArgs e) {
            textCode.Text = currentObject.GetProperty(comboCode.Text);
        }

        private void buttonSaveCode_Click(object sender, EventArgs e) {
            currentObject.SetProperty(comboCode.Text, textCode.Text);
            parent.Saved = false;
        }

        private void buttonObjects_Click(object sender, EventArgs e) {
            parent.ShowObjects();
        }

        private void buttonDeleteObject_Click(object sender, EventArgs e) {
            if (listObjects.SelectedItems.Count == 0) return;

            parent.RemoveObjectFromCurrentRoom(listObjects.SelectedItems[0].Text);
            parent.Saved = false;
        }

        private void formEditor_FormClosing(object sender, FormClosingEventArgs e) {
            parent.EditorLeft = this.Left;
            parent.EditorTop = this.Top;
            parent.EditorHeight = this.Height;
            parent.EditorWidth = this.Width;
        }

        private void gridProperties_CellEndEdit_1(object sender, DataGridViewCellEventArgs e) {
            DataGridViewRow row;

            row = gridProperties.Rows[e.RowIndex];
            if (row.Tag != null) {
                currentObject.RemoveItemFromList(comboMulti.Text.ToLower(), (String)row.Tag);
            }
            currentObject.AddItemToList(comboMulti.Text.ToLower(), (String)row.Cells[0].Value, (String)row.Cells[1].Value);
            SetSaved(false);
        }

        private void buttonDeleteProperty_Click(object sender, EventArgs e) {
            if (gridProperties.CurrentRow == null) return;

            currentObject.RemoveItemFromList(comboMulti.Text.ToLower(), (String)gridProperties.CurrentRow.Tag);
            UpdateMultiGrid();
        }

        private void buttonDeleteAdvanced_Click(object sender, EventArgs e) {
            if (gridAdvanced.CurrentRow == null) return;

            currentObject.RemoveProperty((String)gridAdvanced.CurrentRow.Tag);
            UpdateAdvanced();
        }

        private void comboTemplate_SelectedIndexChanged(object sender, EventArgs e) {
            FieldChanged(sender, e);
        }

        private void gridProperties_UserDeletedRow(object sender, DataGridViewRowEventArgs e) {
            currentObject.RemoveItemFromList(comboMulti.Text.ToLower(), (string)e.Row.Cells[0].Value);
            parent.Saved = false;
        }

        private void gridAdvanced_UserDeletedRow(object sender, DataGridViewRowEventArgs e) {
            currentObject.RemoveProperty((string)e.Row.Cells[0].Value);
            parent.Saved = false;
        }
	}
}
