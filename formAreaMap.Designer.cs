namespace Genesis {
    partial class formAreaMap {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelViewPort = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelViewPort
            // 
            this.panelViewPort.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.panelViewPort.Location = new System.Drawing.Point(18, 29);
            this.panelViewPort.Name = "panelViewPort";
            this.panelViewPort.Size = new System.Drawing.Size(78, 62);
            this.panelViewPort.TabIndex = 0;
            this.panelViewPort.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelViewPort_MouseDown);
            this.panelViewPort.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelViewPort_MouseMove);
            this.panelViewPort.Paint += new System.Windows.Forms.PaintEventHandler(this.panelViewPort_Paint);
            this.panelViewPort.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelViewPort_MouseUp);
            // 
            // formAreaMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(134, 116);
            this.Controls.Add(this.panelViewPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "formAreaMap";
            this.ShowInTaskbar = false;
            this.Text = "Area Map";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formAreaMap_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelViewPort;
    }
}