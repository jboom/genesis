using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Genesis {
    // TODO: Replace with NLog
    class Log {
        public static void error(String source, String message) {
#if DEBUG
            MessageBox.Show(source + " = " + message, Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Console.WriteLine(source + " = " + message);
#else
            MessageBox.Show(message, Common.APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Console.WriteLine("Exception="+message);
#endif
        }

        public static void error(String str) {
            error(getSourceLine(), str);
        }

        public static void error(Exception e) {
            error(getSourceLine(), e.Message);
        }

        private static String getSourceLine() {
            try {
                Exception e = new Exception();
                throw e;
            } catch (Exception ex) {
                if (ex.StackTrace.Length >= 2) return ex.StackTrace[ex.StackTrace.Length - 1].ToString();
            }

            return "Unknown";
        }
    }
}
