using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Genesis {
    class Layouts {
        private static Dictionary<String, DetailLayout> cache = new Dictionary<string, DetailLayout>();
        private static List<String> names;

        static Layouts() {
            Reload();
        }

        private static void LoadLayoutNames() {
            DirectoryInfo dirList = new DirectoryInfo(Common.GetLayoutsPath());

            names = new List<string>();
            foreach (FileInfo file in dirList.GetFiles()) {
                if (file.Extension == Common.EXT_LAYOUT) {
                    names.Add(file.Name.Substring(0, file.Name.Length - Common.EXT_LAYOUT.Length));
                }
            }
        }

        public static void Reload() {
            LoadLayoutNames();
            ClearCache();
        }

        public static List<String> Names {
            get { return Layouts.names; }
            set { Layouts.names = value; }
        }

        public static void ClearCache() {
            cache = new Dictionary<string, DetailLayout>();
        }

        public static DetailLayout GetLayout(string LayoutName) {
            if (cache.ContainsKey(LayoutName)) {
                return cache[LayoutName];
            }

            DetailLayout layout;

            try {
                XmlSerializer xs = new XmlSerializer(typeof(DetailLayout));
                FileStream fs = new FileStream(Common.GetLayoutsPath() + LayoutName + Common.EXT_LAYOUT, FileMode.Open);
                layout = (DetailLayout)xs.Deserialize(fs);
                fs.Close();
                cache.Add(LayoutName, layout);
                return layout;
            } catch (Exception e) {
                Console.WriteLine("Error loading layout file: " + LayoutName);
                Console.WriteLine(e.Message);
                Log.error("Error loading layout file: " + LayoutName);
                return null;
            }
        }
    }
}
